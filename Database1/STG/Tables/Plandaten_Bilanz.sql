﻿CREATE TABLE [STG].[Plandaten_Bilanz] (
    [Kontonummer] VARCHAR (20)     NULL,
    [Monat]       INT              NULL,
    [Planwert]    DECIMAL (38, 20) NULL
);

