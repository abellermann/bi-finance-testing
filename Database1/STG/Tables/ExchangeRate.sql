﻿CREATE TABLE [STG].[ExchangeRate] (
    [Month]        INT              NULL,
    [GuV_Bilanz]   VARCHAR (10)     NULL,
    [ExchangeRate] DECIMAL (38, 20) NULL,
    [Currency]     VARCHAR (5)      NULL
);

