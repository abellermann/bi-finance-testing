﻿CREATE TABLE [STG].[Plandaten_GuV] (
    [Kostenstelle] VARCHAR (20)     NULL,
    [Kontonummer]  VARCHAR (20)     NULL,
    [PostingMonat] INT              NULL,
    [Planwert]     DECIMAL (38, 20) NULL
);

