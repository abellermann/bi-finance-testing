﻿CREATE TABLE [STG].[Kontenplaene] (
    [KontoNummer]            NVARCHAR (55) NULL,
    [EbenenCode]             NVARCHAR (55) NULL,
    [ParentEbenenCode]       BIGINT        NULL,
    [Name_DEU]               VARCHAR (255) NULL,
    [GuV_Bilanz]             VARCHAR (50)  NULL,
    [GKV_UKV]                VARCHAR (50)  NULL,
    [Controlling_Accounting] VARCHAR (50)  NULL,
    [HGB_IFRS]               VARCHAR (50)  NULL,
    [ProfitCenter]           VARCHAR (255) NULL,
    [HGB_IFRS_Beide]         VARCHAR (50)  NULL
);

