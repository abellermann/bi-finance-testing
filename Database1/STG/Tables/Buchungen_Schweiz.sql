﻿CREATE TABLE [STG].[Buchungen_Schweiz] (
    [MandantenName]      VARCHAR (50)     NULL,
    [Kontonummer_SchuBo] VARCHAR (20)     NULL,
    [Kostenstelle]       VARCHAR (20)     NULL,
    [Monat]              INT              NULL,
    [Amount_EUR]         DECIMAL (38, 20) NULL,
    [Amount_CHF]         DECIMAL (38, 20) NULL,
    [LevelID]            INT              NULL,
    [LadeDatum]          DATETIME         NULL
);

