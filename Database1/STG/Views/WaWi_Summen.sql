﻿CREATE VIEW STG.WaWi_Summen as 

		with summen
		as
		(
		select ID_Monat, EK_Wert, VK_Wert, Menge
			, case when Lager = '36' then 'offline AT'
				when Lager = '34' then 'online FF'
				else case when Verkaufskanal = 'Offline' then 'offline DE'
						else case when Lieferland = 'AT' then 'online AT'
								when Lieferland = 'SE' then 'online SE'
								when Lieferland = 'CH' then 'online CH'
								when Lieferland = 'GB' then 'online GB'
								else 'online DE'
								end
						end
				end as Kategorie
		from INPUT.WaWi_Summen
		)
		select ID_Monat
			, Mandant
			, Kostenstelle
			, ProfitCenter_kurz
			, SUM(VK_Wert) as VK_Wert
			, SUM(EK_Wert) as EK_Wert
			, SUM(Menge) as Menge
		from summen s
		left outer join META.WaWi_Mapping m
			 on s.Kategorie = m.Kategorie
		group by ID_Monat
			, Mandant
			, Kostenstelle
			, ProfitCenter_kurz