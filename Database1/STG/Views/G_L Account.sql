﻿
CREATE VIEW [STG].[G_L Account]
AS
SELECT  M.TS_INT, M.MandantID, LTRIM(RTRIM(M.No_)) AS [No], 
		ISNULL(a.[Name],ISNULL(b.[Name],ISNULL(c.[Name],ISNULL(d.[Name],
		ISNULL(e.[Name],ISNULL(f.[Name],ISNULL(g.[Name],ISNULL(h.[Name],
		ISNULL(i.[Name],ISNULL(j.[Name],ISNULL(k.[Name],l.[Name]))))))))))) [Name],
		M.[Account Type], M.Income_Balance, M.Debit_Credit, M.[Name 2], M.[Referenz Text], M.[Name AT], M.[Name ES], 
                         M.[Currency Code], M.[Acc_ Pos_ 1], M.[Acc_ Pos_ 2], M.[Acc_ Pos_ 3], M.[Acc_ Pos_ 4], M.LoadID, M.LoadDate, M.ModifiedLoadID, M.ModifiedDate
FROM            INPUT.[G_L Account] M
LEFT JOIN	INPUT.[G_L Account] as a on M.No_ = a.No_ and a.MandantID = 1
LEFT JOIN	INPUT.[G_L Account] as b on M.No_ = b.No_ and a.MandantID = 2
LEFT JOIN	INPUT.[G_L Account] as c on M.No_ = c.No_ and a.MandantID = 12
LEFT JOIN	INPUT.[G_L Account] as d on M.No_ = d.No_ and a.MandantID = 8
LEFT JOIN	INPUT.[G_L Account] as e on M.No_ = e.No_ and a.MandantID = 7
LEFT JOIN	INPUT.[G_L Account] as f on M.No_ = f.No_ and a.MandantID = 3
LEFT JOIN	INPUT.[G_L Account] as g on M.No_ = g.No_ and a.MandantID = 5
LEFT JOIN	INPUT.[G_L Account] as h on M.No_ = h.No_ and a.MandantID = 6
LEFT JOIN	INPUT.[G_L Account] as i on M.No_ = i.No_ and a.MandantID = 4
LEFT JOIN	INPUT.[G_L Account] as j on M.No_ = j.No_ and a.MandantID = 9
LEFT JOIN	INPUT.[G_L Account] as k on M.No_ = k.No_ and a.MandantID = 10
LEFT JOIN	INPUT.[G_L Account] as l on M.No_ = l.No_ and a.MandantID = 11
WHERE        (LTRIM(RTRIM(M.No_)) NOT LIKE '%A')


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "G_L Account (INPUT)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 344
               Right = 310
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'STG', @level1type = N'VIEW', @level1name = N'G_L Account';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'STG', @level1type = N'VIEW', @level1name = N'G_L Account';

