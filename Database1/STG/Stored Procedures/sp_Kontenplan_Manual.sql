﻿CREATE PROCEDURE [STG].[sp_Kontenplan_Manual]

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		Insert into STG.Kontenplaene
			(KontoNummer
			, EbenenCode
			, ParentEbenenCode
			, Name_DEU
			, GuV_Bilanz
			, ProfitCenter
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS
			, HGB_IFRS_Beide)
		values (null, 1, null, 'Nettoumsatz (WaWi)', 'Rohertrag (WaWi)', 'alle', '', '', '', 'Beide')
			, ('z999999', 11, 1, 'Nettoumsatz (WaWi)', 'Rohertrag (WaWi)', 'alle', '', '', '', 'Beide')
			, ('Pz999999', 10, 1, 'Planung Nettoumsatz (WaWi)', 'Rohertrag (WaWi)', 'alle', '', '', '', 'Beide')
			, (null, 2, null, 'Wareneinsatz (WaWi)', 'Rohertrag (WaWi)', 'alle', '', '', '', 'Beide')
			, ('z999998', 21, 2, 'Wareneinsatz (WaWi)', 'Rohertrag (WaWi)', 'alle', '', '', '', 'Beide')
			, ('Pz999998', 20, 2, 'Planung Wareneinsatz (WaWi)', 'Rohertrag (WaWi)', 'alle', '', '', '', 'Beide')

	END TRY
	
	BEGIN CATCH
	
	END CATCH
	
END