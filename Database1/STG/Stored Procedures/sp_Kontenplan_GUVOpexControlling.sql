﻿CREATE PROCEDURE [STG].[sp_Kontenplan_GUVOpexControlling]

AS
BEGIN
	BEGIN TRY
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		-- Konten Navision mit Informationen anreichern und wegspeichern
		IF OBJECT_ID('tempdb..#NavisionKonten') IS NOT NULL
		DROP TABLE #NavisionKonten
		
		Select convert(varchar,LTRIM(RTRIM(gla.[No])))                   as [KontoNummer]
			 , max(gla.[Name])											as [Name_DEU]
			 , case when MAX(gla.[Acc_ Pos_ 1])	<>0 then 1 else 0 end		as [HGB]
			 , case when MAX(gla.[Acc_ Pos_ 2])	<>0 then 1 else 0 end		as [IFRS]
		into #NavisionKonten 
		from [STG].[G_L Account] gla
		 left outer join [META].[GLA_Kontoart]      glaka on gla.[Account Type]   = glaka.ID
		 left outer join [META].[GLA_IncomeBalance] glaib on gla.[Income_Balance] = glaib.ID
		where gla.[Account Type] = 0 --Selektiert nur normale Konten, keine Summenkonten etc.
			and glaib.[Name_DEU] = 'GuV'
		group by gla.[No], gla.[Account Type]
		
		 
		  declare @KontenplanStufe1 as table(
				KontoNummer					nvarchar(255) NULL
				,Ebene						int
				,EbenenCode					bigint
				,ParentEbenenCode			bigint
				,Name_DEU					varchar(255) NULL
				,[HGB]						tinyint NULL
				,[IFRS]						tinyint NULL
				,GuV_Bilanz					varchar(50)
				,GKV_UKV					varchar(50)
				,Controlling_Accounting		varchar(50)
				, alle						tinyint NULL				
				, online					tinyint NULL
				, offline					tinyint NULL
				, wien						tinyint NULL
				, admin						tinyint NULL
				, einkauf					tinyint NULL
				, it						tinyint NULL
				, logistik					tinyint NULL
				, HGB_IFRS_Beide			varchar(50)
				)
				
			insert into @KontenplanStufe1
			   select a.KontoNummer
					,Ebene
					,EbenenCode
					,ParentEbenenCode
					,a.Name_DEU
					, k.HGB
					, k.IFRS
					, 'GuV' as GuV_Bilanz
					, 'ZKV' as 'GKV_UKV'
					, 'Controlling'
					, alle
					, online
					, offline
					, wien
					, admin
					, einkauf
					, it
					, logistik
					, case when k.HGB = 1 and k.IFRS = 1 then 'Beide'
						when k.HGB = 1 then 'HGB'
						when k.IFRS = 1 then 'IFRS'
						end
				from [META].GuVOpExControlling a
				 left outer join #NavisionKonten k on a.KontoNummer = k.KontoNummer
				 union all
				 --Für alle Konten die nicht im Kontenplan vorkommen
				Select KontoNummer					
						,Ebene						
						,EbenenCode					
						,ParentEbenenCode			
						,Name_DEU					
						,HGB
						,IFRS
						,GuV_Bilanz					
						,GKV_UKV						
						,Controlling_Accounting	
						, alle
						, online
						, offline
						, wien
						, admin
						, einkauf
						, it
						, logistik
						, HGB_IFRS_Beide
				From(
					  Select KontoNummer = n.KontoNummer
						,Ebene = 1
						,EbenenCode = Null --diese Ebene soll nicht als ParentEbene verwendet werden können
						,ParentEbenenCode = -1 
						,Name_DEU = n.Name_DEU
						,HGB
						,IFRS
						,GuV_Bilanz	= 'GuV'			
						,GKV_UKV =	'ZKV'			
						,Controlling_Accounting	= 'Controlling'
						, alle = 1
						, online = null
						, offline = null
						, wien = null
						, admin = null
						, einkauf = null
						, it = null
						, logistik = null
						, case when n.HGB = 1 and n.IFRS = 1 then 'Beide'
							when n.HGB = 1 then 'HGB'
							when n.IFRS = 1 then 'IFRS'
							end as HGB_IFRS_Beide
						from #NavisionKonten n
						where not exists (select KontoNummer 
									from [META].GuVOpExControlling kp 
									where n.KontoNummer = kp.KontoNummer 
									  )
					  ) v
	
		--DummyKonto für Buchungen, die einem unbekannten Profitcenter zugeordnet sind einführen			  
		insert into @KontenplanStufe1
		select KontoNummer = 9999999
			,Ebene = 1
			,EbenenCode = Null --darf nicht als Parent verwendet werden
			,ParentEbenenCode = -1
			,Name_DEU = 'Dummy unbekannt'
			, HGB = 1
			, IFRS = 1
			, 'GuV' as GuV_Bilanz
			, 'ZKV' as 'GKV_UKV'
			, 'Controlling'
			, alle = 1
			, online = Null
			, offline = Null
			, wien = Null
			, admin = Null
			, einkauf = Null
			, it = Null
			, logistik = Null
			, 'Beide' AS HGB_IFRS_Beide
				  		  
		  
		--Ebene 0 einführen
		insert into @KontenplanStufe1
		select KontoNummer = Null
			,Ebene = 0
			,EbenenCode = 0
			,ParentEbenenCode = null
			,Name_DEU = 'Jahresüberschuss/Jahresfehlbetrag'
			, HGB = null
			, IFRS = null
			, 'GuV' as GuV_Bilanz
			, 'ZKV' as 'GKV_UKV'
			, 'Controlling'
			, alle = 1
			, online = Null
			, offline = Null
			, wien = Null
			, admin = Null
			, einkauf = Null
			, it = Null
			, logistik = Null
			, 'Beide' AS HGB_IFRS_Beide
			
		insert into @KontenplanStufe1
		select KontoNummer = Null
			,Ebene = 0
			,EbenenCode = -1
			,ParentEbenenCode = null
			,Name_DEU = 'n.V.'
			, HGB = null
			, IFRS = null
			, 'GuV' as GuV_Bilanz
			, 'ZKV' as 'GKV_UKV'
			, 'Controlling'
			, alle = 1
			, online = Null
			, offline = Null
			, wien = Null
			, admin = Null
			, einkauf = Null
			, it = Null
			, logistik = Null
			, 'Beide' AS HGB_IFRS_Beide
			
		insert into @KontenplanStufe1
		select KontoNummer = Null
			,Ebene = 0
			,EbenenCode = -2
			,ParentEbenenCode = null
			,Name_DEU = 'nicht EBITDA relevant'
			, HGB = 1
			, IFRS = 1
			, 'GuV' as GuV_Bilanz
			, 'ZKV' as 'GKV_UKV'
			, 'Controlling' as Controlling_Accounting
			, alle = 1
			, online = Null
			, offline = Null
			, wien = Null
			, admin = Null
			, einkauf = Null
			, it = Null
			, logistik = Null
			, 'Beide' AS HGB_IFRS_Beide
		  
		--IFRS_HGB Flag korrekt setzen 
		 update d
			set d.HGB = 1
			, d.IFRS = 0
			, d.HGB_IFRS_Beide = 'HGB'
		from @KontenplanStufe1 d
		where d.HGB is null
		  and d.KontoNummer like '%H'
		  
		update d
			set d.IFRS = 1
			, d.HGB = 0
			, d.HGB_IFRS_Beide = 'IFRS'
		from @KontenplanStufe1 d
		where d.IFRS is null
		  and d.KontoNummer like '%I'
		  
		update d
			set d.IFRS = 1
			, d.HGB = 1
			, d.HGB_IFRS_Beide = 'Beide'
		from @KontenplanStufe1 d
		where HGB is null and IFRS is null
				or (HGB = 0  and IFRS = 0)
		
		--ProfitCenter für Knoten auf 'alle' setzen (falls im Kontenplan falsch angegeben)
		update d
			set d.alle = 1
			, d.wien = null
			, d.online = null
			, d.offline = null
			, d.einkauf = null
			, d.it = null
			, d.logistik = null
		from @KontenplanStufe1 d
			where d.KontoNummer is null
		

		-- Wegen der Kontenplanhistorisierung kann das Profitcenter von Konten nicht mehr 'alle' sein. Stattdessen werden 7 Zeilen mit den jeweiligen PCs erzeugt.
		-- Hilfstabelle zum Aufsplitten von 'alle' erzeugen
		declare @ProfitcenterMapping Table(
			online [int] NULL,
			offline [int] NULL,
			wien [int] NULL,
			admin [int] NULL,
			einkauf [int] NULL,
			it [int] NULL,
			logistik [int] NULL)
			
	
		Insert into @ProfitcenterMapping 
		values (1,Null,Null,Null,Null,Null,Null)
			, (Null,1,Null,Null,Null,Null,Null)
			, (Null,Null,1,Null,Null,Null,Null)
			, (Null,Null,Null,1,Null,Null,Null)
			, (Null,Null,Null,Null,1,Null,Null)
			, (Null,Null,Null,Null,Null,1,Null)
			, (Null,Null,Null,Null,Null,Null,1)
		
		insert into @KontenplanStufe1	
		select KontoNummer
			, Ebene
			, EbenenCode
			, ParentEbenenCode
			, Name_DEU
			, HGB
			, IFRS
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, Null as alle
			, m.online
			, m.offline
			, m.wien
			, m.admin
			, m.einkauf
			, m.it
			, m.logistik
			, HGB_IFRS_Beide
		 from @KontenplanStufe1 t
		 inner join @ProfitcenterMapping m on 1=1
		 where alle = 1
		 and KontoNummer is not null
		 and EbenenCode like '304%' --nur im OPEX Bereich wird es jemals eine Aufsplittung geben

		 delete t
		 from @KontenplanStufe1 t
		 inner join @ProfitcenterMapping m on 1=1
		 where alle = 1
		 and KontoNummer is not null
		 and EbenenCode like '304%'
		

		
		--Kontenplan doppeln für Ebitda und Controlling
		insert into @KontenplanStufe1
		Select KontoNummer					
				, Ebene						
				, EbenenCode					
				, ParentEbenenCode			
				, Name_DEU					
				, HGB
				, IFRS
				, GuV_Bilanz					
				, GKV_UKV
				, 'EBITDA' as Controlling_Accounting
				, alle
				, online
				, offline
				, wien
				, admin
				, einkauf
				, it
				, logistik
				, HGB_IFRS_Beide
		from @KontenplanStufe1
		
		--Für EBITDA Anordnung der Konten ändern
		update d
			set d.ParentEbenenCode = 304006  --"Operating Expenses" - "Other Expenses"
		from @KontenplanStufe1 d
		where d.EbenenCode in(301,313) --"Sonstige betr. Erträge", "Sonstige Steuern"
		and d.Controlling_Accounting = 'EBITDA'
		
		update d
			set d.ParentEbenenCode = -2  --nicht EBITDA relevant
		from @KontenplanStufe1 d
		where d.EbenenCode not in (300, 301, 302, 303, 304, 313, 317) 
		and d.Ebene = 1
		and d.Controlling_Accounting = 'EBITDA'
		
		update d
			set Name_DEU = 'EBITDA'
		--	select *
		from @KontenplanStufe1 d
		where d.EbenenCode = 0
		and d.Controlling_Accounting = 'EBITDA'
		
		
		--Kontenplan aufsplitten für IFRS und HGB
		IF OBJECT_ID('tempdb..#KontenplanStufe2') IS NOT NULL
				DROP TABLE #KontenplanStufe2
		
		Select KontoNummer					
				,Ebene						
				,EbenenCode					
				,ParentEbenenCode			
				,Name_DEU							
				,GuV_Bilanz					
				,GKV_UKV						
				,Controlling_Accounting		
				,HGB_IFRS
				,alle
				,online
				,offline
				,wien
				,admin
				,einkauf
				,it
				,logistik
				,HGB_IFRS_Beide			
		into #KontenplanStufe2
		From(
			  Select * 
			  From (Select * from @KontenplanStufe1) t
			  Unpivot
			  (Status FOR HGB_IFRS in (HGB, IFRS)) AS unpvt
			  ) f
		where f.Status <>0
		
		
		--Kostenplan aufsplitten für ProfitCenter
		IF OBJECT_ID('tempdb..#KontenplanStufe3') IS NOT NULL
				DROP TABLE #KontenplanStufe3
				
		Select KontoNummer										
				,EbenenCode					
				,ParentEbenenCode			
				,Name_DEU							
				,GuV_Bilanz					
				,GKV_UKV						
				,Controlling_Accounting		
				,HGB_IFRS
				,ProfitCenter
				,HGB_IFRS_Beide	
		into #KontenplanStufe3
		from (
		Select * 
			  From (Select * from #KontenplanStufe2) p
			  Unpivot
			  (Status_PC FOR ProfitCenter in (alle, offline, online, wien, admin, einkauf, it, logistik)) AS unpvt
			  ) v
		where v.Status_PC <>0
			  
		--In globale Kontenplaene Tabelle einfügen
		Insert into STG.Kontenplaene 
		Select * from #KontenplanStufe3
		
	END TRY
	
	BEGIN CATCH
	END CATCH
END