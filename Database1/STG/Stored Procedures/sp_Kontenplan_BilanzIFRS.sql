﻿Create PROCEDURE [STG].[sp_Kontenplan_BilanzIFRS]
	 @RUNID			int         = null -- wird von Oberprozedur uebergeben
AS
BEGIN
	BEGIN TRY
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		-- Konten Navision mit Informationen anreichern und wegspeichern
		IF OBJECT_ID('tempdb..#NavisionKonten') IS NOT NULL
		DROP TABLE #NavisionKonten
		
		Select convert(varchar,LTRIM(RTRIM(gla.[No])))                   as [KontoNummer]
			 , max(gla.[Name])											as [Name_DEU]
			 , case when MAX(gla.[Acc_ Pos_ 1])	<>0 then 1 else 0 end		as [HGB]
			 , case when MAX(gla.[Acc_ Pos_ 2])	<>0 then 1 else 0 end		as [IFRS]
		into #NavisionKonten 
		from [stg].[G_L Account] gla
		 left outer join  [META].[GLA_Kontoart]      glaka on gla.[Account Type]   = glaka.ID
		 left outer join [META].[GLA_IncomeBalance] glaib on gla.[Income_Balance] = glaib.ID
		where gla.[Account Type] = 0 --Selektiert nur normale Konten, keine Summenkonten etc.
			and glaib.[Name_DEU] = 'Bilanz'
		group by gla.[No], gla.[Account Type]
		  
		  
		  declare @KontenplanStufe1 as table(
				KontoNummer					nvarchar(255) NULL
				,EbenenCode					bigint
				,ParentEbenenCode			bigint
				,Name_DEU					varchar(255) NULL
				,GuV_Bilanz					varchar(50)
				,GKV_UKV					varchar(50)
				,Controlling_Accounting		varchar(50)
				,HGB_IFRS					varchar(255) NULL
				,Profitcenter				varchar(50)
				,HGB_IFRS_Beide				varchar(50)
				)
				
		  insert into @KontenplanStufe1
			select a.KontoNummer
				, a.EbenenCode
				, a.ParentEbenenCode
				, a.Name_DEU
				, 'Bilanz' as GuV_Bilanz
				, '' as GKV_UKV
				, '' as Controlling_Accounting
				, case when k.IFRS = 1 then 'IFRS' else null end as HGB_IFRS
				, 'alle' as Profitcenter
				, case when k.HGB = 1 and k.IFRS = 1 then 'Beide'
						when k.HGB = 1 then 'HGB'
						when k.IFRS = 1 then 'IFRS'
						end
			from [META].BilanzIFRS a
			 left outer join #NavisionKonten k on a.KontoNummer = k.KontoNummer
			 where k.IFRS = 1 or k.IFRS is null
		  union all
		  --Für alle Konten die nicht im Kontenplan vorkommen
			Select KontoNummer										
					, EbenenCode					
					, ParentEbenenCode			
					, Name_DEU					
					, GuV_Bilanz					
					, GKV_UKV
					, Controlling_Accounting
					, HGB_IFRS
					, 'alle' as Profitcenter	
					, HGB_IFRS_Beide				
			From(
				  Select KontoNummer = n.KontoNummer
					,EbenenCode = Null --diese Ebene soll nicht als ParentEbene verwendet werden
					,ParentEbenenCode = -1
					,Name_DEU = n.Name_DEU
					,case when n.IFRS = 1 then 'IFRS' else null end as HGB_IFRS
					,GuV_Bilanz	= 'Bilanz'			
					,GKV_UKV =	''
					,'' as Controlling_Accounting
					, case when n.HGB = 1 and n.IFRS = 1 then 'Beide'
						when n.HGB = 1 then 'HGB'
						when n.IFRS = 1 then 'IFRS'
						end as HGB_IFRS_Beide
					from #NavisionKonten n
					where not exists (select KontoNummer 
								from [META].BilanzIFRS kp 
								where n.KontoNummer = kp.KontoNummer 
								  )
							and n.IFRS = 1 or n.IFRS is null
				  ) v

			  
			  
  --Oberste Ebene einführen
	insert into @KontenplanStufe1
		select KontoNummer = Null
			,EbenenCode = 0
			,ParentEbenenCode = null
			,Name_DEU = 'Bilanz (IFRS)'
			, 'Bilanz' as GuV_Bilanz
			, '' as 'GKV_UKV'
			, '' as Controlling_Accounting
			, 'IFRS' as HGB_IFRS
			, 'alle' as Profitcenter
			, 'Beide' AS HGB_IFRS_Beide

	insert into @KontenplanStufe1
		select KontoNummer = Null
			,EbenenCode = -1
			,ParentEbenenCode = null
			,Name_DEU = 'n.V.'
			, 'Bilanz' as GuV_Bilanz
			, '' as 'GKV_UKV'
			, '' as Controlling_Accounting
			, 'IFRS' as HGB_IFRS
			, 'alle' as Profitcenter
			, 'Beide' AS HGB_IFRS_Beide

	
	--IFRS_HGB Flag für nicht Navision Konten setzen 
		 update d
			set HGB_IFRS = 'HGB' 
			, d.HGB_IFRS_Beide = 'HGB'
		from @KontenplanStufe1 d
		where d.HGB_IFRS is null
		  and d.KontoNummer like '%H'
		  
		update d
			set HGB_IFRS = 'IFRS' 
			, d.HGB_IFRS_Beide = 'IFRS'
		from @KontenplanStufe1 d
		where d.HGB_IFRS is null
		  and d.KontoNummer like '%I'
		  
		update d
			set HGB_IFRS = 'IFRS'
			, d.HGB_IFRS_Beide = 'Beide'
		from @KontenplanStufe1 d
		where HGB_IFRS is null
		

		Insert into STG.Kontenplaene
		Select * from @KontenplanStufe1

		--Select * from stg_fi.Kontenplaene where GuV_Bilanz = 'Bilanz'


	END TRY
	
	BEGIN CATCH
	END CATCH
	
END