﻿CREATE PROCEDURE [STG].[sp_KontoNamen_sammeln]

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY	
		select *
		into #AlteNamen
		from META.KontoNamen

		truncate Table mdm.KontoNamen

		Insert into META.KontoNamen
		select 
			KontoNummer as KontoNummer
			,max(Name_DEU) as Name_DEU
		From
			(SELECT KontoNummer, rtrim(ltrim(Name_DEU)) as Name_DEU FROM [META].[GuVOpExControlling]
			union all
			Select KontoNummer, rtrim(ltrim(Name_DEU)) as Name_DEU From [META].GuVGKVControlling
			union all
			Select KontoNummer, rtrim(ltrim(Name_DEU)) as Name_DEU From [META].GuVGKVAccounting
			union all
			Select KontoNr, rtrim(ltrim(KontoName)) as Name_DEU From [META].GuVUKVControlling
			union all
			Select KontoNr, rtrim(ltrim(KontoName)) as Name_DEU From [META].GuVUKVAccounting
			union all
			Select KontoNummer, rtrim(ltrim(Name_DEU)) as Name_DEU From [META].BilanzHGB
			union all
			Select KontoNummer, rtrim(ltrim(Name_DEU)) as Name_DEU From [META].BilanzIFRS
			) p
		group by KontoNummer
		having KontoNummer is not null
		;
				  
		merge into [META].KontoNamen m
		using
			(select [No]
						, Max(Name) as Name
					from STG.[G_L Account]
					where MandantID not in (3,4,6) -- Diese Mandanten sind nicht mehr relevant
					group by [No]) nav
			on nav.[No] = m.Kontonummer
		when matched then
			update set m.Name_DEU = nav.[Name]
		when not matched then
			insert (Kontonummer,Name_DEU)
			values ([No],[Name])
		;	
		
		Insert into META.hist_KontoNamenAenderungen (
			Kontonummer
			,Neuer_Name
			,Alter_Name
			,ModDate
			,CurrentFlag
		)
		Select kn.Kontonummer
			, kn.Name_DEU as 'Neuer_Name'
			, ka.Name_DEU as 'Alter_Name'
			, GETDATE() as 'ModDate'
			, 1 as 'CurrentFlag'
		from META.KontoNamen kn
		inner join #AlteNamen ka on ka.Kontonummer = kn.Kontonummer
		where ka.Name_DEU <> kn.Name_DEU
	END TRY
	
	BEGIN CATCH
	END CATCH
END	
	