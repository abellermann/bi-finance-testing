﻿
CREATE PROCEDURE [STG].[sp_Kontenplan_GUVUKVAccounting]

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		IF OBJECT_ID('tempdb..#KontenplanStufe1') IS NOT NULL
		DROP TABLE #KontenplanStufe1
		
		create table #KontenplanStufe1 (
			KontoNummer					nvarchar(255) NULL
			, Ebene						int
			, EbenenCode				bigint
			, ParentEbenenCode			bigint
			, Name_DEU					varchar(255) NULL
			, Kostenstelle				varchar(20)
			, [HGB]						tinyint NULL
			, [IFRS]					tinyint NULL
			, GuV_Bilanz				varchar(50)
			, GKV_UKV					varchar(50)
			, Controlling_Accounting	varchar(50)
			, HGB_IFRS_Beide			varchar(50)
			, Wert						smallint
		)
		
		-- Alle Konten in den Kontenplan eintragen
		insert into #KontenplanStufe1
		select KontoNr as KontoNummer
			, Ebene
			, EbeneCode as EbenenCode
			, ParentEbenenCode
			, KontoName as Name_DEU
			, Kostenstelle
			, null as HGB
			, null as IFRS
			, 'GuV' as GuV_Bilanz
			, 'UKV' as 'GKV_UKV'
			, 'Accounting'
			, null as HGB_IFRS_Beide
			, Wert
		from META.GuVUKVAccounting
		where KontoNr is not null
		
		-- Alle Knoten (jeweils einmal) in den Kontenplan eintragen
		insert into #KontenplanStufe1
		select distinct KontoNr as KontoNummer
			, Ebene
			, EbeneCode as EbenenCode
			, ParentEbenenCode
			, KontoName as Name_DEU
			, 'alle' as Profitcenter
			, null as HGB
			, null as IFRS
			, 'GuV' as GuV_Bilanz
			, 'UKV' as 'GKV_UKV'
			, 'Accounting'
			, null as HGB_IFRS_Beide
			, Wert
		from META.GuVUKVAccounting
		where KontoNr is null
		
		
		-- Nicht zugeordnete KoKoKos in Kontenplan unter Ohne Zuordnung einfügen
        insert into #KontenplanStufe1 (
			Ebene
			, EbenenCode
			, ParentEbenenCode
			, KontoNummer
			, Name_DEU
			, Kostenstelle
			, HGB
			, IFRS
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS_Beide
			, Wert
        )
        select 
			2 as Ebene
			,  -100000 - ROW_NUMBER() OVER(Partition by Kostenstelle order by KontoNr) AS Number --Null as Ebenecode--
			, -100 as ParentEbeneCode
			, KontoNr
			, KontoNr as KontoName
			, Kostenstelle
			, null as HGB
			, null as IFRS
			, 'GuV' as GuV_Bilanz
			, 'UKV' as 'GKV_UKV'
			, 'Accounting'
			, null as HGB_IFRS_Beide
			, 1 as Wert
		from META.GuVUKVAccounting
		where KontoNr is not null
		group by KontoNr, Kostenstelle
		having SUM(wert) = 0
		
		-- Alle unnötigen Zeilen löschen
		delete
		from #KontenplanStufe1
		where Wert = 0
		
		
		-- Im Kontenplan fehlende Kostenstellen samt Konto unter Knoten -300 eintragen 
		select distinct Kostenstelle -- aus Performancegründen zunächst in Temporäre Tabelle
		into #BekannteKostenstellen
		from META.GuVUKVAccounting
		--aus G_L_Entry
		select distinct [Cost Center] as Kostenstelle
		into #UnbekannteKostenstellen
		from STG.[G_L Entry] f
		-- Changed to LEFT JOIN
		-- where [Cost Center] not in (
		--			select Kostenstelle from #BekannteKostenstellen
		--	)

		LEFT JOIN #BekannteKostenstellen b on f.[Cost Center] = b.Kostenstelle
		where b.Kostenstelle is NULL
		
		
		--aus stg-Schweiz
		Insert into #UnbekannteKostenstellen
		select distinct Kostenstelle
		from STG.Buchungen_Schweiz
		where Kostenstelle not in (
					select Kostenstelle from #BekannteKostenstellen
					union all 
					select Kostenstelle from #UnbekannteKostenstellen
			)
		
		select distinct KontoNr as Kontonummer
		into #Konten
		from META.GuVUKVAccounting
		
		insert into #KontenplanStufe1 (
			Ebene
			, EbenenCode
			, ParentEbenenCode
			, KontoNummer
			, Name_DEU
			, Kostenstelle
			, HGB
			, IFRS
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS_Beide
        )
        select 
			2 as Ebene
			, Null as EbenenCode
			, -300 as ParentEbenenCode
			, ko.KontoNummer
			, ko.KontoNummer as KontoName
			, ks.Kostenstelle
			, null as HGB
			, null as IFRS
			, 'GuV' as GuV_Bilanz
			, 'UKV' as 'GKV_UKV'
			, 'Accounting'
			, null as HGB_IFRS_Beide
		from #Konten ko
		inner join #UnbekannteKostenstellen ks on 1=1
		
					
		-- Kontenplan mit Navisioninformation anreichern
		IF OBJECT_ID('tempdb..#NavisionKonten') IS NOT NULL
		DROP TABLE #NavisionKonten
		
		Select convert(varchar,LTRIM(RTRIM(gla.[No])))               as [KontoNummer]
			, max(gla.[Name])											as [Name_DEU]
			, case when MAX(gla.[Acc_ Pos_ 1])	<>0 then 1 else 0 end	as [HGB]
			, case when MAX(gla.[Acc_ Pos_ 2])	<>0 then 1 else 0 end	as [IFRS]
		into #NavisionKonten 
		from [STG].[G_L Account] gla
		left outer join [META].[GLA_Kontoart]      glaka on gla.[Account Type]   = glaka.ID
		left outer join [META].[GLA_IncomeBalance] glaib on gla.[Income_Balance] = glaib.ID
		where gla.[Account Type] = 0 --Selektiert nur normale Konten, keine Summenkonten etc.
		  and glaib.[Name_DEU] = 'GuV'
		group by gla.[No], gla.[Account Type]
	
		update kp
			set kp.HGB = na.HGB
			, kp.IFRS = na.IFRS
			, kp.HGB_IFRS_Beide =  case when na.HGB = 1 and na.IFRS = 1 then 'Beide'
									when na.HGB = 1 then 'HGB'
									when na.IFRS = 1 then 'IFRS'
									end
		from #KontenplanStufe1 kp
		left outer join #NavisionKonten na 
			 on na.KontoNummer = kp.KontoNummer collate database_default
		
		 -- Im Kontenplan fehlende Konten aus Navision nachtragen (mit Profitcenter 'alle')
        insert into #KontenplanStufe1 (
			Ebene
			, EbenenCode
			, ParentEbenenCode
			, KontoNummer
			, Name_DEU
			, Kostenstelle
			, HGB
			, IFRS
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS_Beide
        )
        select 
			2 as Ebene
			, Null as EbenenCode
			, -200 as ParentEbenenCode
			, KontoNummer
			, KontoNummer as KontoName
			, 'alle' as Kostenstelle
			, HGB
			, IFRS
			, 'GuV' as GuV_Bilanz
			, 'UKV' as 'GKV_UKV'
			, 'Accounting'
			, case when HGB = 1 and IFRS = 1 then 'Beide'
					when HGB = 1 then 'HGB'
					when IFRS = 1 then 'IFRS'
					end as HGB_IFRS_Beide
		from #NavisionKonten na
		where not exists (select KontoNummer 
							from [META].GuVUKVAccounting kp 
							where na.KontoNummer = kp.KontoNr )
			
		-- fehlende Überebenen einführen
		insert into #KontenplanStufe1 (
			Ebene
			, EbenenCode
			, ParentEbenenCode
			, KontoNummer
			, Name_DEU
			, Kostenstelle
			, Wert
			, GKV_UKV
			, GuV_Bilanz
			, Controlling_Accounting
			, HGB
			, IFRS
			, HGB_IFRS_Beide
        )
        values(0 , 0 , Null , Null , 'Jahresüberschuss/Jahresfehlbetrag',  'alle', Null, 'UKV', 'GuV', 'Accounting', 1, 1, 'Beide' )
			, (0 , -1 , Null , Null , 'n.V.',  'alle', Null, 'UKV', 'GuV', 'Accounting', Null, Null, 'Beide' )
			, (0 , -2 , Null , Null , 'nicht EBITDA relevant',  'alle', Null, 'UKV', 'GuV', 'Accounting', 1, 1, 'Beide' )
			-- ÜberEbene für Konten mit nicht vorhandener KoKoko
			, (1 , -100 , -1 , Null , 'Ohne Zuordnung im Kontenplan',  'alle', Null, 'UKV', 'GuV', 'Accounting', 1, 1, 'Beide' )
			-- ÜberEbene für im KP fehlende Konten
			, (1 , -200 , -1 , Null , 'Konten fehlen im Kontenplan',  'alle', Null, 'UKV', 'GuV', 'Accounting', 1, 1, 'Beide' )
			-- ÜberEbene für im KP fehlende Kostenstellen
			, (1 , -300 , -1 , Null , 'Kostenstellen fehlen im Kontenplan',  'alle', Null, 'UKV', 'GuV', 'Accounting', 1, 1, 'Beide' )
			-- DummyKonto für Buchungen, die einem unbekannten Profitcenter zugeordnet sind einführen
			--, (1 , Null , -1 , 9999999 , 'Fehlende Profitcenterzuordnung',  'alle', Null, 'UKV', 'GuV', 'Accounting', 1, 1, 'Beide' )
			
		--IFRS_HGB Flag korrekt setzen (falls Fehler in Navision)
		update d
			set d.HGB = 1
			, d.IFRS = 0
			, d.HGB_IFRS_Beide = 'HGB'
		from #KontenplanStufe1 d
		where d.HGB is null
		  and d.KontoNummer like '%H'
		  
		update d
			set d.IFRS = 1
			, d.HGB = 0
			, d.HGB_IFRS_Beide = 'IFRS'
		from #KontenplanStufe1 d
		where d.IFRS is null
		  and d.KontoNummer like '%I'
		  
		update d
			set d.IFRS = 1
			, d.HGB = 1
			, d.HGB_IFRS_Beide = 'Beide'
		from #KontenplanStufe1 d
		where HGB is null and IFRS is null
				or (HGB = 0  and IFRS = 0)

		
   		Insert into STG.Kontenplaene (
			 [KontoNummer] 
			, [EbenenCode] 
			, [ParentEbenenCode] 
			, [Name_DEU] 
			, [GuV_Bilanz] 
			, [GKV_UKV] 
			, [Controlling_Accounting] 
			, [HGB_IFRS] 
			, [ProfitCenter] 
			,[HGB_IFRS_Beide])
		Select KontoNummer
			, EbenenCode
			, ParentEbenenCode
			, Name_DEU
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS
			, Kostenstelle as Profitcenter -- ACHTUNG!! NICHT SCHÖN! Profitcenter Name ändern?
			, HGB_IFRS_Beide
		from (
			  Select * 
			  From (Select * from #KontenplanStufe1) t
			  Unpivot
			  (Status FOR HGB_IFRS in (HGB, IFRS)) AS unpvt
			  ) f
		where f.Status <>0
		
		

	END TRY
	
	BEGIN CATCH
	
	END CATCH
	
END

