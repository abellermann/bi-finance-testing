﻿CREATE TABLE [INPUT].[G_L Account] (
    [TS_INT]         BIGINT       NOT NULL,
    [MandantID]      INT          NOT NULL,
    [No_]            VARCHAR (20) NOT NULL,
    [Name]           VARCHAR (30) NOT NULL,
    [Account Type]   INT          NOT NULL,
    [Income_Balance] INT          NOT NULL,
    [Debit_Credit]   INT          NOT NULL,
    [Name 2]         VARCHAR (30) NOT NULL,
    [Referenz Text]  VARCHAR (30) NOT NULL,
    [Name AT]        VARCHAR (30) NOT NULL,
    [Name ES]        VARCHAR (30) NOT NULL,
    [Currency Code]  VARCHAR (10) NOT NULL,
    [Acc_ Pos_ 1]    VARCHAR (20) NOT NULL,
    [Acc_ Pos_ 2]    VARCHAR (20) NOT NULL,
    [Acc_ Pos_ 3]    VARCHAR (20) NOT NULL,
    [Acc_ Pos_ 4]    VARCHAR (20) NOT NULL,
    [LoadID]         INT          NOT NULL,
    [LoadDate]       INT          NOT NULL,
    [ModifiedLoadID] INT          NULL,
    [ModifiedDate]   INT          NULL,
    CONSTRAINT [PK_G_L Account] PRIMARY KEY CLUSTERED ([MandantID] ASC, [No_] ASC)
);

