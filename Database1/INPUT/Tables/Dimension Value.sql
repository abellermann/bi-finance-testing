﻿CREATE TABLE [INPUT].[Dimension Value] (
    [TS_INT]         BIGINT       NOT NULL,
    [MandantID]      INT          NOT NULL,
    [Dimension Code] VARCHAR (20) NOT NULL,
    [Code]           VARCHAR (20) NOT NULL,
    [Name]           VARCHAR (50) NOT NULL,
    [LoadID]         INT          NOT NULL,
    [LoadDate]       INT          NOT NULL,
    [ModifiedLoadID] INT          NULL,
    [ModifiedDate]   INT          NULL,
    CONSTRAINT [PK_Dimension Value] PRIMARY KEY CLUSTERED ([MandantID] ASC, [Dimension Code] ASC, [Code] ASC)
);

