﻿CREATE TABLE [INPUT].[G_L Entry] (
    [TS_INT]                         BIGINT           NOT NULL,
    [PART_KEY]                       INT              NULL,
    [MandantID]                      INT              NOT NULL,
    [Entry No]                       INT              NOT NULL,
    [G_L Account No]                 VARCHAR (20)     NOT NULL,
    [Posting Date]                   DATETIME         NOT NULL,
    [Document Type]                  INT              NOT NULL,
    [Document No]                    VARCHAR (20)     NOT NULL,
    [Description]                    VARCHAR (50)     NOT NULL,
    [Bal_ Account No]                VARCHAR (20)     NOT NULL,
    [Amount]                         DECIMAL (38, 20) NOT NULL,
    [Cost Center]                    VARCHAR (20)     NOT NULL,
    [User ID]                        VARCHAR (20)     NOT NULL,
    [Source Code]                    VARCHAR (10)     NOT NULL,
    [System-Created Entry]           TINYINT          NOT NULL,
    [Prior-Year Entry]               TINYINT          NOT NULL,
    [Reason Code]                    VARCHAR (10)     NOT NULL,
    [Gen_ Posting Type]              INT              NOT NULL,
    [Gen_ Bus_ Posting Group]        VARCHAR (10)     NOT NULL,
    [Gen_ Prod_ Posting Group]       VARCHAR (10)     NOT NULL,
    [Bal_ Account Type]              INT              NOT NULL,
    [Transaction No_]                INT              NOT NULL,
    [Document Date]                  DATETIME         NOT NULL,
    [Source Type]                    INT              NOT NULL,
    [Source No_]                     VARCHAR (20)     NOT NULL,
    [Additional-Currency Amount]     DECIMAL (38, 20) NOT NULL,
    [Close Income Statement Dim_ ID] INT              NOT NULL,
    [Amount (LCY)]                   DECIMAL (38, 20) NOT NULL,
    [Orig_ Currency Code]            VARCHAR (10)     NOT NULL,
    [Original Amount (FCY)]          DECIMAL (38, 20) NOT NULL,
    [Amount (FCY)]                   DECIMAL (38, 20) NOT NULL,
    [Acc_ Pos_ 1]                    VARCHAR (20)     NOT NULL,
    [Acc_ Pos_ 2]                    VARCHAR (20)     NOT NULL,
    [Acc_ Pos_ 3]                    VARCHAR (20)     NOT NULL,
    [Acc_ Pos_ 4]                    VARCHAR (20)     NOT NULL,
    [CreationDate]                   DATETIME         NULL,
    [LoadID]                         INT              NOT NULL,
    [LoadDate]                       INT              NOT NULL,
    [ModifiedLoadID]                 INT              NULL,
    [ModifiedDate]                   INT              NULL,
    CONSTRAINT [PK G_L Entry] PRIMARY KEY NONCLUSTERED ([MandantID] ASC, [Entry No] ASC) ON [PRIMARY]
) ON [PS_by_Year] ([PART_KEY]);


GO
CREATE NONCLUSTERED INDEX [Mandant_CostCenter]
    ON [INPUT].[G_L Entry]([MandantID] ASC, [Cost Center] ASC)
    ON [PS_by_Year] ([PART_KEY]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170207-162604]
    ON [INPUT].[G_L Entry]([MandantID] ASC)
    ON [PS_by_Year] ([PART_KEY]);

