﻿CREATE TABLE [INPUT].[G_L Register] (
    [TS_INT]         BIGINT   NOT NULL,
    [MandantID]      INT      NOT NULL,
    [No_]            INT      NOT NULL,
    [From Entry No_] INT      NOT NULL,
    [To Entry No_]   INT      NOT NULL,
    [Creation Date]  DATETIME NOT NULL,
    [LoadID]         INT      NOT NULL,
    [LoadDate]       INT      NOT NULL,
    [ModifiedLoadID] INT      NULL,
    [ModifiedDate]   INT      NULL,
    CONSTRAINT [PK_G_L Register] PRIMARY KEY CLUSTERED ([MandantID] ASC, [No_] ASC)
);

