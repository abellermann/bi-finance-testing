﻿Create VIEW INPUT.WaWi_Summen as 

			select ID_Monat
				, ID_Retoure
				, VERKAUFSKANAL
				, Lager
				, Lagergroup
				, Lieferland
				, EK_Wert
				, VK_Wert
				, Menge
			from [sbo-bis].[SchuboKI_BI].dbo.vWawiSummen_FiBuExport