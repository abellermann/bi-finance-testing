﻿CREATE TABLE [META].[Company] (
    [id]               INT           IDENTITY (1, 1) NOT NULL,
    [MandantNAV]       VARCHAR (50)  NULL,
    [MandantName]      VARCHAR (100) NULL,
    [Gruppenname]      VARCHAR (50)  NULL,
    [G_L Account PRIO] INT           NULL,
    [NAVISION]         TINYINT       NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([id] ASC)
);

