﻿CREATE TABLE [META].[Allowance] (
    [Monat]                INT              NULL,
    [d999001]              DECIMAL (38, 20) NULL,
    [d999003]              DECIMAL (36, 20) NULL,
    [ID]                   INT              NOT NULL,
    [ModDate]              DATETIME         NULL,
    [Rechnungslegungsnorm] VARCHAR (10)     NULL
);

