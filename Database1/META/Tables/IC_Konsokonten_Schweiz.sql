﻿CREATE TABLE [META].[IC_Konsokonten_Schweiz] (
    [Kontonummer] NVARCHAR (10) NULL,
    [KonsoKonto]  NVARCHAR (50) NULL,
    [ID]          INT           NOT NULL,
    [ModDate]     DATETIME      NULL
);

