﻿CREATE TABLE [META].[Bilanz_Kippkonten] (
    [KontoAlt]     VARCHAR (255) NULL,
    [KontoNeu]     VARCHAR (255) NULL,
    [Beschreibung] VARCHAR (255) NULL,
    [Vorzeichen]   VARCHAR (10)  NULL,
    [Bilanz]       VARCHAR (255) NULL,
    [Knotenpunkt]  VARCHAR (255) NULL,
    [kippen_bis]   INT           NULL,
    [ID]           INT           NOT NULL,
    [ModDate]      DATETIME      NULL
);

