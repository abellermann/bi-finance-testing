﻿CREATE TABLE [META].[IC_Konsokonten] (
    [KontoNummer]  NVARCHAR (10)  NULL,
    [KonsoKonto]   NVARCHAR (50)  NULL,
    [Beschreibung] NVARCHAR (255) NULL,
    [Mandant]      NVARCHAR (255) NULL,
    [ID]           INT            NOT NULL,
    [ModDate]      DATETIME       NULL
);

