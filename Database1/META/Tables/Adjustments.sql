﻿CREATE TABLE [META].[Adjustments] (
    [Mandant]          VARCHAR (50)     NULL,
    [Monat]            INT              NULL,
    [von_Konto]        VARCHAR (20)     NULL,
    [auf_Konto]        VARCHAR (20)     NULL,
    [Wert]             DECIMAL (38, 20) NULL,
    [Beschreibung]     VARCHAR (255)    NULL,
    [ID]               INT              NOT NULL,
    [ModDate]          DATETIME         NULL,
    [von_Kostenstelle] VARCHAR (20)     NULL,
    [auf_Kostenstelle] VARCHAR (20)     NULL,
    [Typ]              VARCHAR (50)     NULL
);

