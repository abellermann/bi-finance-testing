﻿CREATE TABLE [META].[BilanzHGB] (
    [Ebene]            INT            NOT NULL,
    [EbenenCode]       BIGINT         NOT NULL,
    [ParentEbenenCode] BIGINT         NULL,
    [Name_DEU]         NVARCHAR (255) NULL,
    [KontoNummer]      NVARCHAR (255) NULL
);

