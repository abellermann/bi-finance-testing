﻿CREATE TABLE [META].[Kostenstelle_Grobfunktionsbereich] (
    [KST_56]             CHAR (2)     NULL,
    [KST_56_Bezeichnung] VARCHAR (50) NULL,
    [ID]                 INT          NOT NULL,
    [ModDate]            DATETIME     NULL
);

