﻿CREATE TABLE [META].[Kontenplan_Bezeichner] (
    [Bezeichner_ID]          CHAR (1)     NULL,
    [GuV_Bilanz]             VARCHAR (50) NOT NULL,
    [GKV_UKV]                VARCHAR (50) NOT NULL,
    [HGB_IFRS]               VARCHAR (50) NOT NULL,
    [Controlling_Accounting] VARCHAR (50) NOT NULL
);

