﻿CREATE TABLE [META].[Kostenstelle_Gesellschaft] (
    [KST_12]             CHAR (2)     NULL,
    [KST_12_Bezeichnung] VARCHAR (50) NULL,
    [ID]                 INT          NOT NULL,
    [ModDate]            DATETIME     NULL
);

