﻿CREATE TABLE [META].[Kontoumbuchungen] (
    [KontoAlt]     VARCHAR (6)   NULL,
    [KontoNeu]     VARCHAR (6)   NULL,
    [MandantenID]  INT           NULL,
    [KontoNeuSeit] INT           NULL,
    [Beschreibung] VARCHAR (255) NULL
);

