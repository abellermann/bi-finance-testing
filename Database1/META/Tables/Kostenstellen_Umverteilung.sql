﻿CREATE TABLE [META].[Kostenstellen_Umverteilung] (
    [KontoNummer]      NVARCHAR (255)  NULL,
    [Kostenstelle_von] VARCHAR (20)    NULL,
    [Kostenstelle_zu]  VARCHAR (20)    NULL,
    [Anteil]           DECIMAL (10, 9) NULL,
    [ID]               INT             NOT NULL,
    [ModDate]          DATETIME        NULL
);

