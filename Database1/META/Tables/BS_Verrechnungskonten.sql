﻿CREATE TABLE [META].[BS_Verrechnungskonten] (
    [Kontonummer] NVARCHAR (10) NULL,
    [ID]          INT           NOT NULL,
    [ModDate]     DATETIME      NULL,
    [KonsoKonto]  NVARCHAR (50) NULL
);

