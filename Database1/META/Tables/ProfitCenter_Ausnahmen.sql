﻿CREATE TABLE [META].[ProfitCenter_Ausnahmen] (
    [Ebene]      INT             NULL,
    [EbenenCode] VARCHAR (255)   NULL,
    [von]        VARCHAR (20)    NULL,
    [zu]         VARCHAR (20)    NULL,
    [Anteil]     DECIMAL (10, 9) NULL,
    [Monat]      INT             NULL,
    [ID]         INT             NOT NULL,
    [ModDate]    DATETIME        NULL
);

