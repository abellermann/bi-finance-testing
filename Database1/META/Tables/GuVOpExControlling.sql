﻿CREATE TABLE [META].[GuVOpExControlling] (
    [Ebene]            INT            NOT NULL,
    [EbenenCode]       BIGINT         NOT NULL,
    [ParentEbenenCode] BIGINT         NULL,
    [Name_DEU]         NVARCHAR (255) NULL,
    [KontoNummer]      NVARCHAR (255) NULL,
    [alle]             TINYINT        NULL,
    [online]           TINYINT        NULL,
    [offline]          TINYINT        NULL,
    [wien]             TINYINT        NULL,
    [admin]            TINYINT        NULL,
    [einkauf]          TINYINT        NULL,
    [it]               TINYINT        NULL,
    [logistik]         TINYINT        NULL
);

