﻿CREATE TABLE [META].[Kostenstellen] (
    [Profit_Center] VARCHAR (20) NULL,
    [Kostenstelle]  VARCHAR (20) NULL,
    [Mandant]       VARCHAR (50) NULL,
    [ID]            INT          NOT NULL,
    [ModDate]       DATETIME     NULL
);

