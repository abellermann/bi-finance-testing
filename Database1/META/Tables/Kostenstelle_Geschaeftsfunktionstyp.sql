﻿CREATE TABLE [META].[Kostenstelle_Geschaeftsfunktionstyp] (
    [KST_34]             CHAR (2)     NULL,
    [KST_34_Bezeichnung] VARCHAR (50) NULL,
    [ID]                 INT          NOT NULL,
    [ModDate]            DATETIME     NULL
);

