﻿CREATE TABLE [META].[Kontenplan_abweichende_Sortierung] (
    [Financial_Statement]  VARCHAR (50)   NULL,
    [Rechnungslegungsnorm] VARCHAR (50)   NULL,
    [Level]                INT            NULL,
    [KontoID]              NVARCHAR (255) NULL,
    [Sortierung]           NVARCHAR (255) NULL,
    [Kostenverfahren]      VARCHAR (50)   NULL,
    [Funktionsbereich]     VARCHAR (50)   NULL
);

