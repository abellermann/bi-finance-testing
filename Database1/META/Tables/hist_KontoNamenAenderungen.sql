﻿CREATE TABLE [META].[hist_KontoNamenAenderungen] (
    [Kontonummer] VARCHAR (50)  NULL,
    [Neuer_Name]  VARCHAR (255) NULL,
    [Alter_Name]  VARCHAR (255) NULL,
    [ModDate]     DATETIME      NULL,
    [CurrentFlag] TINYINT       NULL
);

