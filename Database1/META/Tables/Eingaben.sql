﻿CREATE TABLE [META].[Eingaben] (
    [Beschreibung] NVARCHAR (50) NULL,
    [Wert]         INT           NULL,
    [ID]           INT           NOT NULL,
    [ModDate]      DATETIME      NULL
);

