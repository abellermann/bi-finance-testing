﻿CREATE TABLE [META].[Kapitalkonsolidierung] (
    [ID]           INT              NULL,
    [ModDate]      DATETIME         NULL,
    [Mandant]      VARCHAR (50)     NULL,
    [Kontonummer]  VARCHAR (20)     NULL,
    [Kostenstelle] VARCHAR (20)     NULL,
    [von_Monat]    INT              NULL,
    [bis_Monat]    INT              NULL,
    [Amount]       DECIMAL (38, 20) NULL
);

