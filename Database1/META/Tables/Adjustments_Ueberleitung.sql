﻿CREATE TABLE [META].[Adjustments_Ueberleitung] (
    [ID]                INT             NOT NULL,
    [ModDate]           DATETIME        NULL,
    [Jahr]              INT             NULL,
    [Mandant]           VARCHAR (50)    NULL,
    [Kontonummer]       VARCHAR (20)    NULL,
    [von_Kostenstelle]  VARCHAR (20)    NULL,
    [auf_Kostenstelle]  VARCHAR (20)    NULL,
    [von_Anteil]        DECIMAL (10, 9) NULL,
    [auf_Anteil]        DECIMAL (10, 9) NULL,
    [Ueberleitung_fuer] INT             NULL,
    [Beschreibung]      VARCHAR (255)   NULL
);

