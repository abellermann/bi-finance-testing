﻿CREATE TABLE [META].[GuVUKVAccounting] (
    [Ebene]            SMALLINT       NULL,
    [EbeneCode]        BIGINT         NULL,
    [ParentEbenenCode] BIGINT         NULL,
    [KontoNr]          NVARCHAR (50)  NULL,
    [KontoName]        NVARCHAR (255) NULL,
    [Kostenstelle]     VARCHAR (20)   NULL,
    [Wert]             SMALLINT       NULL
);

