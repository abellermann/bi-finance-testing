﻿CREATE TABLE [META].[GuVGKVControlling] (
    [Ebene]            INT            NOT NULL,
    [EbenenCode]       BIGINT         NOT NULL,
    [ParentEbenenCode] BIGINT         NULL,
    [Name_DEU]         NVARCHAR (255) NULL,
    [KontoNummer]      NVARCHAR (255) NULL
);

