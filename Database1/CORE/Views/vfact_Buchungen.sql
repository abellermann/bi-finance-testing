﻿CREATE VIEW [core].[vfact_Buchungen]
AS

SELECT isnull(a.MandantEntryNo, cast(a.MandantenID as varchar) + '-9999999') as MandantEntryNo
      ,a.[KontoNummer]
      ,convert(int,substring(convert(varchar,a.[PostingDateID]),1,6)) as PostingMonat
      ,a.Amount
      ,a.Currency
      ,a.Amount_EUR
      ,a.[MandantenID]
      ,a.ID_KP_Bridge
      ,a.KostenstellenID
      ,a.LevelID
      , isnull(a.OneOffID, 0) as OneOffID
      , 0 as AdjustmentID
      , ISNULL(a.ProfitCenterID, -1) as ProfitCenterID
      , coalesce(a.ProfitCenterID_2, a.ProfitCenterID, -1) ProfitCenterID_2
	  , coalesce(a.ProfitCenterID_3, a.ProfitCenterID_2,a.ProfitCenterID, -1) ProfitCenterID_3
      , isnull(a.CapexItID, 0) as CapexItID
      , ID_Nachbuchungen
      , case when EingabeDatumId = 19900101 then 19980101 else ISNULL(EingabeDatumId, 19980102) end as EingabeDatumId
      , 0 as KonsolidierungsID
FROM core.fact_Buchungen a
inner join core.dim_Kontenplan_Bridge b on b.ID_KP_Bridge = a.ID_KP_Bridge
where a.[PostingDateID] between 19979999 and convert(int,convert(varchar(8), eomonth(getdate()), 112))	-- Immer nur bis zum aktuellem Monat anzeigen
	and ((b.GuV_Bilanz = 'GuV' and a.Ultimobuchung = 'N') or b.GuV_Bilanz = 'Bilanz') -- bei GuV dürfen die Ultimobuchungen nicht berücksichtigt werden
	and b.Monat_gueltig = 999999
	
union all 

Select isnull(b.MandantEntryNo, cast(b.MandantenID as varchar) + '-9999999') as MandantEntryNo
	, b.KontoNummer
	, b.PostingMonat
	, b.Amount
	, b.Currency
	, b.Amount_EUR
	, b.MandantenID
	, b.ID_KP_Bridge
	, b.KostenstellenID
	, b.LevelID
	, isnull(OneOffID, 0) as OneOffID
	, ISNULL(AdjustmentID, 0) as AdjustmentID
    , ISNULL(ProfitCenterID, 2) as ProfitCenterID -- ggf. auf offline
    , coalesce(b.ProfitCenterID_2, b.ProfitCenterID, 2) ProfitCenterID_2
	, coalesce(b.ProfitCenterID_3, b.ProfitCenterID_2,b.ProfitCenterID, 2) ProfitCenterID_3
    , isnull(CapexItID, 0) as CapexItID
    , ID_Nachbuchungen
    , case when EingabeDatumId = 19900101 then 19980101 else ISNULL(EingabeDatumId, 19980102) end as EingabeDatumId
    , isnull(KonsolidierungsID,0) as KonsolidierungsID
from core.fact_Zusatzbuchungen b
where (b.LevelID = core.Get_dim_LevelID('Planung')
   or b.PostingMonat between 199799 and convert(int,convert(varchar(8),getdate(),112))/100) -- Planung vollständig, Rest nur bis zum aktuellen Monat anzeigen

