﻿CREATE VIEW [core].[vdim_Kostenstelle]
AS
select 
	ID
    , Code
    , KST_12
	, KST_34
	, case when SUBSTRING(Code, 3, 4) = '5034' then '34 - Zentrallager' else KST_56 end as KST_56
    , Code + ' - '  + Name as Kostenstelle
    , MandantenName
from core.[dim_Dimension Value]
where [Dimension Code] = 'KOSTENSTELLE'