﻿CREATE VIEW [core].[vfact_Bilanz]
AS
SELECT ID_KP_Bridge
      , KontoNummer
      , Monat as PostingMonat
      , Amount
      , Currency
      , Amount_EUR
      , LOAD_DATE
      , MandantenID
      , LevelID
      , ID_Nachbuchungen
      , isnull(a.OneOffID, 0) as OneOffID
      , isnull(KonsolidierungsID, 0) as KonsolidierungsID
FROM core.fact_Bilanz a
where Monat > 199799