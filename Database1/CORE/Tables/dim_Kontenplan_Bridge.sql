﻿CREATE TABLE [CORE].[dim_Kontenplan_Bridge] (
    [ID_KP_Bridge]  INT            NOT NULL,
    [ProfitCenter]  VARCHAR (55)   NULL,
    [KontoNummer]   NVARCHAR (255) NULL,
    [KontoID_Level] VARCHAR (2)    NULL,
    [ID_Kontenplan] INT            NOT NULL,
    [GuV_Bilanz]    NVARCHAR (55)  NULL,
    [GKV_UKV]       VARCHAR (50)   NULL,
    [HGB_IFRS]      VARCHAR (50)   NULL,
    [Monat_gueltig] INT            NULL
)
WITH (DATA_COMPRESSION = ROW);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170207-115934]
    ON [CORE].[dim_Kontenplan_Bridge]([Monat_gueltig] ASC, [GuV_Bilanz] ASC);

