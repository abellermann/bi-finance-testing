﻿CREATE TABLE [CORE].[dim_Dimension Value] (
    [ID]             INT          IDENTITY (1, 1) NOT NULL,
    [Dimension Code] VARCHAR (20) NOT NULL,
    [Code]           VARCHAR (20) NOT NULL,
    [Name]           VARCHAR (50) NOT NULL,
    [LOAD_DATE]      DATETIME     NULL,
    [MandantID]      INT          NULL,
    [MandantenName]  VARCHAR (50) NOT NULL,
    [ProfitCenterID] INT          NULL,
    [KST_12]         VARCHAR (55) NULL,
    [KST_34]         VARCHAR (57) NULL,
    [KST_56]         VARCHAR (59) NULL
);

