﻿CREATE TABLE [CORE].[dim_ProfitCenter] (
    [ID]                         INT          NOT NULL,
    [ProfitCenter]               VARCHAR (30) NULL,
    [ProfitCenter_kurz]          VARCHAR (20) NULL,
    [ProfitCenter_gruppe]        VARCHAR (20) NULL,
    [ProfitCenter_zwischen]      VARCHAR (30) NULL,
    [ProfitCenter_zwischen_kurz] VARCHAR (20) NULL
);

