﻿CREATE TABLE [CORE].[Zusatzbuchungen_intermediate] (
    [Amount]            DECIMAL (38, 20) NULL,
    [EntryNo]           INT              NULL,
    [MandantenID]       INT              NULL,
    [KostenstellenID]   INT              NULL,
    [PostingMonat]      INT              NULL,
    [MS_AUDIT_TIME]     DATETIME         NULL,
    [MS_AUDIT_USER]     NVARCHAR (255)   NULL,
    [KontoNummer]       VARCHAR (255)    NULL,
    [LevelID]           INT              NULL,
    [OneOffID]          TINYINT          NULL,
    [AdjustmentID]      INT              NULL,
    [ProfitCenterID]    INT              NULL,
    [CapexItID]         INT              NULL,
    [MandantEntryNo]    VARCHAR (15)     NULL,
    [ProfitCenterID_2]  INT              NULL,
    [ProfitCenterID_3]  INT              NULL,
    [EingabeDatumID]    INT              NULL,
    [KonsolidierungsID] INT              NULL,
    [Currency]          VARCHAR (5)      NULL,
    [Amount_EUR]        DECIMAL (38, 20) NULL
);

