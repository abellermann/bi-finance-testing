﻿CREATE TABLE [CORE].[fact_Zusatzbuchungen] (
    [Amount]            DECIMAL (38, 20) NULL,
    [EntryNo]           INT              NULL,
    [MandantenID]       INT              NULL,
    [KostenstellenID]   INT              NULL,
    [PostingMonat]      INT              NULL,
    [ID_KP_Bridge]      INT              NULL,
    [MS_AUDIT_TIME]     DATETIME         NULL,
    [MS_AUDIT_USER]     NVARCHAR (255)   NULL,
    [KontoNummer]       VARCHAR (255)    NULL,
    [LevelID]           INT              NULL,
    [OneOffID]          TINYINT          NULL,
    [AdjustmentID]      INT              NULL,
    [ProfitCenterID]    INT              NULL,
    [CapexItID]         INT              NULL,
    [MandantEntryNo]    VARCHAR (15)     NULL,
    [ProfitCenterID_2]  INT              NULL,
    [ProfitCenterID_3]  INT              NULL,
    [ID_Nachbuchungen]  INT              NULL,
    [EingabeDatumId]    INT              NULL,
    [KonsolidierungsID] INT              NULL,
    [Amount_EUR]        DECIMAL (38, 20) NULL,
    [Currency]          VARCHAR (5)      NULL
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170207-120026]
    ON [CORE].[fact_Zusatzbuchungen]([ID_KP_Bridge] ASC);

