﻿CREATE TABLE [CORE].[dim_OneOffs] (
    [OneOffID]           TINYINT        NOT NULL,
    [OneOffCode]         CHAR (1)       NULL,
    [OneOffBeschreibung] NVARCHAR (200) NULL
);

