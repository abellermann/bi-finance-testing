﻿CREATE TABLE [CORE].[allokationsSchluessel_ProfitCenter] (
    [PostingMonat]       INT              NULL,
    [von_ProfitCenterID] INT              NULL,
    [zu_ProfitCenterID]  INT              NULL,
    [Faktor]             DECIMAL (38, 20) NULL
);

