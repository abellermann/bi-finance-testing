﻿CREATE TABLE [CORE].[fact_Buchungen] (
    [ID]               INT              NOT NULL,
    [EntryNo]          INT              NULL,
    [KontoNummer]      VARCHAR (255)    NULL,
    [KontoNummer_Alt]  VARCHAR (255)    NULL,
    [PostingDateID]    INT              NULL,
    [Amount]           DECIMAL (38, 20) NULL,
    [GegenKontoNummer] CHAR (6)         NULL,
    [LOAD_DATE]        DATETIME         NULL,
    [MandantenID]      INT              NULL,
    [Ultimobuchung]    NCHAR (1)        NULL,
    [KostenstellenID]  INT              NULL,
    [OneOffID]         INT              NULL,
    [LevelID]          INT              NULL,
    [Nullstellung]     BIT              NULL,
    [ProfitCenterID]   INT              NULL,
    [CapexItID]        INT              NULL,
    [MandantEntryNo]   VARCHAR (15)     NULL,
    [ID_KP_Bridge]     INT              NULL,
    [ProfitCenterID_2] INT              NULL,
    [ProfitCenterID_3] INT              NULL,
    [ID_Nachbuchungen] INT              NULL,
    [EingabeDatumId]   INT              NULL,
    [Amount_EUR]       DECIMAL (38, 20) NULL,
    [Currency]         VARCHAR (5)      NULL
)
WITH (DATA_COMPRESSION = ROW);


GO
CREATE NONCLUSTERED INDEX [ID_KP_Bridge]
    ON [CORE].[fact_Buchungen]([ID_KP_Bridge] ASC);


GO
CREATE NONCLUSTERED INDEX [Ultimobuchung]
    ON [CORE].[fact_Buchungen]([Ultimobuchung] ASC);

