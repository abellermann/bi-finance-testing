﻿CREATE TABLE [CORE].[fact_Bilanz] (
    [Kontonummer]       VARCHAR (255)    NULL,
    [Amount]            DECIMAL (38, 20) NULL,
    [MandantenID]       INT              NULL,
    [LevelID]           INT              NULL,
    [ID_KP_Bridge]      INT              NULL,
    [LOAD_DATE]         DATETIME         NULL,
    [Monat]             INT              NULL,
    [ID_Nachbuchungen]  INT              NULL,
    [OneOffID]          INT              NULL,
    [KippAmount]        DECIMAL (38, 20) NULL,
    [KonsolidierungsID] INT              NULL,
    [Amount_EUR]        DECIMAL (38, 20) NULL,
    [Currency]          VARCHAR (5)      NULL,
    [ID_Bilanz]         INT              NOT NULL
);

