﻿CREATE FUNCTION [core].[Get_dim_Mandanten_Name]
(
	@MandantenID int 
)
RETURNS varchar(255)
AS
BEGIN
	-- Declare the return variable here
	Declare @MandantenName varchar(255) = '';

	-- Add the T-SQL statements to compute the return value here
	SELECT @MandantenName = MandantNAV from core.dim_Mandant where ID = @MandantenID

	-- Return the result of the function
	RETURN isNull(@MandantenName, 'Unbekannt')

END