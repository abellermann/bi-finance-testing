﻿CREATE FUNCTION [core].[Get_dim_Mandanten_ID]
(
	@MandantenName nvarchar(255) 
)
RETURNS bigint
AS
BEGIN
	-- Declare the return variable here
	Declare @MandantenID int
	SET @MandantenID = 0;

	-- Add the T-SQL statements to compute the return value here
	SELECT @MandantenID = ID from core.dim_Mandant where MandantNAV = @MandantenName

	-- Return the result of the function
	RETURN isNull(@MandantenID, 0)
END