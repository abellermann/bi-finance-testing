﻿create FUNCTION [core].[Get_dim_LevelID]
(
	@Level varchar(50)
)
RETURNS tinyint
AS
BEGIN
	Declare @LevelID int
	SELECT @LevelID = LevelID from core.dim_Level where Level = @Level
	RETURN isNull(@LevelID, 0)
END