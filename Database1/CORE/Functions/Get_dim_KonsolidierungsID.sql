﻿create FUNCTION [core].[Get_dim_KonsolidierungsID]
(
	@Konsolidierungstyp varchar(50)
)
RETURNS tinyint
AS
BEGIN
	Declare @KonsoID int
	SELECT @KonsoID = KonsolidierungsID from core.dim_Konsolidierungstyp where Konsolidierungstyp = @Konsolidierungstyp
	RETURN @KonsoID
END