﻿
CREATE function [core].[Get_dim_ProfitCenterID]
(
	@ProfitCenter_kurz varchar(20) 
)
returns int
as
begin
	-- declare return variable
	declare @ID int
	set @ID = -1;

	-- statements to compute the return value
	select @ID = ID from core.dim_ProfitCenter where ProfitCenter_kurz = @ProfitCenter_kurz

	-- return result
	return isnull(@ID, 0)
end
