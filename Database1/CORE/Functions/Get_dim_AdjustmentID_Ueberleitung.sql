﻿create FUNCTION [core].[Get_dim_AdjustmentID_Ueberleitung]
(
	@Jahr_von int
	, @Jahr_fuer int
)
RETURNS int
AS
BEGIN
	Declare @AdjustmentID int
	
	SELECT @AdjustmentID = AdjustmentID from core.dim_Adjustments
	where AdjustmentDescription = 'Überleitung ' + CAST(@Jahr_fuer as CHAR(4)) + ' zu ' + CAST(@Jahr_von as CHAR(4))
	
	RETURN isNull(@AdjustmentID, 1)
END