﻿Create PROCEDURE [core].[sp_fact_Initialbilanz_Level_Anpassen]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- ---------------------
		-- Variablen deklarieren
		-- ---------------------
		--declare @RUNID int = null
		Declare @StartDateTime datetime = getdate();
		Declare @StartDateTimeStep datetime = getdate();
		declare @error varchar(max)

			update b
			 set b.LevelID = core.Get_dim_LevelID('Schweiz')
			from core.fact_Bilanz b
			where LevelID = 15
			and Monat <> 201603
			and MandantenID = 14


			update b
			 set b.LevelID = core.Get_dim_LevelID('Schweiz')
			from core.fact_Bilanz_Zusatzbuchung b
			where LevelID = 15
			and Monat <> 201603
			and MandantenID = 14


			update b
			 set b.LevelID = core.Get_dim_LevelID('Kapitalkonsolidierung Schweiz')
			from core.fact_Bilanz_Zusatzbuchung b
			where LevelID = 15
			and Monat <> 201603
			and MandantenID = 15
			
			--requirement by accounting - the starting balance for switzerland has to be in April		
			update b			 
				set b.Monat = 201604
			from core.fact_Bilanz b
			where LevelID = 15
			and Monat = 201603
			
			--probably not necessary
			update b			 
				set b.Monat = 201604
			from core.fact_Bilanz_Zusatzbuchung b
			where LevelID = 15
			and Monat = 201603


	END TRY
	
	BEGIN CATCH
	END CATCH
END