﻿
CREATE PROCEDURE [CORE].[sp_fact_Bilanz]
	--@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- ---------------------
		-- Variablen deklarieren
		-- ---------------------
		--declare @RUNID int = null
		Declare @StartDateTime datetime = getdate();
		Declare @StartDateTimeStep datetime = getdate();
		declare @error varchar(max)
		declare @FileSource varchar(100) = 'core.sp_fact_Bilanz'
		
		Declare @DatumAbBilanz varchar(12)
		select  @DatumAbBilanz = Wert from META.sysvar where Beschreibung = 'Bilanz'
		Declare @Stichtag datetime = CONVERT(datetime,@DatumAbBilanz,23)
		Declare @Stichmonat int = Convert(varchar(12),@Stichtag,112)/100
		
		declare @ProcessInfo varchar(100)
		
		--Hilfstabelle Zusatzbuchung + Buchungen erzeugen
		IF OBJECT_ID('tempdb..#BilanzBuchungen') IS NOT NULL
			drop table #BilanzBuchungen	
		
		Select * 
		into #BilanzBuchungen
		from (
		SELECT a.[KontoNummer]
			  , PostingDateID/100 as PostingMonat
			  , a.Amount
			  , a.[MandantenID]
			  , a.ID_KP_Bridge
			  , a.LevelID
			  , a.Nullstellung
			  , isnull(a.ID_Nachbuchungen,0) as ID_Nachbuchungen
			  , a.OneOffID
			  , core.Get_dim_KonsolidierungsID('Keine Konsolidierung') as KonsolidierungsID
			  , a.Currency
		FROM core.fact_Buchungen a
		inner join core.dim_Kontenplan_Bridge kp on a.ID_KP_Bridge = kp.ID_KP_Bridge
		where kp.GuV_Bilanz = 'Bilanz'
		  and kp.Monat_gueltig = 999999
		union all 
		Select  b.KontoNummer
			, b.PostingMonat
			, b.Amount
			, b.MandantenID
			, b.ID_KP_Bridge
			, b.LevelID
			, 0 as Nullstellung
			, isnull(b.ID_Nachbuchungen,0) as ID_Nachbuchungen
			, b.OneOffID
			, isnull(b.KonsolidierungsID,0) as KonsolidierungsID
			, b.Currency
		from core.fact_Zusatzbuchungen b 
		inner join core.dim_Kontenplan_Bridge kp on b.ID_KP_Bridge = kp.ID_KP_Bridge
		where (kp.GuV_Bilanz = 'Bilanz' or kp.GuV_Bilanz = 'Wert Allowance (Bilanz)')
		  and kp.Monat_gueltig = 999999
		) v
		
		
		IF OBJECT_ID('tempdb..#BilanzZwischenWerte') IS NOT NULL
		drop table #BilanzZwischenWerte	
		
		create table #BilanzZwischenWerte (
			Kontonummer varchar(255)
			, Amount decimal(38,20)
			, MandantenID int
			, LevelID int
			, ID_KP_Bridge int
			, ID_Nachbuchungen int
			, OneOffID int
			, KonsolidierungsID int
			, Currency varchar(5)
		)
		
		IF OBJECT_ID('tempdb..#fact_Bilanz_temp') IS NOT NULL
		drop table #fact_Bilanz_temp	
		
		CREATE TABLE #fact_Bilanz_temp(
			[Kontonummer] [varchar](255) NULL,
			[Amount] [decimal](38, 20) NULL,
			[MandantenID] [int] NULL,
			[LevelID] [int] NULL,
			ID_KP_Bridge [int] NULL,
			[Monat] [int] NULL,
			[ID_Nachbuchungen] [int] NULL,
			OneOffID int,
			KonsolidierungsID int,
			Currency varchar(5)
		)
		
		-- ------------------------------------------------------------------------------------------------
		-- Bilanzwert für den Monat vom 'BilanzAbTag' berechnen und in Faktentabelle schreiben
		-- Dies ist der Startpunkt für die sukzessive Berechnung der monatlichen Bilanzwerte
		-- --------------------------------------------------------------------------------
		insert into #BilanzZwischenWerte
		select a.KontoNummer                       
			, isnull(sum([Amount]),0) as Amount
			, a.[MandantenID]                     
			, a.[LevelID]                         
			, a.ID_KP_Bridge
			, a.ID_Nachbuchungen
			, a.OneOffID
			, a.KonsolidierungsID
			, a.Currency
		from #BilanzBuchungen a
		where a.PostingMonat <= @Stichmonat
		  and a.KontoNummer not in ('d86000', '086000','d86000H', '086000H','d86000I', '086000I' )
		group by a.KontoNummer,a.ID_KP_Bridge, a.[MandantenID], a.[LevelID], a.ID_Nachbuchungen, a.OneOffID, a.Currency, a.KonsolidierungsID

		--Startwert in Faktentabelle_temp abspeichern
		Insert into #fact_Bilanz_temp
			( Kontonummer
			, Amount
			, MandantenID
			, ID_KP_Bridge
			, LevelID
			, Monat
			, ID_Nachbuchungen
			, OneOffID
			, KonsolidierungsID
			, Currency)
		Select			
			Kontonummer
			, Amount
			, MandantenID
			, ID_KP_Bridge
			, LevelID
			, @StichMonat as Monat
			, ID_Nachbuchungen
			, OneOffID
			, KonsolidierungsID
			, Currency
		from #BilanzZwischenWerte
		
--		exec dbo.p_protokoll @SourceID=@RUNID, @Package=@@PROCID, @RecsOK=0, @StartDateTime=@StartDateTimeStep, @ProcessInfo = 'Bilanz Initialer Monat erfolgreich'
		set @StartDateTimeStep = GETDATE()
			
		-- ----------------------------------------------------------------------------------------------
		-- Bilanzwert sukzessive für die folgenden Monate bis Jahresende (wegen Plandaten) berechnen,
		-- auf den vorherigen aufaddieren und mit entsprechenden Stichtag in die Faktentabelle schreiben.
		-- ----------------------------------------------------------------------------------------------
		WHILE @Stichmonat < (year(GETDATE())+1)*100+1
		
		BEGIN		
				Set @Stichtag = DATEADD(mm,1,@Stichtag)
				Set @Stichmonat = Convert(varchar,@Stichtag,112)/100
				        
					merge into #BilanzZwischenWerte as t
					using 
						(
							select a.KontoNummer                       as KontoNummer
									, isnull(sum([Amount]),0)			as Amount
									, a.[MandantenID]                     as MandantenID
									, a.[LevelID]                         as LevelID
									, a.ID_KP_Bridge
									, a.ID_Nachbuchungen
									, a.OneoffID
									, a.KonsolidierungsID
									, a.Currency collate database_default as Currency
						 from #Bilanzbuchungen a
								where  a.PostingMonat = @StichMonat
									and a.KontoNummer not in ('d86000', '086000','d86000H', '086000H','d86000I', '086000I')
								group by a.KontoNummer,a.ID_KP_Bridge, a.[MandantenID], a.[LevelID], a.ID_Nachbuchungen, a.OneOffID, a.Currency, a.KonsolidierungsID
						   ) as s
						on (s.ID_KP_Bridge = t.ID_KP_Bridge 
							and s.MandantenID = t.MandantenID 
							and s.LevelID = t.LevelID 
							and s.ID_Nachbuchungen = t.ID_Nachbuchungen
							and s.OneOffID = t.OneOffID
							and s.KonsolidierungsID = t.KonsolidierungsID
							and s.Currency = t.Currency)
					when Matched then
						update set Amount = s.Amount + t.Amount
					when not matched then
						insert (Kontonummer
							, Amount
							, MandantenID
							, ID_KP_Bridge
							, LevelID
							, ID_Nachbuchungen
							, OneOffID
							, KonsolidierungsID
							, Currency
							)
						values (
							 s.Kontonummer
							, s.Amount
							, s.MandantenID
							, s.ID_KP_Bridge
							, s.LevelID
							, s.ID_Nachbuchungen
							, s.OneOffID
							, s.KonsolidierungsID
							, s.Currency
						);

				Insert into #fact_Bilanz_temp(
					Kontonummer
					, Amount
					, MandantenID
					, ID_KP_Bridge
					, LevelID
					, Monat
					, ID_Nachbuchungen
					, OneOffID
					, KonsolidierungsID
					, Currency
				)
				Select			
					Kontonummer
					, Amount
					, MandantenID
					, ID_KP_Bridge
					, LevelID
					, @StichMonat as Monat
					, ID_Nachbuchungen
					, OneOffID
					, KonsolidierungsID
					, Currency
				from #BilanzZwischenWerte
				
			set @ProcessInfo = 'Bilanz: ' + convert(varchar,@Stichmonat)
--			exec dbo.p_protokoll @SourceID=@RUNID, @Package=@@PROCID, @RecsOK=0, @StartDateTime=@StartDateTimeStep, @ProcessInfo = @ProcessInfo
			set @StartDateTimeStep = GETDATE()
			
		END

		-- ------------------------------------------------------------------------------
		-- Für Ausnahmen werden die Bilanzwerte für jeden Stichtag komplett neu berechnet
		-- ------------------------------------------------------------------------------
		-- Folgende Ausnahmen werden berücksichtigt:
			-- Das Konto 086000 wird für alle Mandanten genauso behandelt wie alle anderen Bilanzkonten auch,
				-- nur darf Nullstellung für aktuellen Stichtag nicht berücksichtigt werden.
			-- Das Konto d86000 wird anders berechnet als die anderen Bilanzkonten.
					-- Für dieses Konto darf nur der NetIncome für das aktuelle Jahr berechnet werden.

		set @Stichtag = CONVERT(datetime,@DatumAbBilanz,23)
		Set @Stichmonat = Convert(varchar,@Stichtag,112)/100

		WHILE @Stichmonat < (year(GETDATE())+1)*100+1
		
		BEGIN		
			Set @Stichtag = DATEADD(mm,1,@Stichtag)
			Set @Stichmonat = Convert(varchar,@Stichtag,112)/100
			
			--declare @stichtag int = 20160101
			--declare @stichmonat int = 20160101
			insert into #fact_Bilanz_temp
							(KontoNummer
							, Monat
							, Amount
							, MandantenID
							, ID_KP_Bridge
							, LevelID
							, ID_Nachbuchungen
							, OneOffID
							, KonsolidierungsID
							, Currency)
			   select a.KontoNummer                       as KontoNummer
					, @StichMonat                         as Monat
					, isnull(sum(Amount),0)				  as Amount
					, a.[MandantenID]                     as MandantenID
					, a.ID_KP_Bridge                     as ID_KP_Bridge
					, a.[LevelID]                         as LevelID
					, a.ID_Nachbuchungen				  as ID_Nachbuchungen
					, a.OneOffID						  as OneOffID
					, a.KonsolidierungsID
					, a.Currency						  as Currency
			   from #BilanzBuchungen a
			   where a.PostingMonat <= @StichMonat
				 and a.KontoNummer in ('086000','086000H','086000I')
				 and (cast(a.PostingMonat as varchar) + '-' + isnull(cast(a.Nullstellung as varchar), '0') <> cast(@StichMonat as varchar) + '-1')
			   group by a.KontoNummer, a.[MandantenID], a.[LevelID], a.ID_KP_Bridge , a.ID_Nachbuchungen, a.OneOffID, a.Currency, a.KonsolidierungsID
			  
			   union all
			  
			    select a.KontoNummer as KontoNummer
					, @Stichmonat                         as Monat
					, isnull(sum([Amount]),0)			  as Amount
					, a.[MandantenID]                     as MandantenID
					, a.ID_KP_Bridge                     as ID_KP_Bridge
					, a.[LevelID]                         as LevelID
					, a.ID_Nachbuchungen				  as ID_Nachbuchungen
					, a.OneOffID						  as OneOffID
					, a.KonsolidierungsID
					, a.Currency						  as Currency
			   from #BilanzBuchungen a
			   where a.PostingMonat        <= @Stichmonat
				 and a.PostingMonat/100     = @Stichmonat/100
				 and a.KontoNummer  in ('d86000','d86000I','d86000H')
			   group by a.KontoNummer, a.[MandantenID], a.[LevelID], a.ID_KP_Bridge, a.ID_Nachbuchungen, a.OneOffID, a.KonsolidierungsID, a.Currency

		END
		
--		exec dbo.p_protokoll @SourceID=@RUNID, @Package=@@PROCID, @RecsOK=0, @StartDateTime=@StartDateTimeStep, @ProcessInfo = 'Bilanz Ausnahmen erfolgreich'
		set @StartDateTimeStep = GETDATE()
		
		truncate table core_fi.fact_Bilanz
				
		-- Daten in die Faktentabelle schreiben, einmal ohne und einmal mit Nachbuchungen
		insert into core.fact_Bilanz
			(KontoNummer
			, Monat
			, Amount
			, Amount_EUR
			, MandantenID
			, ID_KP_Bridge
			, LevelID
			, ID_Nachbuchungen
			, LOAD_DATE
			, OneOffID
			, KonsolidierungsID
			, Currency)
		select bi.KontoNummer
			, bi.Monat
			, sum(bi.Amount) as Amount
			, SUM(bi.Amount/isnull(er.ExchangeRate,1.000)) as Amount_EUR
			, bi.MandantenID
			, bi.ID_KP_Bridge
			, bi.LevelID
			, 2 as ID_Nachbuchungen -- ohne Nachbuchungen
			, getdate() as LOAD_DATE
			, bi.OneOffID 
			, bi.KonsolidierungsID
			, bi.Currency 
		from #fact_Bilanz_temp bi
		left outer join stg.ExchangeRate er
			on er.Currency = bi.Currency collate database_default
			 and er.GuV_Bilanz = 'Bilanz'
			 and er.Month = bi.Monat
		where bi.ID_Nachbuchungen = 0
		group by bi.KontoNummer 
			, bi.Monat
			, bi.MandantenID
			, bi.ID_KP_Bridge
			, bi.LevelID
			, bi.OneOffID 
			, bi.KonsolidierungsID
			, bi.Currency 
		
		insert into core.fact_Bilanz
			(KontoNummer
			, Monat
			, Amount
			, Amount_EUR
			, MandantenID
			, ID_KP_Bridge
			, LevelID
			, ID_Nachbuchungen
			, LOAD_DATE
			, OneOffID
			, KonsolidierungsID
			, Currency)
		select bi.KontoNummer
			, bi.Monat
			, sum(bi.Amount) as Amount
			, SUM(bi.Amount/isnull(er.ExchangeRate,1.000)) as Amount_EUR
			, bi.MandantenID
			, bi.ID_KP_Bridge
			, bi.LevelID
			, 3 as ID_Nachbuchungen -- mit Nachbuchungen
			, getdate() as LOAD_DATE
			, bi.OneOffID
			, bi.KonsolidierungsID
			, bi.Currency
		from #fact_Bilanz_temp bi
		left outer join stg.ExchangeRate er
			on er.Currency = bi.Currency collate database_default
			 and er.GuV_Bilanz = 'Bilanz'
			 and er.Month = bi.Monat
		group by KontoNummer
			, Monat
			, MandantenID
			, ID_KP_Bridge
			, LevelID
			, OneOffID
			, bi.KonsolidierungsID
			, bi.Currency
		
--		exec dbo.p_protokoll @SourceID=@RUNID, @Package=@@PROCID, @RecsOK=0, @StartDateTime=@StartDateTimeStep, @ProcessInfo = 'Bilanz: Eintragen in Faktentabelle erfolgreich'
		
	END TRY
	
	BEGIN CATCH
	END CATCH
	
	-- LOG ERFOLG
	END
