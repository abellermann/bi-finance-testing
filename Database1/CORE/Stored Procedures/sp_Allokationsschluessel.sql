﻿CREATE PROCEDURE [core].[sp_Allokationsschluessel]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

	Declare @StartDateTime datetime = getdate()
	declare @error varchar(max)


	select  stat.ID_Monat
		, VK_Wert_online/(VK_Wert_online+VK_Wert_offline) as online_Umsatz_mit
		, 1-VK_Wert_online/(VK_Wert_online+VK_Wert_offline) as offline_Umsatz_mit
		, VK_Wert_online/(VK_Wert_online+VK_Wert_offline_ohne_SV) as online_Umsatz_ohne
		, 1-VK_Wert_online/(VK_Wert_online+VK_Wert_offline_ohne_SV) as offline_Umsatz_ohne
		, Menge_online/(Menge_online + Menge_offline)  as online_Menge_mit
		, 1-Menge_online/(Menge_online + Menge_offline) as offline_Menge_mit
	into #allokationsSchluessel_temp
	--select * 
	from (
		select ID_Monat
			, SUM(case when ID_Retoure = 0 then Menge else 0 end) as Menge_offline --bei Menge Retoure nicht abziehen
			, SUM(VK_Wert) as VK_Wert_offline
		from INPUT.WaWi_Summen ww
		where Lagergroup in ('Fashion Store','Fashion Store','Multi-Store','Second Season', 'Sonstiges','Sonderverkauf')
		group by ID_Monat
		) stat
	inner join (
		select ID_Monat
			, SUM(case when ID_Retoure = 0 then Menge else 0 end) as Menge_offline_ohne_SV --bei Menge Retoure nicht abziehen
			, SUM(VK_Wert) as VK_Wert_offline_ohne_SV
		from INPUT.WaWi_Summen ww
		where Lagergroup in ('Fashion Store','Fashion Store','Multi-Store', 'Sonstiges','Second Season')
		group by ID_Monat
		) stat_ohne
	 on stat.ID_Monat = stat_ohne.ID_Monat
	inner join (
		select ID_Monat
			, Sum(case when ID_Retoure = 0 then Menge else 0 end) as Menge_online --bei Menge Retoure nicht abziehen
			, Sum(VK_Wert) as VK_Wert_online
		from INPUT.WaWi_Summen ww
		where Lagergroup = 'Best Secret'
		group by ID_Monat
		) onl
	 on onl.ID_Monat = stat.ID_Monat
	
	
	
	truncate table core.allokationsSchluessel_ProfitCenter
		
	insert into core.allokationsSchluessel_ProfitCenter(
		PostingMonat
		, Faktor
		, von_ProfitCenterID
		, zu_ProfitCenterID
		)
	select ID_Monat
		, Faktor
		, pcvon.ID as von
		, pczu.ID as zu
	from (

	select ID_Monat 
		,online_Menge_mit as Faktor
		, 'logistik' as von
		, 'online' as zu 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,offline_Menge_mit as Faktor
		, 'logistik' as von
		, 'offline' as online_offline 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,online_Umsatz_ohne as Faktor
		, 'einkauf' as von
		, 'online' as zu 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,offline_Umsatz_ohne as Faktor
		, 'einkauf' as von
		, 'offline' as online_offline 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,online_Umsatz_mit as Faktor
		, 'admin' as von
		, 'online' as zu 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,offline_Umsatz_mit as Faktor
		, 'admin' as von
		, 'offline' as online_offline 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,online_Umsatz_mit as Faktor
		, 'it' as von
		, 'online' as zu 
	from #allokationsSchluessel_temp

	union all

	select ID_Monat 
		,offline_Umsatz_mit as Faktor
		, 'it' as von
		, 'offline' as online_offline 
	from #allokationsSchluessel_temp
	) a
	inner join core.dim_ProfitCenter pcvon on a.von = pcvon.ProfitCenter_kurz
	inner join core.dim_ProfitCenter pczu on a.zu = pczu.ProfitCenter_kurz
	
	END TRY
	
	BEGIN CATCH
	END CATCH
	
	-- LOG ERFOLG

END