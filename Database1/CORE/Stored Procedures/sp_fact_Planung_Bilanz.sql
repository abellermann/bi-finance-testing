﻿CREATE PROCEDURE [core].[sp_fact_Planung_Bilanz]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		-- Variablen deklarieren
		Declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		delete
		from core.fact_Bilanz_Zusatzbuchung
		where LevelID = core.Get_dim_LevelID('Planung')

		Insert into core.fact_Bilanz_Zusatzbuchung
			(Kontonummer
			, Amount
			, Amount_EUR
			, MandantenID
			, LevelID
			, ID_KP_Bridge
			, Monat
			, ID_Nachbuchungen
			, LOAD_DATE
			, Currency)
		select p.Kontonummer
			, case when d.KontoLabel_Level1 = '2 Passiva' then -p.Planwert else p.Planwert end as Amount
			, case when d.KontoLabel_Level1 = '2 Passiva' then -p.Planwert else p.Planwert end as Amount_EUR
			, core.Get_dim_Mandanten_ID('Konzern') as MandantenID
			, core.Get_dim_LevelID('Planung') as ID_Level
			, kp.ID_KP_Bridge
			, p.Monat
			, 0 as ID_Nachbuchungen
			, GETDATE()
			, 'EUR' as Currency
		from stg.Plandaten_Bilanz p
		inner join core.dim_Kontenplan_Bridge kp
			 on p.Kontonummer = kp.KontoNummer
			and kp.GuV_Bilanz in ('Bilanz', 'Wert Allowance (Bilanz)')
			and kp.Monat_gueltig = 999999
		inner join core.dim_Kontenplan d
			 on kp.ID_Kontenplan = d.ID_Kontenplan
		
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END