﻿Create PROCEDURE [core].[sp_fact_Bilanz_Unify]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- ---------------------
		-- Variablen deklarieren
		-- ---------------------
		--declare @RUNID int = null
		Declare @StartDateTime datetime = getdate();
		Declare @StartDateTimeStep datetime = getdate();
		declare @error varchar(max)
		declare @FileSource varchar(100) = 'core.sp_fact_Bilanz_Unify'

	Insert into core.fact_Bilanz (
			ID_KP_Bridge
		  , KontoNummer
		  , Monat
		  , Amount
		  , Currency
		  , Amount_EUR
		  , LOAD_DATE
		  , MandantenID
		  , LevelID
		  , ID_Nachbuchungen
		  , OneOffID
		  , KonsolidierungsID
	)
	SELECT ID_KP_Bridge
		  , KontoNummer
		  , Monat
		  , Amount
		  , Currency
		  , Amount_EUR
		  , LOAD_DATE
		  , MandantenID
		  , LevelID
		  , ID_Nachbuchungen
		  , isnull(a.OneOffID, 0) as OneOffID
		  , isnull(KonsolidierungsID, 0) as KonsolidierungsID
	FROM core.fact_Bilanz_Zusatzbuchung a


	END TRY
	
	BEGIN CATCH
	END CATCH
	
END