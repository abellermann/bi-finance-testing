﻿CREATE PROCEDURE [core].[sp_fact_BS_Verrechnungen]

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		-- Variablen Deklarieren
		Declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
        
        -- IC Konsobuchungen löschen (um Einträge zu vermeiden, falls die zugehörigen Buchungen gelöscht werden)
        delete
        from core.fact_Zusatzbuchungen
        where LevelID = core.Get_dim_LevelID('BestSecret-Verrechnung')

        -- IC Konsobuchungen mit *-1 in die Writeback Tabelle auf den Konsolidierungsmandanten schreiben
        insert into core.fact_Zusatzbuchungen(
			Amount
			, Amount_EUR
			, EntryNo
			, MandantEntryNo
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, OneOffID
			, CapexItID
			, ID_Nachbuchungen
			, EingabeDatumId
			, KonsolidierungsID
			, Currency)
        select Amount *-1 as [Amount] 
			 , Amount_EUR *-1 as Amount_EUR 
             , EntryNo
             , MandantEntryNo
             , core.Get_dim_Mandanten_ID('BestSecret Verrechnung') as [MandantenID]
             , KostenstellenID
             , PostingDateID/100 as [PostingMonat]      
             , ID_KP_Bridge
             , core.Get_dim_LevelID('BestSecret-Verrechnung') as [LevelID]
             , GETDATE() as [MS_AUDIT_TIME]
             , SYSTEM_USER as [MS_AUDIT_USER]
             , KontoNummer
             , OneOffID
             , CapexItID
             , ID_Nachbuchungen
			 , EingabeDatumId
			 , core.Get_dim_KonsolidierungsID('IC-Konsolidierung') as KonsolidierungsID
			 , Currency
        from core.fact_Buchungen a
        inner join core.dim_Mandant m
			 on a.MandantenID = m.ID
        where m.Gruppenname = 'BestSecret'
		  and KontoNummer in (select KontoNummer from meta.BS_Verrechnungskonten)
          and a.Ultimobuchung = 'N'

	END TRY
	
	BEGIN CATCH
	END CATCH

END