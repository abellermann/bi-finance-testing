﻿
CREATE proc [CORE].[sp_fact_Allowances]
--	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- Variablen deklarieren
		declare @StartDateTime		datetime = GETDATE()
		declare @error varchar(max)
        Declare @KostenstelleID int
		
		-- Null-Zeilen löschen
		delete from meta.Allowance where Monat is null
		
		-- GuV-Allowances in d999001
		-- aktuell
		insert into core.Zusatzbuchungen_intermediate (
		Amount
		, MandantenID
		, KostenstellenID
		, PostingMonat
		, MS_AUDIT_TIME
		, MS_AUDIT_USER
		, KontoNummer
		, LevelID
		, ProfitCenterID
		)
		select a.d999001
			, (Select ID from Core.dim_Mandant where MandantNAV ='Konzern') MandantID
			, d.ID as KostenstellenID
			, a.Monat
			, getdate()
			, SYSTEM_USER
			, case when Rechnungslegungsnorm = 'HGB' then 'd999001H'
					when  Rechnungslegungsnorm = 'IFRS' then 'd999001I'
					else 'd999001'
			  end as Kontonummer
			, core.Get_dim_LevelID('Allowances')
			, d.ProfitCenterID
		from meta.Allowance a
		inner join core.[dim_Dimension Value] d
			 on d.MandantenName = 'Konzern' and d.Code = '30303001'

	END TRY
	
	BEGIN CATCH
	END CATCH

END
