﻿CREATE PROCEDURE [core].[sp_dim_Kontenplan]
	@RUNID int = null
AS
BEGIN
	BEGIN TRY
	
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		--Kontenplan Bezeichner Tabelle leeren und neu füllen
		/*if exists (
			Select distinct kp.GuV_Bilanz			
				,kp.GKV_UKV
				,kp.Controlling_Accounting
				,kp.HGB_IFRS
				from stg_fi.Kontenplaene kp
			left outer join mdm.Kontenplan_Bezeichner b
				on kp.GKV_UKV = b.GKV_UKV
					and isnull(kp.GuV_Bilanz,'') = isnull(b.GuV_Bilanz,'')
					and isnull(kp.Controlling_Accounting,'') = isnull(b.Controlling_Accounting,'')
					and isnull(kp.HGB_IFRS,'') = isnull(b.HGB_IFRS,'')
			where b.GuV_Bilanz is null
			)
		begin
			select DB_NAME() as Datenbank into ##DB_Name
			
			declare @recipientList varchar(max)
			set @recipientList = dbo.get_Emailverteiler('Fibu_Tech')
		
			EXEC msdb.dbo.sp_send_dbmail
				@recipients = @recipientList,
				@subject = 'Für neuen Kontenplan: Eintrag in mdm.Kontenplan_Bezeichner fehlt',
				@body = 'Für neuen Kontenplan: Eintrag in mdm.Kontenplan_Bezeichner fehlt.
(Gesendet von [core_fi].[sp_dim_Kontenplan])',
				@query = 'select Datenbank from ##DB_Name';
									
			drop table ##DB_Name
		end */
		
		select KontoNummer        = coalesce(a.KontoNummer, b.Kontonummer, c.KontoNummer, d.KontoNummer, e.KontoNummer, f.KontoNummer, g.KontoNummer)
			 , Name_DEU           = coalesce(isnull(g.KontoNummer,'')+' '+g.Name_DEU, isnull(f.KontoNummer,'')+' '+f.Name_DEU, isnull(e.KontoNummer,'')+' '+e.Name_DEU, isnull(d.KontoNummer,'')+' '+d.Name_DEU, isnull(c.KontoNummer,'')+' '+c.Name_DEU, isnull(b.KontoNummer,'')+' '+b.Name_DEU, isnull(a.KontoNummer,'')+' '+a.Name_DEU)
             , KontoID_Level0     = isnull(a.KontoNummer + '-K',a.EbenenCode)
			 , KontoLabel_Level0  = a.Name_DEU
             , KontoID_Level1     = coalesce(isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level1  = coalesce(isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , KontoID_Level2     = coalesce(isnull(c.KontoNummer + '-K',c.EbenenCode), isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level2  = coalesce(isnull(c.KontoNummer,c.EbenenCode)+' '+c.Name_DEU, isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , KontoID_Level3     = coalesce(isnull(d.KontoNummer + '-K',d.EbenenCode), isnull(c.KontoNummer + '-K',c.EbenenCode), isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level3  = coalesce(isnull(d.KontoNummer,d.EbenenCode)+' '+d.Name_DEU, isnull(c.KontoNummer,c.EbenenCode)+' '+c.Name_DEU, isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , KontoID_Level4     = coalesce(isnull(e.KontoNummer + '-K',e.EbenenCode), isnull(d.KontoNummer + '-K',d.EbenenCode), isnull(c.KontoNummer + '-K',c.EbenenCode), isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level4  = coalesce(isnull(e.KontoNummer,e.EbenenCode)+' '+e.Name_DEU, isnull(d.KontoNummer,d.EbenenCode)+' '+d.Name_DEU, isnull(c.KontoNummer,c.EbenenCode)+' '+c.Name_DEU, isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , KontoID_Level5     = coalesce(isnull(f.KontoNummer + '-K',f.EbenenCode), isnull(e.KontoNummer + '-K',e.EbenenCode), isnull(d.KontoNummer + '-K',d.EbenenCode), isnull(c.KontoNummer + '-K',c.EbenenCode), isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level5  = coalesce(isnull(f.KontoNummer,f.EbenenCode)+' '+f.Name_DEU, isnull(e.KontoNummer,e.EbenenCode)+' '+e.Name_DEU, isnull(d.KontoNummer,d.EbenenCode)+' '+d.Name_DEU, isnull(c.KontoNummer,c.EbenenCode)+' '+c.Name_DEU, isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , KontoID_Level6     = coalesce(isnull(g.KontoNummer + '-K',g.EbenenCode), isnull(f.KontoNummer + '-K',f.EbenenCode), isnull(e.KontoNummer + '-K',e.EbenenCode), isnull(d.KontoNummer + '-K',d.EbenenCode), isnull(c.KontoNummer + '-K',c.EbenenCode), isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level6  = coalesce(isnull(g.KontoNummer,g.EbenenCode)+' '+g.Name_DEU, isnull(f.KontoNummer,f.EbenenCode)+' '+f.Name_DEU, isnull(e.KontoNummer,e.EbenenCode)+' '+e.Name_DEU, isnull(d.KontoNummer,d.EbenenCode)+' '+d.Name_DEU, isnull(c.KontoNummer,c.EbenenCode)+' '+c.Name_DEU, isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , KontoID_Level7     = coalesce(isnull(g.KontoNummer + '-K',g.EbenenCode), isnull(f.KontoNummer + '-K',f.EbenenCode), isnull(e.KontoNummer + '-K',e.EbenenCode), isnull(d.KontoNummer + '-K',d.EbenenCode), isnull(c.KontoNummer + '-K',c.EbenenCode), isnull(b.KontoNummer + '-K',b.EbenenCode), isnull(a.KontoNummer + '-K',a.EbenenCode))
			 , KontoLabel_Level7  = coalesce(isnull(g.KontoNummer,g.EbenenCode)+' '+g.Name_DEU, isnull(f.KontoNummer,f.EbenenCode)+' '+f.Name_DEU, isnull(e.KontoNummer,e.EbenenCode)+' '+e.Name_DEU, isnull(d.KontoNummer,d.EbenenCode)+' '+d.Name_DEU, isnull(c.KontoNummer,c.EbenenCode)+' '+c.Name_DEU, isnull(b.KontoNummer,b.EbenenCode)+' '+b.Name_DEU, isnull(a.KontoNummer,a.EbenenCode)+' '+a.Name_DEU)
			 , GuV_Bilanz			= a.GuV_Bilanz
			 , GKV_UKV = a.GKV_UKV
			 , Controlling_Accounting = a.Controlling_Accounting
			 , HGB_IFRS = a.HGB_IFRS
			 , ProfitCenter = coalesce(g.ProfitCenter,f.ProfitCenter,e.ProfitCenter,d.ProfitCenter,c.ProfitCenter,b.ProfitCenter,a.ProfitCenter)
			 , HGB_IFRS_Beide = coalesce(g.HGB_IFRS_Beide,f.HGB_IFRS_Beide,e.HGB_IFRS_Beide,d.HGB_IFRS_Beide,c.HGB_IFRS_Beide,b.HGB_IFRS_Beide,a.HGB_IFRS_Beide)
			 , '' as KontoID_Level
			, null as IC
			, null as Level2_Sortierung
		into #Kontenplan_Aktuell
		from stg.Kontenplaene a
		left outer join stg.Kontenplaene b on b.ParentEbenenCode = a.EbenenCode and isnull(b.GuV_Bilanz,'') = isnull(a.GuV_Bilanz,'') and isnull(b.GKV_UKV,'') = isnull(a.GKV_UKV,'') and isnull(b.Controlling_Accounting,'') = isnull(a.Controlling_Accounting,'') and isnull(b.HGB_IFRS,'') = isnull(a.HGB_IFRS,'')
		left outer join stg.Kontenplaene c on c.ParentEbenenCode = b.EbenenCode and isnull(c.GuV_Bilanz,'') = isnull(b.GuV_Bilanz,'') and isnull(c.GKV_UKV,'') = isnull(b.GKV_UKV,'') and isnull(c.Controlling_Accounting,'') = isnull(b.Controlling_Accounting,'') and isnull(c.HGB_IFRS,'') = isnull(b.HGB_IFRS,'')
		left outer join stg.Kontenplaene d on d.ParentEbenenCode = c.EbenenCode and isnull(d.GuV_Bilanz,'') = isnull(c.GuV_Bilanz,'') and isnull(d.GKV_UKV,'') = isnull(c.GKV_UKV,'') and isnull(d.Controlling_Accounting,'') = isnull(c.Controlling_Accounting,'') and isnull(d.HGB_IFRS,'') = isnull(c.HGB_IFRS,'')
		left outer join stg.Kontenplaene e on e.ParentEbenenCode = d.EbenenCode and isnull(e.GuV_Bilanz,'') = isnull(d.GuV_Bilanz,'') and isnull(e.GKV_UKV,'') = isnull(d.GKV_UKV,'') and isnull(e.Controlling_Accounting,'') = isnull(d.Controlling_Accounting,'') and isnull(e.HGB_IFRS,'') = isnull(d.HGB_IFRS,'')
		left outer join stg.Kontenplaene f on f.ParentEbenenCode = e.EbenenCode and isnull(f.GuV_Bilanz,'') = isnull(e.GuV_Bilanz,'') and isnull(f.GKV_UKV,'') = isnull(e.GKV_UKV,'') and isnull(f.Controlling_Accounting,'') = isnull(e.Controlling_Accounting,'') and isnull(f.HGB_IFRS,'') = isnull(e.HGB_IFRS,'')
		left outer join stg.Kontenplaene g on g.ParentEbenenCode = f.EbenenCode and isnull(g.GuV_Bilanz,'') = isnull(f.GuV_Bilanz,'') and isnull(g.GKV_UKV,'') = isnull(f.GKV_UKV,'') and isnull(g.Controlling_Accounting,'') = isnull(f.Controlling_Accounting,'') and isnull(g.HGB_IFRS,'') = isnull(f.HGB_IFRS,'')
		left outer join stg.Kontenplaene h on h.ParentEbenenCode = g.EbenenCode and isnull(h.GuV_Bilanz,'') = isnull(g.GuV_Bilanz,'') and isnull(h.GKV_UKV,'') = isnull(g.GKV_UKV,'') and isnull(h.Controlling_Accounting,'') = isnull(g.Controlling_Accounting,'') and isnull(h.HGB_IFRS,'') = isnull(g.HGB_IFRS,'')
		where a.ParentEbenenCode  is null
		  and coalesce(a.KontoNummer, b.Kontonummer, c.KontoNummer, d.KontoNummer, e.KontoNummer, f.KontoNummer, g.KontoNummer) is not null --filtert Knoten ohne zugehörige Konten aus
		
		--LevelID setzen
		update a
			set a.KontoID_Level = kb.Bezeichner_ID
		from #Kontenplan_Aktuell a
		left outer join META.Kontenplan_Bezeichner kb 
			on kb.GuV_Bilanz=a.GuV_Bilanz  
				and isnull(kb.GKV_UKV,'') = isnull(a.GKV_UKV,'')
				and isnull(kb.HGB_IFRS,'') = isnull(a.HGB_IFRS,'') 
				and isnull(kb.Controlling_Accounting,'') = isnull(a.Controlling_Accounting,'')
		
		
		delete from core.dim_Kontenplan
		where Monat_gueltig = 999999

		insert into core.dim_Kontenplan (KontoNummer
			, Name_DEU 
			, KontoID_Level
			, KontoID_Level0
			, KontoLabel_Level0
			, KontoID_Level1
			, KontoLabel_Level1
			, KontoID_Level2
			, KontoLabel_Level2
			, KontoID_Level3
			, KontoLabel_Level3
			, KontoID_Level4
			, KontoLabel_Level4
			, KontoID_Level5
			, KontoLabel_Level5
			, KontoID_Level6
			, KontoLabel_Level6
			, KontoID_Level7
			, KontoLabel_Level7
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS
			, ProfitCenter
			, HGB_IFRS_Beide
			, Monat_gueltig
			)
		select KontoNummer
			, Name_DEU 
			, KontoID_Level	, KontoID_Level0	
			, KontoLabel_Level0
			, KontoID_Level1
			, KontoLabel_Level1
			, KontoID_Level2
			, KontoLabel_Level2
			, KontoID_Level3
			, KontoLabel_Level3
			, KontoID_Level4
			, KontoLabel_Level4
			, KontoID_Level5
			, KontoLabel_Level5
			, KontoID_Level6
			, KontoLabel_Level6
			, KontoID_Level7
			, KontoLabel_Level7
			, GuV_Bilanz
			, GKV_UKV
			, Controlling_Accounting
			, HGB_IFRS
			, ProfitCenter
			, HGB_IFRS_Beide
			, 999999 as Monat_gueltig
		from #Kontenplan_Aktuell
		;
		
		
		-- IC setzen fuer Intercompany-Konsolidierung und -Verrechnung
		update kp
			set IC = case when ic.KonsoKonto = '' then 'IC' else ic.KonsoKonto end
		from core.dim_Kontenplan kp
		inner join meta.IC_Konsokonten ic
			 on kp.KontoNummer = ic.KontoNummer
			 
		update kp
			set IC = ics.KonsoKonto
		from core.dim_Kontenplan kp
		inner join meta.IC_Konsokonten_Schweiz ics
			 on kp.KontoNummer = ics.KontoNummer
			 
		update kp
			set IC = bs.KonsoKonto
		from core.dim_Kontenplan kp
		inner join meta.BS_Verrechnungskonten bs
			 on kp.KontoNummer = bs.KontoNummer
		

		
		-- Kontenplan nach Benutzereingabe wegspeichern
		declare @Monat varchar(6);
		select @Monat = Wert from meta.Eingaben where Beschreibung = 'Kontenplan wegspeichern fuer Monat (z.B. 201605)'
	
		if @Monat is not null
		begin
		
			if ISNUMERIC(@Monat)=1 and LEN(@Monat) =6
			begin
			
				delete from core.dim_Kontenplan
					where Monat_gueltig = @Monat
				
				insert into core.dim_Kontenplan (KontoNummer
					, Name_DEU 
					, KontoID_Level
					, KontoID_Level0
					, KontoLabel_Level0
					, KontoID_Level1
					, KontoLabel_Level1
					, KontoID_Level2
					, KontoLabel_Level2
					, KontoID_Level3
					, KontoLabel_Level3
					, KontoID_Level4
					, KontoLabel_Level4
					, KontoID_Level5
					, KontoLabel_Level5
					, KontoID_Level6
					, KontoLabel_Level6
					, KontoID_Level7
					, KontoLabel_Level7
					, GuV_Bilanz
					, GKV_UKV
					, Controlling_Accounting
					, HGB_IFRS
					, ProfitCenter
					, HGB_IFRS_Beide
					, Monat_gueltig
				)
				select KontoNummer
					, Name_DEU 
					, KontoID_Level	
					, KontoID_Level0	
					, KontoLabel_Level0
					, KontoID_Level1
					, KontoLabel_Level1
					, KontoID_Level2
					, KontoLabel_Level2
					, KontoID_Level3
					, KontoLabel_Level3
					, KontoID_Level4
					, KontoLabel_Level4
					, KontoID_Level5
					, KontoLabel_Level5
					, KontoID_Level6
					, KontoLabel_Level6
					, KontoID_Level7
					, KontoLabel_Level7
					, GuV_Bilanz
					, GKV_UKV
					, Controlling_Accounting
					, HGB_IFRS
					, ProfitCenter
					, HGB_IFRS_Beide
					, @Monat
				from #Kontenplan_Aktuell
				
				update ein
					set ein.Wert = null
				from meta.Eingaben ein
				where Beschreibung = 'Kontenplan wegspeichern fuer Monat (z.B. 201605)'
				
			end
	/*		else 
			begin
				EXEC msdb.dbo.sp_send_dbmail	@recipients = 'schubo_tng01@Schustermann-Borenstein.de; julia.winkler@Schustermann-Borenstein.de; alexander.schumach@schustermann-borenstein.de',
								@subject = 'Eingabe für Kontenplan wegspeichern für Monat im falschen Format',
								@body = 'Die Eingabe des  Monats, für den der Kontenplan weggespeichert werden soll, ist im falschen Format. Das richtige Format ist YYYYMM, also z.B. 201605'
								;
			end */
		end
		;
			
		
		--Falls Benutzereingabe für Vormonat nicht erfolgt: Kontenplan am 30./28. des Vormonats automatisch wegspeichern
		if datepart(dd,GETDATE()) = 30 or  (datepart(mm,GETDATE()) = 2 and  datepart(dd,GETDATE()) = 28)
		begin
			if not exists (select * from core.dim_Kontenplan where Monat_gueltig = CONVERT(varchar(6),dateadd(MONTH,-1,GETDATE()),112))
			begin
				insert into core.dim_Kontenplan (KontoNummer
				, Name_DEU 
				, KontoID_Level
				, KontoID_Level0
				, KontoLabel_Level0
				, KontoID_Level1
				, KontoLabel_Level1
				, KontoID_Level2
				, KontoLabel_Level2
				, KontoID_Level3
				, KontoLabel_Level3
				, KontoID_Level4
				, KontoLabel_Level4
				, KontoID_Level5
				, KontoLabel_Level5
				, KontoID_Level6
				, KontoLabel_Level6
				, KontoID_Level7
				, KontoLabel_Level7
				, GuV_Bilanz
				, GKV_UKV
				, Controlling_Accounting
				, HGB_IFRS
				, ProfitCenter
				, HGB_IFRS_Beide
				, Monat_gueltig
			)
			select KontoNummer
				, Name_DEU 
				, KontoID_Level	, KontoID_Level0	
				, KontoLabel_Level0
				, KontoID_Level1
				, KontoLabel_Level1
				, KontoID_Level2
				, KontoLabel_Level2
				, KontoID_Level3
				, KontoLabel_Level3
				, KontoID_Level4
				, KontoLabel_Level4
				, KontoID_Level5
				, KontoLabel_Level5
				, KontoID_Level6
				, KontoLabel_Level6
				, KontoID_Level7
				, KontoLabel_Level7
				, GuV_Bilanz
				, GKV_UKV
				, Controlling_Accounting
				, HGB_IFRS
				, ProfitCenter
				, HGB_IFRS_Beide
				, CONVERT(varchar(6),Getdate(),112)-1 as Monat_gueltig
			from #Kontenplan_Aktuell
			end
		end
		;
		
		-- Änderung von Kontonamen für die historischen Kontenpläne nachziehen
		if exists (select * from META.hist_KontoNamenAenderungen where CurrentFlag = 1)
		begin
			update kp
				set kp.KontoLabel_Level1 = case when kp.KontoID_Level1 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level1 end
				, kp.KontoLabel_Level2 = case when kp.KontoID_Level2 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level2 end
				, kp.KontoLabel_Level3 = case when kp.KontoID_Level3 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level3 end
				, kp.KontoLabel_Level4 = case when kp.KontoID_Level4 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level4 end
				, kp.KontoLabel_Level5 = case when kp.KontoID_Level5 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level5 end
				, kp.KontoLabel_Level6 = case when kp.KontoID_Level6 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level6 end
				, kp.KontoLabel_Level7 = case when kp.KontoID_Level7 like '%-K' then kn.Kontonummer +' ' + kn.Neuer_Name else kp.KontoLabel_Level7 end
				, kp.Name_DEU = kn.Kontonummer +' ' + kn.Neuer_Name
				--select *
			from core.dim_Kontenplan kp
			inner join META.hist_KontoNamenAenderungen kn on kn.Kontonummer = kp.KontoNummer
			where  kp.Name_DEU <> kn.Kontonummer +' ' + kn.Neuer_Name
				and CurrentFlag = 1
			
			update k
				set k.CurrentFlag = 0
			from META.hist_KontoNamenAenderungen k
			where CurrentFlag = 1
		end
		;
		
		-- für alte Opex Kontenpläne n.v. Konten löschen, da diese nicht bebucht sein dürfen und es sonst zu Komplikationen führt, wenn das Konto jetzt neu gesplittet wird
		delete
		from core.dim_Kontenplan
		where GKV_UKV = 'ZKV'
			and KontoID_Level0 = '-1'
			and Monat_gueltig <> 999999
	
	
		-- Sortierung setzen, nur für abweichende Sortierung, kann für weitere Level gemacht werden, sollte aber möglichst selten genutzt werden!
		
		--first delete all previous Sortierungen, so that an if an entry in mdm.Kontenplan_abweichende_Sortierung is deleted, it is also deleted in the old Kontenplan Entries
		
		update kp
			set Level2_Sortierung = null
			, Level1_Sortierung = null
		from core.dim_Kontenplan kp
		where Level1_Sortierung is not null or Level2_Sortierung is not null
		
		--Level 1
		update kp
			set Level1_Sortierung = s.Sortierung
			--select *
		from core.dim_Kontenplan kp
		left outer join META.Kontenplan_abweichende_Sortierung s
			 on kp.GuV_Bilanz = s.Financial_Statement
			and kp.HGB_IFRS = s.Rechnungslegungsnorm
			and kp.GKV_UKV = s.Kostenverfahren
			and kp.Controlling_Accounting = s.Funktionsbereich
			and kp.KontoID_Level1 = s.KontoID
		where s.Level = 1 --only works if we update Sortierung for all Monat_gueltig (else: error while cube processing)
		
		-- Level2
		update kp
			set Level2_Sortierung = s.Sortierung
			--select *
		from core.dim_Kontenplan kp
		inner join META.Kontenplan_abweichende_Sortierung s
			 on kp.GuV_Bilanz = s.Financial_Statement
			and kp.HGB_IFRS = s.Rechnungslegungsnorm
			and kp.KontoID_Level2 = s.KontoID
		where s.Level = 2 
		
		
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END

