﻿CREATE PROCEDURE [core].[sp_dim_Kontenplan_Bridge]
	
AS
BEGIN
	BEGIN TRY
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		

		truncate table core.dim_Kontenplan_Bridge
		
		/*
		--Index Löschen
		IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'core_fi.dim_Kontenplan_Bridge') AND name = N'idx_KP_Bridge')
			DROP INDEX idx_KP_Bridge ON core_fi.dim_Kontenplan_Bridge WITH ( ONLINE = OFF )
		*/
		
		Insert into core.dim_Kontenplan_Bridge(
					ID_KP_Bridge
					, ProfitCenter
					, KontoNummer
					, KontoID_Level
					, ID_Kontenplan
					, GuV_Bilanz
					, GKV_UKV
					, HGB_IFRS
					, Monat_gueltig)
		select 
			dense_rank() over(order by ProfitCenter,KontoNummer,KontoID_Level) as ID_KP_Bridge
			, ProfitCenter
			, KontoNummer
			, KontoID_Level
			, ID_Kontenplan
			, GuV_Bilanz
			, GKV_UKV
			, HGB_IFRS
			, Monat_gueltig
		from core.dim_Kontenplan
		order by ProfitCenter,KontoNummer,KontoID_Level
		
		/*
		--Index erstellen
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'core_fi.dim_Kontenplan_Bridge') AND name = N'idx_KP_Bridge') begin
			CREATE NONCLUSTERED INDEX idx_KP_Bridge ON core_fi.dim_Kontenplan_Bridge 
			(
				Monat_gueltig ASC
				, ID_KP_Bridge ASC
				, ID_Kontenplan ASC
				, KontoID_level Asc
			)
			WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		end
		*/
				
		END TRY
	
	BEGIN CATCH
	END CATCH
	
END