﻿CREATE PROCEDURE [core].[sp_fact_NetIncome_Adjustbuchungen]
--	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
	/*	
	drop table #Amount_1
	drop table #Amount_2
	drop table #TotalAmount
	drop table #TotalAmount_proKontenplan
	*/
	
		-- Variablen Deklarieren
		Declare @StartDateTime    datetime = GETDATE()
		declare @error			  varchar(max)
        Declare @KostenstellenID   int 

        -- KostenstelleID für den Konzern Code -1
        select @KostenstellenID = ID from [core].[vdim_Kostenstelle] where MandantenName = 'Konzern' and Code = -1
		
		-- Net Income Adjustbuchungen löschen
		delete wb
		from core.fact_Zusatzbuchungen wb
		inner join core.dim_Level d
			 on wb.LevelID = d.LevelID
		where wb.KontoNummer in ('d86000', 'd86000H', 'd86000I')
		  and d.Typ in ('Konsolidation', 'Basic')
		
		
		-- Netincome Adjust Buchungen Amount ermitteln--------------------------------------------------------------------------------------
		--aus Buchungen
		select isnull(sum(a.Amount),0) *-1 as sumAmount
			, isnull(sum(a.Amount_EUR),0) *-1 as sumAmount_EUR
			, y.KontoID_Level
			, y.HGB_IFRS
			, a.PostingDateID/100 as PostingMonat
			, a.MandantenID
			, a.ID_Nachbuchungen
			, 1 as LevelID
			, 0 as KonsolidierungsID
			, Currency
		into #Amount_1
		from core.fact_Buchungen a
		inner join core.dim_Kontenplan_Bridge y on a.ID_KP_Bridge = y.ID_KP_Bridge and Monat_gueltig = 999999
		where y.GuV_Bilanz = 'GuV'
		  and a.Ultimobuchung = 'N'
		  and a.KontoNummer not in ('d999001', 'd999001H', 'd999001I') -- d999001 Inventory Allowance darf beim Net Income nicht berücksichtigt werden.
		group by y.KontoID_Level, HGB_IFRS, a.PostingDateID/100, a.MandantenID, a.ID_Nachbuchungen, Currency
			 
			  
		--aus Zusatzbuchungen
		select isnull(sum(a.Amount),0) *-1 as sumAmount
			, isnull(sum(a.Amount_EUR),0) *-1 as sumAmount_EUR
			, y.KontoId_Level
			, y.HGB_IFRS
			, a.PostingMonat
			, a.MandantenID
			, a.ID_Nachbuchungen
			, a.LevelID
			, a.KonsolidierungsID
			, a.Currency
		into #Amount_2             
		from core.fact_Zusatzbuchungen a
		inner join core.dim_Kontenplan_Bridge y on a.ID_KP_Bridge = y.ID_KP_Bridge and Monat_gueltig = 999999
		where y.GuV_Bilanz = 'GuV'
		  and y.KontoNummer not in ('d999001', 'd999001H', 'd999001I') -- d999001 Inventory Allowance darf beim Net Income nicht berücksichtigt werden.
		  and a.LevelID <> core.Get_dim_LevelID('Planung')
		group by y.KontoID_Level, y.HGB_IFRS, a.PostingMonat, a.MandantenID, a.ID_Nachbuchungen, a.LevelID, a.KonsolidierungsID, a.Currency
		 
		--Buchungen und Zusatzbuchungen vereinigen
		Select 
			sum(sumAmount) as totalAmount
			, sum(sumAmount_EUR) as totalAmount_EUR
			, v.KontoID_Level
			, v.HGB_IFRS
			, v.PostingMonat
			, v.MandantenID
			, isnull(v.ID_Nachbuchungen,0) as ID_Nachbuchungen
			, v.LevelID
			, isnull(v.KonsolidierungsID,0) as KonsolidierungsID
			, v.Currency
		into #TotalAmount_ProKontenplan
		from (Select * from #Amount_1 a
				union all 
				Select * from #Amount_2 b) v
		group by v.KontoID_Level, v.HGB_IFRS, v.PostingMonat, v.MandantenID, isnull(v.ID_Nachbuchungen,0), v.LevelID, isnull(v.KonsolidierungsID,0), v.Currency
		

		--Überprüfen ob für alle GuV-Kontenpläne die gleiche Summe heraus kommt (jeweils für HGB und IFRS). Wenn nicht, eine Email senden.
/*		if exists( select COUNT(*) as Anzahl,PostingMonat,MandantenID,HGB_IFRS,ID_Nachbuchungen,LevelID, KonsolidierungsID, currency from (
			 select distinct totalAmount,HGB_IFRS,PostingMonat,MandantenID,ID_Nachbuchungen, LevelID, KonsolidierungsID, currency from #TotalAmount_ProKontenplan
			  ) v
			group by PostingMonat,MandantenID,HGB_IFRS,ID_Nachbuchungen,LevelID, KonsolidierungsID, currency
			having COUNT(*)>1
			)
		begin
			select DB_NAME() as Name into ##DB_Name
			
			declare @recipientList varchar(max)
			set @recipientList = dbo.get_Emailverteiler('Fibu_Tech')
		
			EXEC msdb.dbo.sp_send_dbmail
			@recipients = @recipientList,
			@subject = '!Die NetIncome_Adjustbuchungen sind nicht für alle GuV Kontenpläne identisch!',
			@body = 'Warnung: Die NetIncome_Adjustbuchungen sind nicht für alle GuV Kontenpläne identisch',
			@query = 'select Name from ##DB_Name';
			
			drop table ##DB_Name
		end
*/		
		select max(totalAmount) as totalAmount
			, max(totalAmount_EUR) as totalAmount_EUR
			, MandantenID
			, HGB_IFRS
			, PostingMonat 
			, ID_Nachbuchungen
			, LevelID
			, KonsolidierungsID
			, Currency
		into #TotalAmount
		from #TotalAmount_ProKontenplan
		group by HGB_IFRS,PostingMonat,MandantenID,ID_Nachbuchungen, LevelID, KonsolidierungsID, Currency
	
			  
		--Netincome Adjustbuchungen für jeden Monat auf die Konten d86000/H/I schreiben
		--Buchung immer in Euro, da sonst beim Bilanzieren der falsche Wechselkurs (Bilanz) verwendet wird.
			
		--HGB
		 insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency)
		select totalAmount_EUR
			, totalAmount_EUR
			, MandantenID
			, @KostenstellenID
			, convert(varchar,a.PostingMonat) as PostingMonat
			, d.ID_KP_Bridge
			, case when core.Get_dim_Mandanten_Name(MandantenID) in ('Konzern','Konsolidierung Schweiz') then core.Get_dim_LevelID('NetIncome-Adjustbuchungen')
						else core.Get_dim_LevelID('Basic') end as LevelID
			, getdate() as MS_AUDIT_TIME
			, system_user as MS_AUDIT_USER
			, d.KontoNummer
			, ID_Nachbuchungen
			, KonsolidierungsID
			, 'EUR' as Currency
		from #TotalAmount a
		inner join core.dim_Kontenplan_Bridge d
			 on d.KontoNummer = 'd86000H' 
			and d.HGB_IFRS = 'HGB' 
			and d.GuV_Bilanz = 'Bilanz'
			and Monat_gueltig = 999999
		where a.HGB_IFRS = 'HGB'
			and a.PostingMonat >= 201401 --erst ab diesem Datum wird zwischen HGB und IFRS unterschieden
			and a.PostingMonat <= convert(varchar,year(getdate()))+'12' --nur Buchungen bis zum Ende des Jahres anzeigen
		  
		  --IFRS
		insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency)
		select totalAmount_EUR
			, totalAmount_EUR
			, MandantenID
			, @KostenstellenID
			, convert(varchar,a.PostingMonat) as PostingMonat
			, d.ID_KP_Bridge
			, case when core.Get_dim_Mandanten_Name(MandantenID) in ('Konzern','Konsolidierung Schweiz') then core.Get_dim_LevelID('NetIncome-Adjustbuchungen')
						else core.Get_dim_LevelID('Basic') end
			, getdate()
			, system_user
			, d.KontoNummer
			, ID_Nachbuchungen
			, KonsolidierungsID
			, 'EUR' as Currency
		from #TotalAmount a
		inner join core.dim_Kontenplan_Bridge d
			 on d.KontoNummer = 'd86000I' 
			and d.HGB_IFRS = 'IFRS' 
			and d.GuV_Bilanz = 'Bilanz'
			and Monat_gueltig = 999999
		where a.HGB_IFRS = 'IFRS'
			and a.PostingMonat >= 201401  --erst ab diesem Datum wird zwischen HGB und IFRS unterschieden
			and a.PostingMonat <= convert(varchar,year(getdate()))+'12' --nur Buchungen bis zum Ende des Jahres anzeigen
		
		  
		  --Keine Unterteilung in HGB/IFRS vor 2014
		  insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency)
		select totalAmount_EUR
			, totalAmount_EUR
			, MandantenID
			, @KostenstellenID
			, convert(varchar,a.PostingMonat) as PostingMonat
			, d.ID_KP_Bridge
			, case when core.Get_dim_Mandanten_Name(MandantenID) in ('Konzern','Konsolidierung Schweiz') then core.Get_dim_LevelID('NetIncome-Adjustbuchungen')
						else core.Get_dim_LevelID('Basic') end
			, getdate()
			, system_user
			, d.KontoNummer
			, ID_Nachbuchungen
			, KonsolidierungsID
			, 'EUR' as Currency
		from #TotalAmount a
		inner join core.dim_Kontenplan_Bridge d
			 on d.KontoNummer = 'd86000' 
			and d.HGB_IFRS = a.HGB_IFRS 
			and d.GuV_Bilanz = 'Bilanz'
			and Monat_gueltig = 999999
		where a.PostingMonat < 201401
			and a.PostingMonat >= 201201 -- in diesem Zeitraum wird noch nicht zwischen HGB und IFRS unterschieden
		
	END TRY
	
	BEGIN CATCH

	END CATCH
END
