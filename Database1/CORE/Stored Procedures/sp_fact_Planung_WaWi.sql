﻿
CREATE proc [CORE].[sp_fact_Planung_WaWi]
	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- Variablen deklarieren
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		insert into core.Zusatzbuchungen_intermediate (
			Amount
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, LevelID
			, ProfitCenterID
		)
		select p.Planwert as Amount
			, C.id as MandantenID
			, d.KostenstellenID
			, p.PostingMonat
			, getdate()
			, SYSTEM_USER
			, p.Kontonummer
			, core.Get_dim_LevelID('Planung') as LevelID
			, d.ProfitCenterID
		from stg.Plandaten_WaWi p
		inner join core.dim_Mandant c on c.MandantNAV = 'Konzern'
		left outer join (select Code, MIN(ProfitCenterID) as ProfitCenterID, MIN(ID) as KostenstellenID
						from core.[dim_Dimension Value]
						where [Dimension Code] = 'KOSTENSTELLE'
						group by Code) d
			 on p.Kostenstelle = d.Code
			
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END
