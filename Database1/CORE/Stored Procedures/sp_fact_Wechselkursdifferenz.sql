﻿Create proc [core].[sp_fact_Wechselkursdifferenz]
	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- Variablen deklarieren
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		declare @rowcount int
		
		/*
		delete from core_fi.fact_Bilanz_Zusatzbuchung
		where Kontonummer = '085150'
		*/

		select sum(bu.Amount_EUR) as Amount_EUR
			, bu.Monat
			, bu.ID_Nachbuchungen
			, bu.LevelID
			, bu.MandantenID
			, bu.OneOffID
		into #Wechselkursdifferenz
		from core.fact_Bilanz bu
		inner join core.dim_Kontenplan_Bridge kb 
			on bu.ID_KP_Bridge = kb.ID_KP_Bridge 
			and kb.Monat_gueltig = '999999'
			and kb.KontoID_Level='A'
		where bu.MandantenID = 14
		group by bu.Monat
			, bu.ID_Nachbuchungen
			, bu.LevelID
			, bu.MandantenID
			, bu.OneOffID
			
		Insert into fact_Bilanz(
			Amount_EUR
			, MandantenID
			, Monat
			, LOAD_DATE
			, KontoNummer
			, LevelID
			, OneOffID
			, ID_KP_Bridge
			, ID_Nachbuchungen
		)
		select -1 * w.Amount_EUR
			, w.MandantenID
			, w.Monat
			, GETDATE() as MS_AUDIT_TIME
			, kb.KontoNummer
			, LevelID
			, w.OneOffID
			, kb.ID_KP_Bridge
			, ID_Nachbuchungen
		from #Wechselkursdifferenz w
		inner join core.dim_Kontenplan_Bridge kb
			on kb.KontoNummer = '085150'
			and Monat_gueltig = 999999
		order by Monat
		
		
		set @rowcount = @@ROWCOUNT
				
	END TRY
	
	BEGIN CATCH

	END CATCH
	
END