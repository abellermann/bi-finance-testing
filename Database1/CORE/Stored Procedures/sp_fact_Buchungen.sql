﻿CREATE PROCEDURE [core].[sp_fact_Buchungen]
--	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		Declare @StartDateTime	datetime = getdate()
		Declare @StartDateTimeStep	datetime = getdate()
		declare @error			varchar(max)

		-- -----------------------------
		-- Profitcenter 2 und 3 zuordnen
		-- -----------------------------
		
		
		IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'core.fact_Buchungen') AND name = N'idx_fact_Buchungen')
		DROP INDEX idx_fact_Buchungen ON core_fi.fact_Buchungen WITH ( ONLINE = OFF )
		
		
		-- für Kostenstellen in Kostenstellen_PC2 das Profitcenter2 entsprechend setzen
		update f
			set f.ProfitCenterID_2 = d.ID
		-- select kst.Code, f.ProfitCenterID, d.ID
		from core.GLEntry_intermediate f
		inner join core.[dim_Dimension Value] kst
			 on f.KostenstellenID = kst.ID
		inner join (select * from meta.Kostenstellen_PC2) p
			 on kst.Code = p.Kostenstelle
		inner join core.dim_ProfitCenter d
			 on p.Profit_Center = d.ProfitCenter_kurz
		;
		-- Profitcenter3 setzen, wo Profitcenter 1 nicht online oder offline und Profitcenter 2 nicht gesetzt ist
		
		Select  ID
			, EntryNo
			, KontoNummer
			, KontoNummer_Alt
			, PostingDateID
			, BelegartID
			, Amount * isnull(Faktor,1) as Amount
			, GegenKontoNummer
			, LOAD_DATE
			, MandantenID
			, Ultimobuchung
			, KostenstellenID
			, OneOffID
			, LevelID
			, Nullstellung
			, ProfitCenterID
			, ProfitCenterID_2
			, zu_ProfitCenterID as ProfitCenterID_3
			, CapexItID
			, MandantEntryNo
			, ID_Nachbuchungen
			, 10000000 as EingabeDatumID
			, Amount_EUR * isnull(Faktor,1) as Amount_EUR
			, Currency
		into #Buchungen_temp
		from core.GLEntry_intermediate b
		left outer join core.allokationsSchluessel_ProfitCenter al
			on b.PostingDateID/100 = al.PostingMonat 
			and b.ProfitCenterID = al.von_ProfitCenterID
			and b.ProfitCenterID_2 is null 
		;			
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg ProfitcenterID2 und 3 gesetzt'
		set @StartDateTimeStep = getDate()
		
		
/*		Update b
			set b.EingabeDatumId = ma.Buchungsausfuehrungsdatum
		from #Buchungen_temp b
		inner join dbo.EntryNoDateMapping ma
			on ma.MandantenID = b.MandantenID
				and ma.EntryNo = b.EntryNo	
*/		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'EingabeDatum zugeordnet'
		set @StartDateTimeStep = getDate()
		
		
		-- Faktentabellen leeren
		Truncate table core_fi.fact_Buchungen
		
		-- ----------------------
		-- Kontenplan ID zuordnen
		-- ----------------------

		--Buchungen auf Konten, die unabhängig vom Profitcenter und der Kostenstelle die gleiche Position im Kontenplan haben
		insert into core.fact_Buchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [KontoNummer_Alt] 
			, [PostingDateID] 
			, [Amount] 
			, [GegenKontoNummer] 
			, [LOAD_DATE] 
			, [MandantenID] 
			, [Ultimobuchung] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [Nullstellung] 
			, [ProfitCenterID] 
			,  ProfitCenterID_2
			,  ProfitCenterID_3
			, [CapexItID] 
			, [MandantEntryNo]
			, ID_KP_Bridge
			, ID_Nachbuchungen
			, EingabeDatumId
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, f.KontoNummer 
			, f.KontoNummer_Alt 
			, f.PostingDateID 
			, case when d.GuV_Bilanz = 'GuV' then -f.Amount else f.Amount end
			, f.GegenKontoNummer 
			, f.LOAD_DATE 
			, f.MandantenID 
			, f.Ultimobuchung 
			, f.KostenstellenID 
			, f.OneOffID 
			, f.LevelID 
			, f.Nullstellung 
			, f.ProfitCenterID 
			, f.ProfitCenterID_2
			, f.ProfitCenterID_3
			, f.CapexItID 
			, f.MandantEntryNo
			, d.ID_KP_Bridge 
			, ID_Nachbuchungen
			, EingabeDatumId
			, case when d.GuV_Bilanz = 'GuV' then -f.Amount_EUR else f.Amount_EUR end
			, Currency
		from #Buchungen_temp f
		inner join core.dim_Kontenplan_Bridge d on f.KontoNummer = d.KontoNummer 
		where d.ProfitCenter = 'alle'
		and d.Monat_gueltig = 999999
		;
		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg GKV Buchungen zugeordnet'
		set @StartDateTimeStep = getDate()

							
		--Buchungen auf Konten, bei denen die Position im Kontenplan abhängig vom Profitcenter ist (ZKV-Kontenplan)
		insert into core.fact_Buchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [KontoNummer_Alt] 
			, [PostingDateID] 
			, [Amount] 
			, [GegenKontoNummer] 
			, [LOAD_DATE] 
			, [MandantenID] 
			, [Ultimobuchung] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [Nullstellung] 
			, [ProfitCenterID] 
			,  ProfitCenterID_2
			,  ProfitCenterID_3
			, [CapexItID] 
			, [MandantEntryNo]
			, ID_KP_Bridge
			, ID_Nachbuchungen
			, EingabeDatumId
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, f.KontoNummer 
			, f.KontoNummer_Alt 
			, f.PostingDateID 
			, case when d.GuV_Bilanz = 'GuV' then -f.Amount else f.Amount end
			, f.GegenKontoNummer 
			, f.LOAD_DATE 
			, f.MandantenID 
			, f.Ultimobuchung 
			, f.KostenstellenID 
			, f.OneOffID 
			, f.LevelID 
			, f.Nullstellung 
			, f.ProfitCenterID 
			, f.ProfitCenterID_2
			, f.ProfitCenterID_3
			, f.CapexItID 
			, f.MandantEntryNo
			, d.ID_KP_Bridge  
			, ID_Nachbuchungen
			, EingabeDatumId
			, case when d.GuV_Bilanz = 'GuV' then -f.Amount_EUR else f.Amount_EUR end
			, Currency
		from #Buchungen_temp f
			inner join core.dim_ProfitCenter pc on f.ProfitCenterID = pc.ID
			inner join core.dim_Kontenplan_Bridge d 
				on f.KontoNummer = d.KontoNummer 
					and pc.ProfitCenter_zwischen_kurz = d.ProfitCenter
		where d.GKV_UKV = 'ZKV'
		and d.Monat_gueltig = 999999
		;
		
		--Buchungen auf Konten, bei denen die Position im Kontenplan abhängig vom Profitcenter ist, das Profitcenter der Buchung aber unbekannt ist. (ZKV-Kontenplan)
		--Diese werden alle dem DummyKonto mit der Nummer 9999999 zugeordnet !ACHTUNG, wenn dieses Konto nicht existiert, gehen diese Buchungen verloren!
		insert into core.fact_Buchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [KontoNummer_Alt] 
			, [PostingDateID] 
			, [Amount] 
			, [GegenKontoNummer] 
			, [LOAD_DATE] 
			, [MandantenID] 
			, [Ultimobuchung] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [Nullstellung] 
			, [ProfitCenterID]
			, ProfitCenterID_2
			, ProfitCenterID_3
			, [CapexItID] 
			, [MandantEntryNo]
			, ID_KP_Bridge 
			, ID_Nachbuchungen
			, EingabeDatumId
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, KontoNummer 
			, KontoNummer_Alt 
			, PostingDateID 
			, case when GuV_Bilanz = 'GuV' then -Amount else Amount end
			, GegenKontoNummer 
			, LOAD_DATE 
			, MandantenID 
			, Ultimobuchung 
			, KostenstellenID 
			, OneOffID 
			, LevelID 
			, Nullstellung 
			, ProfitCenterID 
			, ProfitCenterID_2
			, ProfitCenterID_3
			, CapexItID 
			, MandantEntryNo
			, ID_KP_Bridge  
			, ID_Nachbuchungen
			, EingabeDatumId
			, case when GuV_Bilanz = 'GuV' then -Amount_EUR else Amount_EUR end
			, Currency
		from
		(
			Select f.*
				, kp.ID_KP_Bridge
				, d.GuV_Bilanz
			from #Buchungen_temp f
			inner join (select distinct Kontonummer
							, GuV_Bilanz
							, KontoID_Level 
						from core.dim_Kontenplan_Bridge
						where ProfitCenter <> 'alle'
							and GKV_UKV = 'ZKV'
							and Monat_gueltig = 999999) d 
					on f.KontoNummer = d.KontoNummer 
			inner join core.dim_Kontenplan_Bridge kp 
				on d.KontoID_Level = kp.KontoID_Level 
				and kp.KontoNummer = '9999999' 
				and kp.Monat_gueltig = 999999
			where isnull(f.ProfitCenterID,-1) = -1
		) g
		;
		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg ZKV Buchungen zugeordnet'
		set @StartDateTimeStep = getDate()
	
		--Buchungen auf Konten, bei denen die Position im Kontenplan abhängig von der Kostenstelle ist (UKV-Kontenplan)
		insert into core.fact_Buchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [KontoNummer_Alt] 
			, [PostingDateID] 
			, [Amount] 
			, [GegenKontoNummer] 
			, [LOAD_DATE] 
			, [MandantenID] 
			, [Ultimobuchung] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [Nullstellung] 
			, [ProfitCenterID] 
			, ProfitCenterID_2
			, ProfitCenterID_3
			, [CapexItID] 
			, [MandantEntryNo]
			, ID_KP_Bridge 
			, ID_Nachbuchungen
			, EingabeDatumId
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, f.KontoNummer 
			, f.KontoNummer_Alt 
			, f.PostingDateID 
			, case when d.GuV_Bilanz = 'GuV' then -f.Amount else f.Amount end
			, f.GegenKontoNummer 
			, f.LOAD_DATE 
			, f.MandantenID 
			, f.Ultimobuchung 
			, f.KostenstellenID 
			, f.OneOffID 
			, f.LevelID 
			, f.Nullstellung 
			, f.ProfitCenterID 
			, f.ProfitCenterID_2
			, f.ProfitCenterID_3
			, f.CapexItID 
			, f.MandantEntryNo
			, d.ID_KP_Bridge 
			, ID_Nachbuchungen
			, EingabeDatumId
			, case when d.GuV_Bilanz = 'GuV' then -f.Amount_EUR else f.Amount_EUR end
			, Currency
		from #Buchungen_temp f
			inner join core.[dim_Dimension Value] ks on f.KostenstellenID = ks.ID
			inner join core.dim_Kontenplan_Bridge d 
				on f.KontoNummer = d.KontoNummer 
					and ks.Code = d.ProfitCenter
			where GKV_UKV = 'UKV'
			and d.Monat_gueltig = 999999
		;	
			
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg UKV Buchungen zugeordnet'
		set @StartDateTimeStep = getDate()
		;
		
		-- -------------------------------------
		-- Ausnahmen für Profit- und Cost-Center
		-- -------------------------------------
		
		-- Level2, Anteil 100%, für alle Monate
		update f
			set f.ProfitCenterID = d.zu
				, f.Load_Date = Null --Datensätze markieren, bei denen sich das Profitcenter ändert
		--select *
		from core.fact_Buchungen f
		inner join 
			(select distinct KontoNummer 
				, core.Get_dim_ProfitCenterID(a.von) as von
				, core.Get_dim_ProfitCenterID(a.zu) zu
				, d.GKV_UKV
				from core.dim_Kontenplan d
			 	inner join meta.ProfitCenter_Ausnahmen a
					on d.KontoID_Level2 = a.EbenenCode and d.GKV_UKV = 'GKV'
				where a.Ebene = 2
					and (a.Monat is null or a.Monat = '')
					and a.Anteil = 1
					and d.Monat_gueltig = 999999
					) d
			 on f.KontoNummer = d.KontoNummer
			and f.ProfitCenterID = d.von
		;
			
		-- Level3, Anteil 100%, für alle Monate
		update f
			set f.ProfitCenterID = d.zu
			, Load_Date = Null --Datensätze markieren, bei denen sich das Profitcenter ändert
		--select *
		from core.fact_Buchungen f
		inner join 
			(select distinct KontoNummer 
				, core.Get_dim_ProfitCenterID(a.von) as von
				, core.Get_dim_ProfitCenterID(a.zu) zu
				from core.dim_Kontenplan d
			 	inner join meta.ProfitCenter_Ausnahmen a
					on d.KontoID_Level3 = a.EbenenCode and d.GKV_UKV = 'GKV'
				where a.Ebene = 3
					and (a.Monat is null or a.Monat = '')
					and a.Anteil = 1
					and d.Monat_gueltig = 999999
			) d
			 on f.KontoNummer = d.KontoNummer
			and f.ProfitCenterID = d.von
		;
		
		-- Level2, pro Monat: Zeilen doppeln, je eine für ProfitCenterID von und zu, mit Amount von jeweiligem Anteil
		insert into core.fact_Buchungen
			(EntryNo
			, MandantEntryNo
			, KontoNummer
			, KontoNummer_Alt
			, PostingDateID
			, Amount
			, GegenKontoNummer
			, LOAD_DATE
			, MandantenID
			, Ultimobuchung
			, KostenstellenID
			, OneOffID
			, LevelID
			, Nullstellung
			, ProfitCenterID
			, CapexItID
			, ID_KP_Bridge
			, ID_Nachbuchungen
			, EingabeDatumId
			, Amount_EUR
			, Currency)
		select f.EntryNo
			, f.MandantEntryNo
			, f.KontoNummer
			, f.KontoNummer_Alt
			, f.PostingDateID
			, f.Amount * d.Anteil
			, f.GegenKontoNummer
			, Null as LOAD_DATE --Datensätze markieren, bei denen sich das Profitcenter ändert
			, f.MandantenID
			, f.Ultimobuchung
			, f.KostenstellenID
			, f.OneOffID
			, f.LevelID
			, f.Nullstellung
			, d.zu as ProfitCenterID
			, f.CapexItID
			, f.ID_KP_Bridge
			, ID_Nachbuchungen
			, EingabeDatumId
			, Amount_EUR * d.Anteil
			, Currency
		from core.fact_Buchungen f
		inner join 
			(select distinct d.KontoNummer 
				, core.Get_dim_ProfitCenterID(a.von) as von
				, core.Get_dim_ProfitCenterID(a.zu) zu
				, a.Anteil
				, a.Monat
				from core.dim_Kontenplan d
			 	inner join meta.ProfitCenter_Ausnahmen a
					on d.KontoID_Level2 =  a.EbenenCode and d.GKV_UKV = 'GKV'
				where a.Ebene = 2
					and (a.Monat is not null and a.Monat <> '')
					and d.Monat_gueltig = 999999
			) d
			 on f.KontoNummer = d.KontoNummer
			and f.ProfitCenterID = d.von
			and left(f.PostingDateID, 6) = d.Monat
		;
			
		update f
			set f.Amount = f.Amount * (1 - d.Anteil)
			, f.Amount_EUR = f.Amount_EUR * (1 - d.Anteil)
			, Load_Date = Null --Datensätze markieren, bei denen sich das Profitcenter ändert
		--select *
		from core.fact_Buchungen f
		inner join 
			(select distinct d.KontoNummer 
				, core.Get_dim_ProfitCenterID(a.von) as von
				, core.Get_dim_ProfitCenterID(a.zu) zu
				, a.Anteil
				, a.Monat
				from core.dim_Kontenplan d
			 	inner join meta.ProfitCenter_Ausnahmen a
					on d.KontoID_Level2 = a.EbenenCode and d.GKV_UKV = 'GKV'
				where a.Ebene = 2
					and (a.Monat is not null and a.Monat <> '')
					and d.Monat_gueltig = 999999
			) d
			 on f.KontoNummer = d.KontoNummer
			and f.ProfitCenterID = d.von
			and left(f.PostingDateID, 6) = d.Monat
		;
		
		-- Kontenplan ID für die Konten im ZKV-Plan mit geändertem PC neu setzen, wenn nötig	
		update b
			set b.ID_KP_Bridge = kpnew.ID_KP_Bridge
			, LOAD_DATE = GETDATE()
		--select *
		from core.fact_Buchungen b
		inner join (select br.KontoID_Level
						, br.GKV_UKV
						,br.ID_KP_Bridge

				from core.dim_Kontenplan_Bridge br
				where br.Monat_gueltig = 999999 
					and br.GKV_UKV = 'ZKV'
				) kpold 
				on kpold.ID_KP_Bridge = b.ID_KP_Bridge
		inner join core.dim_ProfitCenter pc on b.ProfitCenterID = pc.ID
		inner join (select br.KontoID_Level
						, GKV_UKV
						, ID_KP_Bridge
						, KontoNummer
						, ProfitCenter
				 from core.dim_Kontenplan_Bridge br 
				 where br.Monat_gueltig = 999999 
					and br.GKV_UKV = 'ZKV'
				 ) kpnew 
			 on b.KontoNummer = kpnew.KontoNummer 
			and (pc.ProfitCenter_zwischen_kurz = kpnew.ProfitCenter)
			and kpnew.KontoID_Level = kpold.KontoID_Level
		where LOAD_DATE is null
		;
		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg Profitcenter Umverteilung (ZKV)'

		--index anlegen
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'core_fi.fact_Buchungen') AND name = N'idx_fact_Buchungen') begin
			CREATE NONCLUSTERED INDEX idx_fact_Buchungen ON core.fact_Buchungen 
			(
				ID_KP_Bridge ASC
			)
			WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		end
		
	END TRY
	
	BEGIN CATCH
	END CATCH
	
	END