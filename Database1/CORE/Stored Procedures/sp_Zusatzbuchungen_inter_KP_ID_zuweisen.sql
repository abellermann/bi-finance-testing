﻿CREATE proc [core].[sp_Zusatzbuchungen_inter_KP_ID_zuweisen]
--	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		
		-- Variablen deklarieren
		declare @StartDateTime datetime = GETDATE()
		Declare @StartDateTimeStep	datetime = getdate()
		declare @error varchar(max)
		
		-- -----------------------------
		-- Profitcenter 2 und 3 zuordnen
		-- -----------------------------
		
		-- für Kostenstellen in Kostenstellen_PC2 das Profitcenter2 entsprechend setzen
		update f
			set f.ProfitCenterID_2 = d.ID
		-- select kst.Code, f.ProfitCenterID, d.ID
		from core.Zusatzbuchungen_intermediate f
		inner join core.[dim_Dimension Value] kst
			 on f.KostenstellenID = kst.ID
		inner join (select * from meta.Kostenstellen_PC2) p
			 on kst.Code = p.Kostenstelle
		inner join core.dim_ProfitCenter d
			 on p.Profit_Center = d.ProfitCenter_kurz
		
		-- Profitcenter3 setzen, wo Profitcenter 1 nicht online oder offline und Profitcenter 2 nicht gesetzt ist
		Select EntryNo
			, KontoNummer
			, b.PostingMonat
			, Amount * isnull(Faktor,1) as Amount
			, MandantenID
			, KostenstellenID
			, OneOffID
			, LevelID
			, ProfitCenterID
			, ProfitCenterID_2
			, zu_ProfitCenterID as ProfitCenterID_3
			, CapexItID
			, MandantEntryNo
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, isnull(EingabeDatumID,19980101) as EingabeDatumID
			, KonsolidierungsID
			, Amount_EUR * isnull(Faktor,1) as Amount_EUR
			, Currency
		into #Zusatzbuchungen_temp
		from core.Zusatzbuchungen_intermediate b
		left outer join core.allokationsSchluessel_ProfitCenter al
			on b.PostingMonat = al.PostingMonat 
			and b.ProfitCenterID = al.von_ProfitCenterID
			and b.ProfitCenterID_2 is null 
			
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg ProfitcenterID2 und 3 gesetzt'
		set @StartDateTimeStep = getDate()
			

		--Kontenplan IDs zuweisen
		--Buchungen auf Konten, die unabhängig vom Profitcenter die gleiche Position im Kontenplan haben
		insert into core.fact_Zusatzbuchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [Amount] 
			, [MandantenID] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [ProfitCenterID] 
			, [CapexItID] 
			, [MandantEntryNo]
			, [ID_KP_Bridge]
			, PostingMonat 
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, f.KontoNummer 
			, f.Amount
			, f.MandantenID 
			, f.KostenstellenID 
			, f.OneOffID 
			, f.LevelID 
			, f.ProfitCenterID 
			, f.CapexItID 
			, f.MandantEntryNo
			, d.ID_KP_Bridge 
			, f.PostingMonat
			, f.MS_AUDIT_TIME
			, f.MS_AUDIT_USER
			, f.AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		from #Zusatzbuchungen_temp f
		inner join core.dim_Kontenplan_Bridge d on f.KontoNummer = d.KontoNummer 
		where d.ProfitCenter = 'alle'
		  and d.Monat_gueltig = 999999
		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg GKV Buchungen zugeordnet'
		set @StartDateTimeStep = getDate()
		
		--Buchungen auf Konten, bei denen die Position im Kontenplan abhängig vom Profitcenter ist (ZKV-Kokntenplan)
		insert into core.fact_Zusatzbuchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [Amount] 
			, [MandantenID] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [ProfitCenterID] 
			, [CapexItID] 
			, [MandantEntryNo]
			, [ID_KP_Bridge]
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, f.KontoNummer 
			, f.Amount
			, f.MandantenID 
			, f.KostenstellenID 
			, f.OneOffID 
			, f.LevelID 
			, f.ProfitCenterID 
			, f.CapexItID 
			, f.MandantEntryNo
			, d.ID_KP_Bridge 
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		from #Zusatzbuchungen_temp f
		inner join core.dim_ProfitCenter pc on f.ProfitCenterID = pc.ID
		inner join core.dim_Kontenplan_Bridge d 
			 on f.KontoNummer = d.KontoNummer 
			and (pc.ProfitCenter_zwischen_kurz = d.ProfitCenter)
		where d.GKV_UKV = 'ZKV'
		  and d.Monat_gueltig = 999999

		--Buchungen auf Konten, bei denen die Position im Kontenplan abhängig vom Profitcenter ist, das Profitcenter der Buchung aber unbekannt ist.
		-- Diese werden alle dem DummyKonto mit der Nummer 9999999 zugeordnet !ACHTUNG, wenn dieses Konto nicht existiert, gehen diese Buchungen verloren!
		insert into core.fact_Zusatzbuchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [Amount] 
			, [MandantenID] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [ProfitCenterID] 
			, [CapexItID] 
			, [MandantEntryNo]
			, [ID_KP_Bridge] 
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, KontoNummer 
			, Amount
			, MandantenID 
			, KostenstellenID 
			, OneOffID 
			, LevelID 
			, ProfitCenterID 
			, CapexItID 
			, MandantEntryNo
			, ID_KP_Bridge
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		--Select * 
		from
		(
			Select f.*
				, kp.ID_KP_Bridge
			from #Zusatzbuchungen_temp f
				inner join (select distinct Kontonummer
									, KontoID_Level 
							from core.dim_Kontenplan_Bridge
							where ProfitCenter <> 'alle'
								and GKV_UKV = 'ZKV'
								and Monat_gueltig = 999999
							) d
					 on f.KontoNummer = d.KontoNummer 
				inner join core.dim_Kontenplan_Bridge kp
					 on d.KontoID_Level = kp.KontoID_Level 
					 and kp.KontoNummer = '9999999'
					 and kp.Monat_gueltig = 999999	 
				where isnull(f.ProfitCenterID,-1) = -1
		) g
		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg ZKV Buchungen zugeordnet'
		set @StartDateTimeStep = getDate()
		
		
		--Buchungen auf Konten, bei denen die Position im Kontenplan abhängig von der Kostenstelle ist (UKV-Kontenplan)
		insert into core.fact_Zusatzbuchungen(
			[EntryNo] 
			, [KontoNummer] 
			, [Amount] 
			, [MandantenID] 
			, [KostenstellenID] 
			, [OneOffID] 
			, [LevelID] 
			, [ProfitCenterID] 
			, [CapexItID] 
			, [MandantEntryNo]
			, [ID_KP_Bridge] 
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		)
		Select EntryNo 
			, f.KontoNummer 
			, f.Amount
			, f.MandantenID 
			, f.KostenstellenID 
			, f.OneOffID 
			, f.LevelID 
			, f.ProfitCenterID 
			, f.CapexItID 
			, f.MandantEntryNo
			, d.ID_KP_Bridge
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, AdjustmentID
			, EingabeDatumId
			, KonsolidierungsID
			, Amount_EUR
			, Currency
		from #Zusatzbuchungen_temp f
		inner join core.[dim_Dimension Value] ks on f.KostenstellenID = ks.ID
		inner join core.dim_Kontenplan_Bridge d 
			on f.KontoNummer = d.KontoNummer 
			and ks.Code = d.ProfitCenter
		where GKV_UKV = 'UKV'	
		  and d.Monat_gueltig = 999999	 
		
--		exec dbo.p_protokoll @RUNID, @@PROCID, @@PROCID, 0, @StartDateTimeStep, 'Erfolg UKV Buchungen zugeordnet'
		set @StartDateTimeStep = getDate()
		
	END TRY
	
	BEGIN CATCH
	END CATCH
END