﻿
CREATE PROCEDURE [CORE].[sp_fact_Kapital_Konsobuchungen]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		-- Variablen deklarieren
		Declare @StartDateTime                datetime = GETDATE()
		declare @error						  varchar(max)
		Declare @Stichtag                     int = null	
		Declare @Datum                        datetime	
        Declare @DatumAktuell                 datetime = dateadd(mm,+1,getdate())          
        Declare @PostingMonat                 int = 0

		
		-- Kapitalkonsolidierung aus meta.Kapitalkonsolidierung eintraegen
		
        set @Datum        = (select cast(MIN(von_Monat) as CHAR(6)) + '01' from meta.Kapitalkonsolidierung)
		set @Stichtag     = CONVERT(varchar(8),(@Datum - day(@Datum)),112)  
        set @PostingMonat = @Stichtag/100
        
        -- Alle Stichtage durchlaufen 
		while @Datum <= @DatumAktuell begin
		
			insert into core.Zusatzbuchungen_intermediate
				(Amount
				, MandantenID
				, KostenstellenID
				, PostingMonat
				, KontoNummer
				, LevelID
				, ProfitCenterID
				, MS_AUDIT_TIME
				, MS_AUDIT_USER
				, KonsolidierungsID
				, currency)
			select
				k.Amount
				, C.ID as MandantenID
				, kst.ID as KostenstellenID
				, @PostingMonat as PostingMonat
				, KontoNummer
				, case when Mandant = 'Konsolidierung Schweiz' then core.Get_dim_LevelID('Kapitalkonsolidierung Schweiz')
						else core.Get_dim_LevelID('Kapitalkonsolidierung') end as LevelID
				, ProfitCenterID
				, getdate() as MS_AUDIT_TIME
				, SYSTEM_USER as MS_AUDIT_USER
				, core.Get_dim_KonsolidierungsID('Kapitalkonsolidierung') as KonsolidierungsID
				, 'EUR' as Currency
			from meta.Kapitalkonsolidierung k
			Inner Join core.dim_Mandant c on c.MandantNAV = k.Mandant
			left outer join core.[dim_Dimension Value] kst
				 on k.Mandant = kst.MandantenName
				and k.Kostenstelle = kst.Code
			where @PostingMonat between k.von_Monat and k.bis_Monat

			set @Datum        = DATEADD(mm,+1,@Datum)
			set @Stichtag     = CONVERT(varchar(8),(@Datum - day(@Datum)),112)
            set @PostingMonat = @Stichtag/100

		end
		
		declare @rowcount int
        select @rowcount = @@ROWCOUNT
		
	END TRY
	
	BEGIN CATCH
	END CATCH
	

END
