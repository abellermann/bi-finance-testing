﻿CREATE proc [core].[sp_fact_Bilanz_Zusatzbuchungen]
	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- Variablen deklarieren
		declare @StartDateTime	datetime	= GETDATE()
		declare @error			varchar(max)

		delete from core.fact_Bilanz_Zusatzbuchung
		where LevelID = core.Get_dim_LevelID('Basic')
		
		create table #Zusatzbuchungen_temp(
			Kontonummer varchar(20)
			, Amount decimal(38,20)
			, Amount_EUR decimal(38,20)
			, MandantenID int
			, LevelID int
			, ID_Kontenplan int
			, Monat int
			, ID_Nachbuchungen varchar(20)
			, OneOffID int
			, Currency varchar(5)
			)
				
		if (select Wert from meta.Eingaben where Beschreibung = 'Buchungen_filtern') = 1
		begin
		
			declare @Year int = DATEPART(yy,getdate())
			declare @CurrentMonth int = DATEPART(mm,getdate())
			
			declare @Monat int =  (@Year)*100 + @CurrentMonth
			declare @PYEnd int = (@Year-1)*100 + 12
			
			truncate table #Zusatzbuchungen_temp 
			
			--zunächst Werte für das ganze Jahr bestimmen
			insert into #Zusatzbuchungen_temp
			select 
				Kontonummer
				, Amount
				, Amount_EUR
				, MandantenID
				, LevelID
				, ID_KP_Bridge
				, Monat
				, ID_Nachbuchungen
				, OneOffID
				, Currency
			from core.fact_Bilanz
			where KontoNummer in ('d86000','d86000H','d86000I')
			  and LevelID = core.Get_dim_LevelID('Basic')
			  and Monat = @PYEnd --201512
		   
			--Dann in jeden Monat des aktuellen Jahres bis zum aktuellen Monat eintragen
			while @Monat>(@year)*100
			begin
				insert into core.fact_Bilanz_Zusatzbuchung (
					Kontonummer
					, Monat
					, Amount
					, LOAD_DATE
					, MandantenID
					, ID_KP_Bridge
					, LevelID
					, ID_Nachbuchungen
					, OneOffID
					, Amount_EUR
					, Currency)
				select case when KontoNummer = 'd86000' then '086000' 
							when KontoNummer = 'd86000H' then '086000H' 
							when KontoNummer = 'd86000I' then '086000I'
							end 
						as Kontonummer
					, @Monat as Monat
					, Amount
					, getdate() as Load_Date
					, MandantenID
					, ID_Kontenplan
					, LevelID
					, ID_Nachbuchungen
					, OneOffID
					, Amount_EUR
					, Currency
				from #Zusatzbuchungen_temp
				
				set @Monat = @Monat -1
			end
			
		end	 
		
		-- Kontenplan ID neu setzen
		update b
			set b.ID_KP_Bridge = kpnew.ID_KP_Bridge
		--select b.*,kpold.KontoID_Level,kpold.ProfitCenter,kpnew.*
		from core.fact_Bilanz_Zusatzbuchung b
		inner join core.dim_Kontenplan_Bridge kpold 
			 on kpold.ID_KP_Bridge = b.ID_KP_Bridge	
			and kpold.Monat_gueltig = 999999				
		inner join core.dim_Kontenplan_Bridge kpnew 
			 on b.KontoNummer = kpnew.KontoNummer 
			and kpnew.KontoID_Level = kpold.KontoID_Level
			and kpnew.Monat_gueltig = 999999
		
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END