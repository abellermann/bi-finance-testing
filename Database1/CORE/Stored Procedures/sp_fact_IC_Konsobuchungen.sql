﻿CREATE PROCEDURE [core].[sp_fact_IC_Konsobuchungen]
--RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		-- Variablen Deklarieren
		Declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
        
        -- IC Konsobuchungen (um Einträge zu vermeiden, falls die zugehörigen Buchungen gelöscht werden)
        delete
        from core.fact_Zusatzbuchungen
        where LevelID = core.Get_dim_LevelID('Intercompany-Konsolidierung')

        -- IC Konsobuchungen mit *-1 in die Writeback Tabelle auf den Konsolidierungsmandanten schreiben
        insert into core.fact_Zusatzbuchungen(
			Amount
			, Amount_EUR
			, EntryNo
			, MandantEntryNo
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, OneOffID
			, CapexItID
			, ID_Nachbuchungen
			, ProfitCenterID
			, EingabeDatumId
			, KonsolidierungsID
			, Currency)
        select a.Amount *-1 as [Amount] 
			 , Amount_EUR *-1 as Amount_EUR
             , a.EntryNo
             , a.MandantEntryNo
             , [core].[Get_dim_Mandanten_ID]('Konzern') as [MandantenID]
             , a.KostenstellenID
             , a.PostingDateID/100 as [PostingMonat]      
             , a.ID_KP_Bridge
             , core.Get_dim_LevelID('Intercompany-Konsolidierung') as [LevelID]
             , GETDATE() as [MS_AUDIT_TIME]
             , SYSTEM_USER as [MS_AUDIT_USER]
             , KontoNummer
             , OneOffID
             , CapexItID
             , ID_Nachbuchungen
             , ProfitCenterID
			 , EingabeDatumId
			 , core.Get_dim_KonsolidierungsID('IC-Konsolidierung') as KonsolidierungsID
			 , Currency
        from core.fact_Buchungen a
			inner join core.dim_Mandant ma on a.MandantenID = ma.ID
        where a.KontoNummer in (select KontoNummer from meta.IC_Konsokonten)
          and a.Ultimobuchung = 'N'
          and ma.MandantNAV not in ('Konzern', 'Swiss Online Shopping', 'Konsolidierung Schweiz')

		declare @rowcount int
        select @rowcount = @@ROWCOUNT

	END TRY
	
	BEGIN CATCH
	END CATCH
	
	-- LOG ERFOLG
END