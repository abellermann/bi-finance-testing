﻿CREATE PROCEDURE [core].[sp_matview_Kontenplan]
	@RUNID int = null
AS
BEGIN
	BEGIN TRY
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)

		IF EXISTS (Select * 
				   from INFORMATION_SCHEMA.TABLES 
				   where TABLE_TYPE='BASE TABLE' 
					   and TABLE_NAME='dim_Kontenplan_Bridge_Buchungen'
					   and TABLE_SCHEMA = 'core') 
		drop table core_fi.dim_Kontenplan_Bridge_Buchungen;
		
		select distinct ID_KP_Bridge 
		into #temp_buchungen
		from core.vfact_Buchungen
	
		select b.ID_KP_Bridge
			 , ID_Kontenplan
		into core.dim_Kontenplan_Bridge_Buchungen
		from core.dim_Kontenplan_Bridge b
		inner join #temp_buchungen dist on dist.ID_KP_Bridge = b.ID_KP_Bridge
		
		;
		
		IF EXISTS (Select * 
				   from INFORMATION_SCHEMA.TABLES 
				   where TABLE_TYPE='BASE TABLE' 
					   and TABLE_NAME='dim_Kontenplan_Bridge_Bilanz'
					   and TABLE_SCHEMA = 'core_fi') 
		drop table core_fi.dim_Kontenplan_Bridge_Bilanz;
		
		select distinct ID_KP_Bridge 
		into #temp_bilanz
		from core.vfact_Bilanz

		select b.ID_KP_Bridge
			 , ID_Kontenplan
		into core.dim_Kontenplan_Bridge_Bilanz
		from core.dim_Kontenplan_Bridge b
		inner join #temp_bilanz dist on dist.ID_KP_Bridge = b.ID_KP_Bridge
		;
		
		IF EXISTS (Select * 
				   from INFORMATION_SCHEMA.TABLES 
				   where TABLE_TYPE='BASE TABLE' 
					   and TABLE_NAME='dim_Kontenplan_Reduced'
					   and TABLE_SCHEMA = 'core_fi') 
		drop table core_fi.dim_Kontenplan_Reduced;
		
		select *
		into #temp_beide
		from(		
			select ID_KP_Bridge, ID_Kontenplan from core.dim_Kontenplan_Bridge_Bilanz
				union
			 select ID_KP_Bridge, ID_Kontenplan from core.dim_Kontenplan_Bridge_Buchungen) v
			 
		 select kp.*
		 into core.dim_Kontenplan_Reduced
		 from core.dim_Kontenplan kp
		 inner join #temp_beide dist on dist.ID_Kontenplan = kp.ID_Kontenplan 
				
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END