﻿Create PROCEDURE [core].[sp_fact_calc_Amount_EUR]
--	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
	-- Variablen Deklarieren
		Declare @Result           int = 0
		Declare @StartDateTime    datetime = GETDATE()
		declare @error			  varchar(max)
	
		update zi
			set zi.Amount_EUR = zi.Amount/ISNULL(er.ExchangeRate,1.0000)
		from core.Zusatzbuchungen_intermediate zi
		left outer join stg.ExchangeRate er 
			on er.Currency = zi.currency
			 and er.GuV_Bilanz = 'GUV'
			 and er.Month = zi.PostingMonat
		where Amount_EUR is null --ermöglicht das setzen mit Abweichendem Wechselkurs für Anpassungen
                                                                                	
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END