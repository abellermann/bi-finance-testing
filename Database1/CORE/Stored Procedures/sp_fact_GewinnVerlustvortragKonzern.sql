﻿
CREATE PROCEDURE [CORE].[sp_fact_GewinnVerlustvortragKonzern]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
/*	
	drop table #Amount_1
	drop table #Amount_2
	drop table #TotalAmount
	drop table #TotalAmount_proKontenplan
*/		
		-- Variablen Deklarieren
		Declare @StartDateTime  datetime = GETDATE()
		declare @error			varchar(max)
        Declare @KostenstelleID int
        Declare @Konzern        int = [core].[Get_dim_Mandanten_ID]('Konzern')
        
        -- KostenstelleID für den Konzern Code -1
        select @KostenstelleID = ID from [core].[vdim_Kostenstelle] where MandantenName = 'Konzern' and Code = -1
        
		-- Gewinn Verlustvortrag ermitteln--------------------------------------------------------------------------------------
		--aus Buchungen
		select isnull(sum(a.Amount),0) *-1 as sumAmount
				, isnull(sum(a.Amount_EUR),0) *-1 as sumAmount_EUR
				, y.KontoID_Level
				, y.HGB_IFRS
				, a.PostingDateID/10000 as PostingYear
				, a.ID_Nachbuchungen
				, a.MandantenID
				, 1 as LevelID
				, 0 as KonsolidierungsID
				, a.Currency
		into #Amount_1 
		from core.fact_Buchungen a
		inner join core.dim_Kontenplan_Bridge y on a.ID_KP_Bridge = y.ID_KP_Bridge and Monat_gueltig = 999999
          where y.GuV_Bilanz = 'GuV'
			  and MandantenID = @Konzern
			  and a.Ultimobuchung = 'N'
          group by y.KontoID_Level, HGB_IFRS, a.PostingDateID/10000, ID_Nachbuchungen, MandantenID, Currency

		--aus Zusatzbuchungen
        select  isnull(sum(a.Amount),0) *-1 as sumAmount
				, isnull(sum(a.Amount_EUR),0) *-1 as sumAmount_EUR
				, y.KontoId_Level
				, y.HGB_IFRS
				, a.PostingMonat/100 as PostingYear
				, a.ID_Nachbuchungen
				, a.MandantenID
				, a.LevelID
				, a.KonsolidierungsID
				, a.Currency
        into #Amount_2             
        from core.fact_Zusatzbuchungen a
        inner join core.dim_Kontenplan_Bridge y on a.ID_KP_Bridge = y.ID_KP_Bridge and y.Monat_gueltig = 999999
		where  y.GuV_Bilanz = 'GuV'
			and y.KontoNummer not in ('d86003', 'd86003H', 'd86003I', 'd999001', 'd999001H', 'd999001I')
			and a.MandantenID = @Konzern
			and a.LevelID <> core.Get_dim_LevelID('Planung')
		group by y.KontoID_Level, y.HGB_IFRS, a.PostingMonat/100, a.MandantenID, ID_Nachbuchungen, a.LevelID, a.KonsolidierungsID, Currency
          
        Select 
			sum(v.sumAmount) as totalAmount
			, sum(v.sumAmount_EUR) as totalAmount_EUR
			, v.KontoID_Level
			, v.HGB_IFRS
			, v.PostingYear
			, v.MandantenID
			, isnull(v.ID_Nachbuchungen,0) as ID_Nachbuchungen
			, v.LevelID
			, isnull(v.KonsolidierungsID,0) as KonsolidierungsID
			, v.Currency
		into #TotalAmount_ProKontenplan
		from (Select * from #Amount_1 a
				union all 
				Select * from #Amount_2 b) v
		group by v.KontoID_Level, v.HGB_IFRS, v.Postingyear, v.MandantenID, isnull(ID_Nachbuchungen,0),v.LevelID, isnull(v.KonsolidierungsID,0), Currency


			--Überprüfen ob für alle GuV-Kontenpläne die gleiche Summe heraus kommt (jeweils für HGB und IFRS). Wenn nicht, eine Email senden.
/*		if exists(  select COUNT(*) as Anzahl,PostingYear,MandantenID,HGB_IFRS,ID_Nachbuchungen,LevelID, KonsolidierungsID, currency from (
			 select distinct totalAmount,HGB_IFRS,PostingYear, MandantenID,ID_Nachbuchungen, LevelID, KonsolidierungsID, currency from #TotalAmount_ProKontenplan --order by PostingYear desc
			  ) v
			group by PostingYear,HGB_IFRS,ID_Nachbuchungen, MandantenID, LevelID, KonsolidierungsID, currency
			having COUNT(*)>1)
		begin
			select DB_NAME() as Name into ##DB_Name
		
			declare @recipientList varchar(max)
			set @recipientList = dbo.get_Emailverteiler('Fibu_Tech')
		
			EXEC msdb.dbo.sp_send_dbmail
			@recipients = @recipientList,
			@subject = '!Der GewinnVerlustVortrag (Konzern) ist nicht für alle GuV Kontenpläne identisch!',
			@body = 'Warnung: Der GewinnVerlustVortrag (Konzern) ist nicht für alle GuV Kontenpläne identisch',
			@query = 'select Name from ##DB_Name';
			
			drop table ##DB_Name
		end
*/		
		--einen zufälligen Kontenplan auswählen, (für den Fall dass nicht alle den Gleichen Wert haben)
		select max(totalAmount) as totalAmount
			, MAX(totalAmount_EUR) as totalAmount_EUR 
			, HGB_IFRS
			, PostingYear
			, MandantenID
			, ID_Nachbuchungen
			, LevelID
			, KonsolidierungsID
			, Currency
		into #TotalAmount
		from #TotalAmount_ProKontenplan
		group by HGB_IFRS,PostingYear, ID_Nachbuchungen, MandantenID, LevelID, KonsolidierungsID, currency
		

        -- Gewinn Verlustvortrag Aktuell vom Konsolidierungsmandanten auf das Konto 086000/H/I schreiben. Nur einmalig pro Jahr auf Januar!
		insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, ProfitCenterID
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency
			)
		select totalAmount
			, totalAmount_EUR
			, MandantenID
			, @KostenstelleID
			, convert(varchar,a.PostingYear+1) + '01' as PostingMonat
			, d.ID_KP_Bridge
			, core.Get_dim_LevelID('GewinnVerlustvortrag')
			, getdate()
			, system_user
			, d.KontoNummer
			, -1 as ProfitCenterID
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency
		from #TotalAmount a
		inner join core.dim_Kontenplan_Bridge d
			 on d.HGB_IFRS = a.HGB_IFRS 
			and d.GuV_Bilanz = 'Bilanz' 
			and Monat_gueltig = 999999
		where a.HGB_IFRS = 'HGB'
			and d.KontoNummer = '086000H'  
			and a.PostingYear >= 2014
			and a.PostingYear <= convert(varchar,year(getdate())) --keine zukünftigen Buchungen anzeigen
				
				
		insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, ProfitCenterID
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency)
		select totalAmount
			, totalAmount_EUR
			, MandantenID
			, @KostenstelleID
			, convert(varchar,a.PostingYear+1) + '01' as PostingMonat
			, d.ID_KP_Bridge
			, core.Get_dim_LevelID('GewinnVerlustvortrag')
			, getdate()
			, system_user
			, d.KontoNummer
			, -1 as ProfitCenterID
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency
		from #TotalAmount a
		inner join core.dim_Kontenplan_Bridge d
			 on d.HGB_IFRS = a.HGB_IFRS 
			and d.GuV_Bilanz = 'Bilanz'
			and Monat_gueltig = 999999
		where a.HGB_IFRS = 'IFRS'
			and d.KontoNummer = '086000I'  
			and a.PostingYear >= 2014
			and a.PostingYear <= convert(varchar,year(getdate())) --keine zukünftigen Buchungen anzeigen
				
				
		--Keine Unterteilung in HGB/IFRS vor 2014
		insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, LevelID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, ProfitCenterID
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency)
		select totalAmount
			, totalAmount_EUR
			, MandantenID
			, @KostenstelleID
			, convert(varchar,a.PostingYear+1) + '01' as PostingMonat
			, d.ID_KP_Bridge
			, core.Get_dim_LevelID('GewinnVerlustvortrag')
			, getdate()
			, system_user
			, d.KontoNummer
			, -1 as ProfitCenterID
			, ID_Nachbuchungen
			, KonsolidierungsID
			, Currency
		from #TotalAmount a
		inner join core.dim_Kontenplan_Bridge d
			 on d.HGB_IFRS = a.HGB_IFRS 
			and d.GuV_Bilanz = 'Bilanz'
			and Monat_gueltig = 999999
		where d.KontoNummer = '086000'  
			and a.PostingYear < 2014
			and a.PostingYear >= 2012

	END TRY

	BEGIN CATCH
	END CATCH
	
END
