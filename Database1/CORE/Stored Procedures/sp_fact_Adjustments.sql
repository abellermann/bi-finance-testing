﻿
CREATE proc [CORE].[sp_fact_Adjustments]
--	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- Variablen deklarieren
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		-- Null-Zeilen löschen
		delete from meta.Adjustments where Monat is null
		
		
		insert into core.Zusatzbuchungen_intermediate (
			Amount
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, LevelID
			, OneOffID
			, AdjustmentID
			, ProfitCenterID
			)
		Select ad.Wert
				, c.id as MandantID
				, isnull(kst.ID, 591) as KostenstellenID
				, ad.Monat
				, getdate() as MS_AUDIT_TIME
				, SYSTEM_USER as MS_AUDIT_USER
				, ad.von_Konto
				, core.Get_dim_LevelID('Adjustments') as LevelID
				, case when ad.Beschreibung like 'OO-%' then oo.OneOffID end as OneOffID
				, isnull(dim.AdjustmentID,1) as AdjustmentID
				, isnull(kst.ProfitCenterID, -1) as ProfitCenterID
			from meta.Adjustments ad
			INNER JOIN CORE.dim_Mandant c on c.MandantNAV = ad.Mandant
				left outer join core.dim_OneOffs oo
					 on SUBSTRING(ad.Beschreibung, 4, 1) = oo.OneOffCode
				left outer join core.[dim_Dimension Value] kst
					 on ad.Mandant = kst.MandantenName
					and ad.von_Kostenstelle = kst.Code	
				left outer join core.dim_Adjustments dim
					on dim.AdjustmentDescription = ad.Typ				
				where ad.von_Konto is not null
			
			union all
			
			Select (-1) * ad.Wert
				, c.id as MandantID
				, isnull(kst.ID, 591) as KostenstellenID
				, ad.Monat
				, getdate() as MS_AUDIT_TIME
				, SYSTEM_USER as MS_AUDIT_USER
				, ad.auf_Konto
				, core.Get_dim_LevelID('Adjustments') as LevelID
				, case when ad.Beschreibung like 'OO-%' then oo.OneOffID end as OneOffID
				, isnull(dim.AdjustmentID,1) as AdjustmentID
				, isnull(kst.ProfitCenterID, -1) as ProfitCenterID
			from meta.Adjustments ad
			Inner Join CORE.dim_Mandant c on c.MandantNAV = ad.Mandant
				left outer join core.dim_OneOffs oo
					 on SUBSTRING(ad.Beschreibung, 4, 1) = oo.OneOffCode
				left outer join core.[dim_Dimension Value] kst
					 on ad.Mandant = kst.MandantenName
					and ad.auf_Kostenstelle = kst.Code
				left outer join core.dim_Adjustments dim
					on dim.AdjustmentDescription = ad.Typ						
				where ad.auf_Konto is not null
				
		
	END TRY
	
	BEGIN CATCH
	END CATCH

END
