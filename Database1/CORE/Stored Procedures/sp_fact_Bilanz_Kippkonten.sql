﻿CREATE PROCEDURE [core].[sp_fact_Bilanz_Kippkonten] 
--	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		Declare @StartDateTime		datetime = getdate();
		declare @error				varchar(max)
		
		--KippAmaount berechnen
		select ID_Bilanz,
				SUM(bi.Amount_EUR) OVER(PARTITION BY Kontonummer, MandantenID, ID_KP_Bridge, Monat, ID_Nachbuchungen) as Kippamount
		into #Kippamount
		from core.fact_Bilanz bi
		
		update bi
			set bi.KippAmount = ka.Kippamount
		from core.fact_Bilanz bi
		 inner join #Kippamount ka
		   on ka.ID_Bilanz = bi.ID_Bilanz

		-- Kippkonten mit Kontenplaninformationen anreichern
		select kk.KontoAlt
			, kk.KontoNeu
			, kpalt.ID_KP_Bridge as ID_KP_Alt
			, kpneu.ID_KP_Bridge as ID_KP_Neu
			, kk.Bilanz 
			, kk.kippen_bis
		into #Kippkonten
		from meta.Bilanz_Kippkonten kk
		inner join core.dim_Kontenplan_Bridge kpalt
			 on kk.KontoAlt = kpalt.KontoNummer
			and kpalt.Monat_gueltig = 999999
		inner join core.dim_Kontenplan_Bridge kpneu
			 on kpneu.KontoNummer = kk.KontoNeu
			and kpalt.KontoID_Level = kpneu.KontoID_Level
			and kpneu.Monat_gueltig = 999999


		select KontoNummer
		into #IC_Konten
		from
			(select KontoNummer
			from meta.IC_Konsokonten
			union
			select KontoNummer
			from meta.IC_Konsokonten_Schweiz
			union
			select KontoNummer
			from meta.BS_Verrechnungskonten) a
				
		-- Konsolidierungsmandanten (Aufteilung in IC-Konsokonten und normale Konten)

			--Nach AKTIVA Kippen-----------------------------------------------------------------------------------------------
		
			Update a
			set a.Kontonummer = kk.KontoNeu
			, a.ID_KP_Bridge = kk.ID_KP_Neu
			--Select *
			from core.fact_Bilanz a
			inner join #Kippkonten kk on a.ID_KP_Bridge = kk.ID_KP_Alt and kk.Bilanz = 'Aktiva' and a.Monat <= isnull(kk.kippen_bis, 999999)
            where a.KippAmount > 0
              and a.KontoNummer not in (select y.KontoNummer from #IC_Konten y)
              and a.MandantenID in (core.Get_dim_Mandanten_ID('Konzern'), core.Get_dim_Mandanten_ID('Konsolidierung Schweiz'))
              
            -- IC-Konten:
            -- Bei IC_Konsokonten auf dem Konzernmandanten ist das Vorzeichen der Buchungen immer genau anders herum als bei den anderen Mandanten. Daher muss auch bei den Kippkonten das Vorzeichen anders behandelt werden
            Update a
			set a.Kontonummer = kk.KontoNeu
			, a.ID_KP_Bridge = kk.ID_KP_Neu
			--Select*
            from core.fact_Bilanz a
			inner join #Kippkonten kk on a.ID_KP_Bridge = kk.ID_KP_Alt and kk.Bilanz = 'Aktiva' and a.Monat <= isnull(kk.kippen_bis, 999999)
            where a.KippAmount < 0  --Hier werden negative Summen gewählt, da das Vorzeichen anders sein muss als bei normalen Konten
              and a.KontoNummer in (select y.KontoNummer from #IC_Konten y)
			  and a.MandantenID in (core.Get_dim_Mandanten_ID('Konzern'), core.Get_dim_Mandanten_ID('Konsolidierung Schweiz'))


			--Nach Passiva Kippen------------------------------------------------------------------------------------------------
			
			Update a
			set a.Kontonummer = kk.KontoNeu
			, a.ID_KP_Bridge = kk.ID_KP_Neu
			--Select *
			from core.fact_Bilanz a
            inner join #Kippkonten kk on a.ID_KP_Bridge = kk.ID_KP_Alt and kk.Bilanz = 'Passiva' and a.Monat <= isnull(kk.kippen_bis, 999999)
            where a.KippAmount < 0
              and a.KontoNummer not in (select y.KontoNummer from #IC_Konten y)
			  and a.MandantenID in (core.Get_dim_Mandanten_ID('Konzern'), core.Get_dim_Mandanten_ID('Konsolidierung Schweiz'))
			                    
            -- Bei IC_Konsokonten auf dem Konzernmandanten ist das Vorzeichen der Buchungen immer genau anders herum als bei den anderen Mandanten. Daher muss auch bei den Kippkonten das Vorzeichen anders behandelt werden
            Update a
			set a.Kontonummer = kk.KontoNeu
			, a.ID_KP_Bridge = kk.ID_KP_Neu
			--Select *
            from core.fact_Bilanz a
            inner join #Kippkonten kk on a.ID_KP_Bridge = kk.ID_KP_Alt and kk.Bilanz = 'Passiva' and a.Monat <= isnull(kk.kippen_bis, 999999)
            where a.KippAmount > 0  --Hier werden positive Summen gewählt, da das Vorzeichen anders sein muss als bei normalen Konten
              and a.KontoNummer in (select y.KontoNummer from #IC_Konten y)
			  and a.MandantenID in (core.Get_dim_Mandanten_ID('Konzern'), core.Get_dim_Mandanten_ID('Konsolidierung Schweiz'))

		--Andere Mandanten: Keine Aufteilung
		
			--Nach AKTIVA Kippen-----------------------------------------------------------------------------------------------
				
			Update a
			set a.Kontonummer = kk.KontoNeu
			, a.ID_KP_Bridge = kk.ID_KP_Neu
			--Select *
            from core.fact_Bilanz a
            inner join #Kippkonten kk on a.ID_KP_Bridge = kk.ID_KP_Alt and kk.Bilanz = 'Aktiva' and a.Monat <= isnull(kk.kippen_bis, 999999)
            where a.KippAmount > 0
              and a.MandantenID not in (core.Get_dim_Mandanten_ID('Konzern'), core.Get_dim_Mandanten_ID('Konsolidierung Schweiz'))
                 
                  
			--Nach Passiva Kippen------------------------------------------------------------------------------------------------
            Update a
			set a.Kontonummer = kk.KontoNeu
			, a.ID_KP_Bridge = kk.ID_KP_Neu
			--Select *
            from core.fact_Bilanz a
            inner join #Kippkonten kk on a.ID_KP_Bridge = kk.ID_KP_Alt and kk.Bilanz = 'Passiva' and a.Monat <= isnull(kk.kippen_bis, 999999)
            where a.KippAmount < 0
			  and a.MandantenID not in (core.Get_dim_Mandanten_ID('Konzern'), core.Get_dim_Mandanten_ID('Konsolidierung Schweiz'))
			
	END TRY
	
	BEGIN CATCH
	END CATCH
END
