﻿CREATE PROCEDURE [core].[sp_fact_WaWi_Zusatzbuchungen]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		-- Variablen Deklarieren
		declare @Datum datetime = getdate()
		Declare @StartDateTime datetime = getdate()
		declare @error varchar(max)
		
        -- Umsatz und Wareneinsatz eintragen
		insert into core.Zusatzbuchungen_intermediate 
			(Amount
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, KontoNummer
			, LevelID
			, ProfitCenterID
			, MS_AUDIT_TIME
			, MS_AUDIT_USER)
		-- Umsatz
		select SUM(VK_Wert) as Amount
			, core.Get_dim_Mandanten_ID(Mandant) as MandantenID
			, kst.ID as KostenstellenID
			, ID_Monat as PostingMonat
			, 'z999999' as KontoNummer
			, core.Get_dim_LevelID('WaWi-Daten') as LevelID
			, core.Get_dim_ProfitCenterID(ProfitCenter_kurz) as ProfitCenterID
			, GETDATE() as MS_AUDIT_TIME
			, system_user as MS_AUDIT_USER
		from STG.WaWi_Summen s
		left outer join core.[dim_Dimension Value] kst
			 on s.Mandant = kst.MandantenName
			and s.Kostenstelle = kst.Code
		group by Mandant, kst.ID, ID_Monat, ProfitCenter_kurz
		union all
		-- Wareneinsatz
		select - SUM(EK_Wert) as Amount
			, core.Get_dim_Mandanten_ID(Mandant) as MandantenID
			, kst.ID as KostenstellenID
			, ID_Monat as PostingMonat
			, 'z999998' as KontoNummer
			, core.Get_dim_LevelID('WaWi-Daten') as LevelID
			, core.Get_dim_ProfitCenterID(ProfitCenter_kurz) as ProfitCenterID
			, GETDATE() as MS_AUDIT_TIME
			, system_user as MS_AUDIT_USER
		from STG.WaWi_Summen s
		left outer join core.[dim_Dimension Value] kst
			 on s.Mandant = kst.MandantenName
			and s.Kostenstelle = kst.Code
		group by Mandant, kst.ID, ID_Monat, ProfitCenter_kurz
		
	END TRY

	BEGIN CATCH
	END CATCH
	
END