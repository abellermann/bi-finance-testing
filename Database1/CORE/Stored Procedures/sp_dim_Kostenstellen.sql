﻿
CREATE proc [CORE].[sp_dim_Kostenstellen]
--	@RUNID int = null
as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- ------------------------------------------------
		-- Mandanten ermitteln, für die geladen werden soll
		-- ------------------------------------------------
		Declare @mandanten table ([ID] int null, [Name] varchar(255) null, [G_L_Account_PRIO] int null)
		Declare @sql varchar(max)
		set @sql ='select ID, MandantNAV, [G_L Account PRIO] from CORE.dim_Mandant where  NAVISION = 1 order by ISNULL([G_L Account PRIO],99)  '
		Insert @mandanten exec (@sql)
		--Select * from @mandanten
		declare @StartDateTime datetime = getdate()
		declare @error varchar(max)
		declare @MandantenName varchar(50)
		declare @MandantenID int
		declare @FileSource varchar(100) = 'core.sp_dim_Kostenstellen'
	
		-- Kostenstellen für die Schweiz aus dem Kontenplanmapping anlegen (solange diese nicht in Navision integriert sind)	
	--	exec core_fi.sp_dim_Kostenstelle_Schweiz @runid = @runid

	
	DECLARE crs_master_core CURSOR FOR  

	SELECT [ID],[Name] from @mandanten

	OPEN crs_master_core  
	 
	FETCH NEXT FROM crs_master_core INTO @MandantenID , @MandantenName   

	WHILE @@FETCH_STATUS = 0 BEGIN		
		
        if not exists(select * from core.[dim_Dimension Value] where [Dimension Code] = 'KOSTENSTELLE' and [Code] = -1 and [MandantenName] = @MandantenName) 
        begin
		    insert into core.[dim_Dimension Value] ([Dimension Code]
													, Code
													, Name
													, LOAD_DATE
													, MandantenName
													, MandantID )
		    select 'KOSTENSTELLE' as [Dimension Code], 
		           -1             as [Code], 
		           'Unbekannt'    as [Name], 
		           getdate()      as [LOAD_DATE], 						
			       @MandantenName as [MandantenName],
				   @MandantenID as [MandantID]
        end

        merge core.[dim_Dimension Value] as t 
        using (select [Dimension Code], 
		              [Code], 
		              [Name], 
		              [LOAD_DATE] = getdate(), 						
			          [MandantID]
		       from [stg].[Dimension Value]               
               where [Dimension Code] = 'KOSTENSTELLE'                
		         and [MandantID]  = @MandantenID) as s
        on (t.MandantID = s.MandantID and t.Code = s.Code)
        when Matched then
         update set [Dimension Code]                 = s.[Dimension Code], 
                    [Name]                           = s.[Name], 
                    [LOAD_DATE]                      = getdate()
        when not Matched then 
         INSERT ([Code],
                 [Dimension Code],                
                 [Name],                          
                 [LOAD_DATE],
                 [MandantenName],
				 MandantID)
         values (s.Code,
                 s.[Dimension Code],          
                 s.[Name], 
                 getdate(),
                 @MandantenName,
				 s.MandantID);		
                 
                 
		-- ID_ProfitCenter in core_fi.dim_Dimension_Value aus Meta-DB Kostenstellen setzen
		-- null setzen, weil sich die Zuordnung ändern kann
		update d
			set d.ProfitCenterID = null
		from core.[dim_Dimension Value] d
		
		-- Kostenstelle '' anlegen, wenn noch nicht vorhanden, mit ProfitCenter = offline
		if not exists(select * from core.[dim_Dimension Value] 
						where [Dimension Code] = 'KOSTENSTELLE'
						  and [Code] = ''
						  and [MandantID] = @MandantenID) 
        begin
		    insert into core.[dim_Dimension Value] ([Dimension Code]
													, Code
													, Name
													, LOAD_DATE
													, MandantenName
													, MandantID
													, ProfitCenterID)
		    select 'KOSTENSTELLE' as [Dimension Code], 
		           ''             as [Code], 
		           'Unbekannt'    as [Name], 
		           getdate()      as [LOAD_DATE], 						
			       @MandantenName as [MandantenName],
				   @MandantenID	  as MandantID,
			       core.Get_dim_ProfitCenterID('offline') as ProfitCenterID
        end



		-- wenn Mandant existiert
		update d
			set d.ProfitCenterID = pcc.ID
		--select *
		from core.[dim_Dimension Value] d
		inner join meta.Kostenstellen k
			 on d.Code = k.Kostenstelle
			and d.MandantenName = k.Mandant
		inner join core.dim_ProfitCenter pcc
			 on k.Profit_Center = pcc.ProfitCenter_kurz
		where k.Mandant is not null
		
		-- für alle anderen den Default der Kostenstelle
		update d
			set d.ProfitCenterID = pcc.ID
		--select *
		from core.[dim_Dimension Value] d
		inner join meta.Kostenstellen k
			 on d.Code = k.Kostenstelle
		inner join core.dim_ProfitCenter pcc
			 on k.Profit_Center = pcc.ProfitCenter_kurz
		where k.Mandant is null
		  and d.ProfitCenterID is null
		  
		-- für nicht-achtstellige Kostenstellen KST_12, KST_34 und KST_56 mit Dummywert befüllen
		update d
			set d.KST_12 = 'Kostenstellen bis 2014'
			, d.KST_34 = 'Kostenstellen bis 2014'
			, d.KST_56 = 'Kostenstellen bis 2014'
		-- select *
		from core.[dim_Dimension Value] d
		where len(Code) <> 8
		  and d.KST_12 is null
		  
		-- für achtstellige Kostenstellen KST_12, KST_34 und KST_56 sinnvoll setzen und soweit möglich mit Info aus Meta-DB anreichern
		update d
			set d.KST_12 = LEFT(Code, 2) + ' - ' + isnull(k12.KST_12_Bezeichnung, '')
			, d.KST_34 = SUBSTRING(d.Code, 3, 2) + ' - ' + isnull(k34.KST_34_Bezeichnung, '')
			, d.KST_56 = SUBSTRING(d.Code, 5, 2) + ' - ' + isnull(k56.KST_56_Bezeichnung, '')
		from core.[dim_Dimension Value] d
		left outer join (select KST_12, KST_12_Bezeichnung collate database_default as KST_12_Bezeichnung from meta.Kostenstelle_Gesellschaft) k12
			 on LEFT(d.Code, 2) = k12.KST_12 collate database_default
		left outer join (select KST_34, KST_34_Bezeichnung collate database_default as KST_34_Bezeichnung from meta.Kostenstelle_Geschaeftsfunktionstyp) k34
			 on SUBSTRING(d.Code, 3, 2) = k34.KST_34 collate database_default
		left outer join (select KST_56, KST_56_Bezeichnung collate database_default as KST_56_Bezeichnung from meta.Kostenstelle_Grobfunktionsbereich) k56
			 on SUBSTRING(d.Code, 5, 2) = k56.KST_56 collate database_default
		where len(Code) = 8
		
	FETCH NEXT FROM crs_master_core INTO @MandantenID, @MandantenName
	END   	
	
	CLOSE crs_master_core   
	DEALLOCATE crs_master_core
		
                 
	END TRY
	
	BEGIN CATCH
	END CATCH
	
END
