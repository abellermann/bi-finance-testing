﻿CREATE proc [core].[sp_fact_Adjustments_Ueberleitung]

as
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		-- Variablen deklarieren
		declare @StartDateTime datetime = GETDATE()
		declare @error varchar(max)
		
		-- Null-Zeilen löschen
		delete from meta.Adjustments_Ueberleitung where Jahr is null
		
		-- bisherige Einträge löschen
		delete z
		from core.fact_Zusatzbuchungen z
		inner join core.dim_Adjustments a
			 on z.AdjustmentID = a.AdjustmentID
		where LevelID = core.Get_dim_LevelID('Adjustments')
		  and AdjustmentDescription like 'Überleitung%'
		
		-- umzubuchende Summen bestimmen
		IF OBJECT_ID('tempdb..#GL_Amount') IS NOT NULL
		DROP TABLE #GL_Amount
		
		select gl.MandantenID
			, gl.KostenstellenID
			, left(gl.PostingDateID, 6) as PostingMonat
			, gl.ID_KP_Bridge
			, a.Kontonummer
			, gl.OneOffID
			, gl.ProfitCenterID
			, gl.CapexItID
			, gl.ID_Nachbuchungen
			, a.auf_Kostenstelle
			, core.Get_dim_AdjustmentID_Ueberleitung(a.Jahr, a.Ueberleitung_fuer) as AdjustmentID
			, a.von_Anteil
			, a.auf_Anteil
			, sum(Amount) as Amount
			, SUM(Amount_EUR) as Amount_EUR
			, gl.EingabeDatumId
			, Currency
		into #GL_Amount
		from meta.Adjustments_Ueberleitung a
		inner join (select ID, MandantenName, Code as Kostenstelle 
					from core.[dim_Dimension Value] where [Dimension Code] = 'KOSTENSTELLE'
					union
					select ID, MandantenName, '' as Kostenstelle 
					from core.[dim_Dimension Value] d1
					where [Dimension Code] = 'KOSTENSTELLE'
					  and Code = '-1'
					  and not exists (select * from core.[dim_Dimension Value] d2
										where [Dimension Code] = 'KOSTENSTELLE'
										 and Code = ''
										 and d1.MandantenName = d2.MandantenName)) kst
			 on a.Mandant = kst.MandantenName
			and a.von_Kostenstelle = kst.Kostenstelle
		inner join core.fact_Buchungen gl
			 on core.Get_dim_Mandanten_ID(a.Mandant) = gl.MandantenID
			and a.Kontonummer = gl.KontoNummer
			and kst.ID = gl.KostenstellenID
			and a.Jahr = left(gl.PostingDateID, 4)
		group by gl.MandantenID
			, gl.KostenstellenID
			, left(gl.PostingDateID, 6)
			, gl.ID_KP_Bridge
			, a.Kontonummer
			, gl.OneOffID
			, gl.ProfitCenterID
			, gl.CapexItID
			, gl.ID_Nachbuchungen
			, gl.EingabeDatumId
			, core.Get_dim_AdjustmentID_Ueberleitung(a.Jahr, a.Ueberleitung_fuer)
			, a.von_Anteil
			, a.auf_Anteil
			, a.auf_Kostenstelle
			, Currency
		having sum(Amount_EUR) <> 0
		
		
		-- von Kostenstelle
		insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, LevelID
			, OneOffID
			, AdjustmentID
			, ProfitCenterID
			, CapexItID
			, ID_Nachbuchungen
			, EingabeDatumId
			, Currency)
		select - Amount * von_Anteil as Amount
			, - Amount_EUR * von_Anteil as Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, getdate() as MS_AUDIT_TIME
			, SYSTEM_USER as MS_AUDIT_USER
			, Kontonummer
			, core.Get_dim_LevelID('Adjustments') as LevelID
			, OneOffID
			, AdjustmentID
			, ProfitCenterID
			, CapexItID
			, ID_Nachbuchungen
			, EingabeDatumId
			, Currency
		from #GL_Amount
		where von_Anteil > 0
		
		-- auf Kostenstelle
		insert into core.fact_Zusatzbuchungen
			(Amount
			, Amount_EUR
			, MandantenID
			, KostenstellenID
			, PostingMonat
			, ID_KP_Bridge
			, MS_AUDIT_TIME
			, MS_AUDIT_USER
			, KontoNummer
			, LevelID
			, OneOffID
			, AdjustmentID
			, ProfitCenterID
			, CapexItID
			, ID_Nachbuchungen
			, EingabeDatumId
			, Currency)
		select gl.Amount * auf_Anteil as Amount
			, gl.Amount_EUR * auf_Anteil as Amount_EUR
			, gl.MandantenID
			, kst.ID as KostenstellenID
			, gl.PostingMonat
			, gl.ID_KP_Bridge
			, getdate() as MS_AUDIT_TIME
			, SYSTEM_USER as MS_AUDIT_USER
			, gl.Kontonummer
			, core.Get_dim_LevelID('Adjustments') as LevelID
			, gl.OneOffID
			, gl.AdjustmentID
			, kst.ProfitCenterID as ProfitCenterID
			, gl.CapexItID
			, gl.ID_Nachbuchungen
			, EingabeDatumId
			, Currency
		from #GL_Amount gl
		inner join (select ID, MandantenName, Code, ProfitCenterID 
					from core.[dim_Dimension Value] where [Dimension Code] = 'KOSTENSTELLE') kst
			 on gl.MandantenID = core.Get_dim_Mandanten_ID(kst.MandantenName)
			and gl.auf_Kostenstelle = kst.Code
			and core.Get_dim_Mandanten_Name(gl.MandantenID) = kst.MandantenName
		
	END TRY
	
	BEGIN CATCH
	END CATCH

END