﻿
CREATE PROCEDURE [CORE].[sp_GLEntry_intermediate]
	@RUNID int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		Declare @StartDateTime	datetime = getdate()
		declare @error			varchar(max)

		-- ---------------
		-- Tabellen leeren
		-- ---------------
        truncate table core.GLEntry_intermediate

					
		-- ------------------------------------------------------
		-- Index core_fi.GLEntry_intermediate - idx_GLEntry_intermediate - löschen
		-- ------------------------------------------------------
/*		IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[core_fi].[GLEntry_intermediate]') AND name = N'idx_GLEntry_intermediate')
			DROP INDEX [idx_GLEntry_intermediate] ON [core_fi].[GLEntry_intermediate] WITH ( ONLINE = OFF )
*/
		-- ---------------------------------------------------
		-- Index stg_fi.G_L_Entry - idx_stg_fakten - erstellen
		-- ---------------------------------------------------
/*		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[stg_fi].[G_L_Entry]') AND name = N'idx_stg_fakten') begin
			CREATE NONCLUSTERED INDEX [idx_stg_fakten] ON [stg].[G_L_Entry] 
			(
				[G_L_AccountNo_] ASC,
				[PostingDate] ASC,
				[MandantenName] ASC
			)
			WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		end
*/		
		-- --------------------------------------------------
		-- Fakten Aktuelle Wahrheit in den Core Bereich laden
		-- --------------------------------------------------
		
		declare @Datum			datetime    = getdate()
		insert into [core].[GLEntry_intermediate]
			(EntryNo
			,MandantEntryNo
			,KontoNummer
			,KontoNummer_Alt
			,PostingDateID
			,BelegartID
			,Amount
			,GegenKontoNummer
			,LOAD_DATE
			,MandantenID
			,Ultimobuchung
			,KostenstellenID
			,LevelID
			,OneOffID
			,ProfitCenterID
			,CapexItID
			,ID_Nachbuchungen
			,Amount_EUR
			,Currency
			,PART_KEY
			,[User ID]
			,[Reason Code]
			,[System-Created Entry]
			,[Additional-Currency Amount]
			,[Amount (FCY)]
			,[Amount (LCY)]
			,CreationDate
			,[Orig_ Currency Code]
			,[Original Amount (FCY)]
			,[Source No_]
			,[Source Type]
			,[Source Code]
			,[Transaction No_]
			,TS_INT)
		select a.[Entry No]                                                                                              as EntryNo_
			 , cast(c.ID as varchar) + '-' + isnull(cast(a.[Entry No] as varchar), '9999999')							 as MandantEntryNo
			 , a.[G_L Account No]																						 as KontoNummer
			 , a.[G_L Account No]                                                                                        as KontoNummer_Alt
			 , convert(int,(convert(varchar(8),a.[Posting Date], 112)))                               						 as PostingDateID
			 , a.[Document Type]                                                                  						 as BelegartID
			 , a.Amount                                             													 as Amount
			 , convert(char(6),LTRIM(RTrim(a.[Bal_ Account No])))                            	 						 as GegenKontoNummer
			 , @Datum                                                                            						 as LOAD_DATE
			 , c.ID                                                                              						 as MandantenID
			 , case when convert(varchar(8),a.[Posting Date],108) = '00:00:00' then 'N' 
					when convert(varchar(8),a.[Posting Date],108) = '23:59:59' then 'J'
					else 'J' end                                                                 						 as Ultimobuchung
			 , ISNULL(e.ID,f.ID)                                                                 						 as KostenstellenID
             , h.LevelID                                                                                                 as LevelID
             , case when a.Description like 'FZK%' then 50 else isnull(i.OneOffID,0) end								 as OneOffID
             , ISNULL(e.ProfitCenterID, case when a.[Posting Date] < '20150101' then f.ProfitCenterID else -1 end) 		 as ProfitCenterID
             , case when a.Description like 'CIT %' then 1 else 0 end													 as CapexItID
             , [Prior-Year Entry]																						 as ID_Nachbuchung
             , a.Amount / ISNULL(er.ExchangeRate,1.0000)	
             , a.Currency																								 as Amount_EUR
			 , a.PART_KEY
			 , a.[User ID]
			 , a.[Reason Code]
			 , a.[System-Created Entry]
			 , a.[Additional-Currency Amount]
			 , a.[Amount (FCY)]
			 , a.[Amount (LCY)]
			 , a.CreationDate
			 , a.[Orig_ Currency Code]
			 , a.[Original Amount (FCY)]
			 , a.[Source No_]
			 , a.[Source Type]
			 , a.[Source Code]
			 , a.[Transaction No_]
			 , a.TS_INT
		from [stg].[G_L Entry] a with(tablock)
		 left join [core].[dim_Mandant] c	with(tablock) 
			on c.ID = a.MandantID	 
		 left join [core].[dim_Dimension Value] e	with(tablock) 
			on e.MandantenName = a.MandantenName  
			and e.Code = a.[Cost Center]    
			and e.[Dimension Code] = 'KOSTENSTELLE'
		 left join [core].[dim_Dimension Value] f	with(tablock) 
			on f.MandantenName = a.MandantenName  
			and f.Code = '-1'
			and f.[Dimension Code] = 'KOSTENSTELLE'
         left join [core].[dim_Level] h	with(tablock) 
			on h.[Level] = 'Basic'
         left join (select MandantenName, [Entry No], isnull(dim.OneOffID, 9) as OneOffID
					from stg.[G_L Entry] stg left outer join core.dim_OneOffs dim
						on SUBSTRING(stg.Description, 4, 1) = dim.OneOffCode
					where [Posting Date] >= '20140101' and Description like 'OO-%') i		 
			on a.MandantenName = i.MandantenName 
			and a.[Entry No] = i.[Entry No]
		left join stg.ExchangeRate er with(tablock) 
				on er.Month = convert(int,(convert(varchar(8),a.[Posting Date], 112)))/100
				 and er.GuV_Bilanz = 'GuV'
				 and er.Currency = a.Currency
					
		-- Kontoumbuchungen	
		update t
			set KontoNummer = kub.KontoNeu
		from [core].[GLEntry_intermediate] t
		inner join meta.Kontoumbuchungen kub 
			on kub.KontoAlt = t.KontoNummer_Alt collate database_default
			and kub.KontoNeuSeit > PostingDateID 
			and kub.MandantenID  = t.MandantenID
		

		-- --------------------------------------------------------
		-- Index core_fi.GLEntry_intermediate - idx_GLEntry_intermediate - erstellen
		-- --------------------------------------------------------
/*		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[core_fi].[GLEntry_intermediate]') AND name = N'idx_GLEntry_intermediate') begin
			CREATE NONCLUSTERED INDEX [idx_GLEntry_intermediate] ON [core_fi].[GLEntry_intermediate] 
			(
				[PostingDateID] ASC,
				[KontoNummer] ASC,
				[MandantenID] ASC,
				[KostenstellenID] ASC
			)
			WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		end
*/		
		-- -------------------------------------------------
		-- Index stg_fi.G_L_Entry - idx_stg_fakten - löschen
		-- -------------------------------------------------
/*		IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[stg_fi].[G_L_Entry]') AND name = N'idx_stg_fakten')
			DROP INDEX [idx_stg_fakten] ON [stg_fi].[G_L_Entry] WITH ( ONLINE = OFF )
*/				  
		-- -------------------
		-- zusätzliche OneOffs
		-- -------------------
		-- OneOffs setzen bis inkl. 2013: alle Buchungen auf bestimmte Konten sind OneOffs
		update core.GLEntry_intermediate
		set OneOffID = 9 -- nicht genauer bestimmtes Exceptional Item
		where KontoNummer in ('495002', '495110', '495330')
		  and PostingDateID < '20140101'
		  
		-- Kostenstelle 30700006 und 10700006 (Sprüngli) sind OneOff i - expenses in connection with swiss-aquisition
		update f
			set OneOffID = 10
		from core.GLEntry_intermediate f
		inner join core.[dim_Dimension Value] kst
			 on f.KostenstellenID = kst.ID
		where Code in ('30700006', '10700006')
		and PostingDateID >= 20160101
		and OneOffID <> 50
		
		-- Kostenstelle 30700008 ist OneOff j one-off expenses in connection with project fashionista
		update f
			set OneOffID = 11
		from core.GLEntry_intermediate f
		inner join core.[dim_Dimension Value] kst
			 on f.KostenstellenID = kst.ID
		where Code = '30700008'
		and OneOffID <> 50	
		
		-- -------------------------------------------------------------
		-- Nullstellung setzen bei GuV Konten Nullstellung, Konto 086000
		-- -------------------------------------------------------------
		update f
		set f.Nullstellung = 1
		--select * 
		from core.GLEntry_intermediate f
		inner join stg.[G_L Entry] stg
			 on f.EntryNo = stg.[Entry No]
		where f.KontoNummer in ('086000','086000H','086000I')
		  and DATEPART(hh, stg.[Posting Date]) = 23
			
		-- ----------------------------------------------------------------------------------------------------------
		-- Ausnahmen: Kostenstellenumverteilung für bestimmte Konten und Kostenstellen mit festem Schlüssel imk Jahr 2015
		-- ----------------------------------------------------------------------------------------------------------
		
		-- bisherige Einträge markieren, indem LOAD_DATE auf 1.1.1900 gesetzt wird
		update f
			set f.LOAD_DATE = '19000101'
		-- select *
		from core.GLEntry_intermediate f
		inner join core.[dim_Dimension Value] dvon
			 on f.KostenstellenID = dvon.ID
			and f.MandantenID = core.Get_dim_Mandanten_ID(dvon.MandantenName)
		inner join (select distinct KontoNummer, Kostenstelle_von from meta.Kostenstellen_Umverteilung) ku
			 on f.KontoNummer = ku.KontoNummer
			and dvon.Code = ku.Kostenstelle_von
		where PostingDateID < 20160101
		
		-- zusätzliche Einträge pro Kostenstelle_zu
		insert into core.GLEntry_intermediate
			(EntryNo
			, MandantEntryNo
			, KontoNummer
			, KontoNummer_Alt
			, PostingDateID
			, BelegartID
			, Amount
			, Amount_EUR
			, GegenKontoNummer
			, LOAD_DATE
			, MandantenID
			, Ultimobuchung
			, KostenstellenID
			, OneOffID
			, LevelID
			, Nullstellung
			, ProfitCenterID
			, CapexItID
			, ID_Nachbuchungen
			, Currency
			, PART_KEY
			, [User ID]
			, [Reason Code]
			, [System-Created Entry]
			, [Additional-Currency Amount]
			, [Amount (FCY)]
			, [Amount (LCY)]
			, CreationDate
			, [Orig_ Currency Code]
			, [Original Amount (FCY)]
			, [Source No_]
			, [Source Type]
			, [Source Code]
			, [Transaction No_]
			, TS_INT)
		select EntryNo
			, MandantEntryNo
			, f.KontoNummer
			, KontoNummer_Alt
			, PostingDateID
			, BelegartID
			, Amount * ku.Anteil
			, Amount_EUR * ku.Anteil
			, GegenKontoNummer
			, getdate()
			, MandantenID
			, Ultimobuchung
			, dzu.ID as KostenstellenID
			, OneOffID
			, LevelID
			, Nullstellung
			, dzu.ProfitCenterID as ProfitCenterID
			, CapexItID
			, ID_Nachbuchungen
			, Currency
			, PART_KEY
			, [User ID]
			, [Reason Code]
			, [System-Created Entry]
			, [Additional-Currency Amount]
			, [Amount (FCY)]
			, [Amount (LCY)]
			, CreationDate
			, [Orig_ Currency Code]
			, [Original Amount (FCY)]
			, [Source No_]
			, [Source Type]
			, [Source Code]
			, [Transaction No_]
			, TS_INT
		from core.GLEntry_intermediate f
		inner join core.[dim_Dimension Value] dvon
			 on f.KostenstellenID = dvon.ID
			and f.MandantenID = core.Get_dim_Mandanten_ID(dvon.MandantenName)
		inner join meta.Kostenstellen_Umverteilung ku
			 on f.KontoNummer = ku.KontoNummer
			and dvon.Code = ku.Kostenstelle_von
		inner join core.[dim_Dimension Value] dzu
			 on ku.Kostenstelle_zu = dzu.Code
			and f.MandantenID = core.Get_dim_Mandanten_ID(dzu.MandantenName)
		where PostingDateID < 20160101	
				
		-- Originaleinträge löschen (die vorher markierten)
		delete f
		-- select *
		from core.GLEntry_intermediate f
		inner join core.[dim_Dimension Value] dvon
			 on f.KostenstellenID = dvon.ID
			and f.MandantenID = core.Get_dim_Mandanten_ID(dvon.MandantenName)
		inner join (select distinct KontoNummer, Kostenstelle_von from meta.Kostenstellen_Umverteilung) ku
			 on f.KontoNummer = ku.KontoNummer
			and dvon.Code = ku.Kostenstelle_von
		where f.LOAD_DATE = '19000101'
		
	END TRY
	
	BEGIN CATCH
	END CATCH
END
